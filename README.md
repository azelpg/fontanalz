# fontanalz

http://azsky2.html.xdomain.jp/

TrueType/OpenType/CFF フォントの解析を行い、標準出力またはテキストファイルに出力します。<br>
フォントを直接読み込むプログラムを作りたい人や、フォントの中身を確認したい人向け。

- 一部の情報には対応していません。
- 可変(バリアブル)フォントの情報には対応していません。
- name テーブルの文字列や、Unicode 文字は、常に UTF-8 で出力されます。<br>
標準出力に出力した場合、ロケールが UTF-8 以外だと文字化けしますので、その場合は、テキストファイルに出力してください。

## 動作環境

Linux/FreeBSD/macOS

## ビルド・インストール

C コンパイラ、ninja (ビルドツール) が必要です。<br>
ninja のインストールパッケージ名は、**ninja** または **ninja-build** です。

~~~
$ ./configure
$ cd build
$ ninja
# ninja install
~~~

## 出力例

~~~
==================================
 * 'head' table
==================================
u16 majorVersion       : 1
u16 minorVersion       : 0
s32 fontRevision       : 2.0010 (0x00020041)
u32 checkSumAdjustment : 0x05DFE23A
u32 magicNumber        : 0x5F0F3CF5
u16 flags              : 0x0003
  ON = bit0,bit1
u16 unitsPerEm         : 1000
u64 created            : 2019/04/04 21:58:23
u64 modified           : 2019/04/04 21:58:23
s16 xMin               : -1002
s16 yMin               : -1048
s16 xMax               : 2928
s16 yMax               : 1808
u16 macStyle           : 0x0000
  bit0 (0) Bold
  bit1 (0) Italic
  bit2 (0) Underline
  bit3 (0) Outline
  bit4 (0) Shadow
  bit5 (0) Condensed
  bit6 (0) Extended
u16 lowestRecPPEM      : 3
s16 fontDirectionHint  : 2
s16 indexToLocFormat   : 0
s16 glyphDataFormat    : 0
~~~


