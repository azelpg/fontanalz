**********************************************
  fontanalz
  Copyright (c) 2022 Azel

  http://azsky2.html.xdomain.jp/
  https://gitlab.com/azelpg/fontanalz
  <azelpg@gmail.com>
**********************************************

This software is released under the MIT License.
Please see the file COPYING for details.


=====================================
 * Overview
=====================================

Analyze TrueType/OpenType/CFF fonts and output as stdout or text file.

For those who want to create a program that reads fonts directly
or for those who want to check the contents of fonts.

- Some information is not supported.
- Variable font information is not supported.

* String and Unicode characters in the name table are always output in UTF-8.
  When output to stdout, characters will be garbled if the locale is not UTF-8.


=====================================
 * Operating environment
=====================================

Linux/FreeBSD/macOS


=====================================
 * What you need to compile
=====================================

- C compiler
- ninja (build tool)


--- Required packages  ---

(Debian/Ubuntu)

  gcc or clang, ninja-build

(RedHat)

  gcc or clang, ninja-build

(Arch Linux)

  gcc or clang, ninja

(macOS)

  xcode ninja

(FreeBSD)

  ninja


=====================================
 * Compile and install
=====================================

After extracting the archive, go to the extracted directory and do the following.

$ ./configure
$ cd build
$ ninja
# ninja install


By default, it will be installed under "/usr/local".

You can change the settings by specifying options when you run ./configure.
You can get help with $ ./configure --help.

[Exsample] Install in /usr
$ ./configure --prefix=/usr


## Uninstall
# ninja uninstall

## Generate files for package file
$ DESTDIR=<DIR> ninja install
