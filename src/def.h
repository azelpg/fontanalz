/*$
fontanalz
Copyright (c) 2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

typedef struct
{
	const char *in_filename;	//フォントファイル名
	
	const char *opt_output;
	uint32_t tags,	//処理するタグのフラグ (default = TAG_DEFAULT)
		optflags;
	int index,		//コレクションフォントのインデックス (default=0)
		gid_top,gid_end;	//アウトライン出力時のGID範囲
}AppData;

extern AppData g_app;

/* オプションフラグ */

enum
{
	OPTF_FILE = 1<<0,	//各タグをファイルに出力
	OPTF_TABLES = 1<<1,	//各テーブルをファイルに出力
	OPTF_VERBOSE = 1<<2,	//タグの詳細出力
	OPTF_RAW = 1<<3			//タグの生データ出力
};

/* tag */

enum
{
	TAG_TTCF = 1<<0,
	TAG_SFNT = 1<<1,
	TAG_HEAD = 1<<2,
	TAG_OS2  = 1<<3,
	TAG_MAXP = 1<<4,
	TAG_NAME = 1<<5,
	TAG_POST = 1<<6,
	TAG_CMAP = 1<<7,
	TAG_CMAP_UNI = 1<<8,
	TAG_HHEA = 1<<9,
	TAG_HMTX = 1<<10,
	TAG_VHEA = 1<<11,
	TAG_VMTX = 1<<12,
	TAG_VORG = 1<<13,
	TAG_LOCA = 1<<14,
	TAG_GLYF = 1<<15,
	TAG_GLYF_COMP = 1<<16,
	TAG_EBLC = 1<<17,
	TAG_GDEF = 1<<18,
	TAG_BASE = 1<<19,
	TAG_GPOS = 1<<20,
	TAG_GSUB = 1<<21,
	TAG_CFF  = 1<<22,

	TAG_DEFAULT = TAG_TTCF | TAG_SFNT | TAG_HEAD | TAG_OS2
		| TAG_MAXP | TAG_NAME | TAG_POST | TAG_CMAP | TAG_HHEA | TAG_VHEA,

	TAG_ALL = TAG_DEFAULT | TAG_CMAP_UNI | TAG_HMTX | TAG_VMTX | TAG_VORG
		| TAG_LOCA | TAG_EBLC | TAG_GDEF | TAG_BASE | TAG_GPOS | TAG_GSUB | TAG_CFF
};

/* util */

void app_memset0(void *ptr,int size);
void *app_malloc(uint32_t size);
void *app_malloc0(uint32_t size);
void app_free(void *ptr);

int app_strcomp_camma(const char *s1,const char *s2);
int app_is_existdir(const char *path);
char *app_join_path(const char *path,const char *fname);
char *app_get_output_path(const char *output,const char *filename,const char *suffix);
