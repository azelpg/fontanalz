/*$
fontanalz
Copyright (c) 2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/**********************************
 * フォント
 **********************************/

#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "def.h"
#include "font.h"
#include "font_pv.h"


/* ヘッダ読み込み */

static int _read_header(Font *p,int index)
{
	uint32_t tag,num;
	uint16_t u16[4];
	int ret;

	if(index < 0) index = 0;

	//4byte

	if(font_read32(p, &tag)) return FONTERR_READ;

	//CFF ファイルか

	if((tag & 0xffffff00) == 0x01000400)
	{
		p->is_cff_file = 1;
		return 0;
	}

	//TTC ヘッダ

	if(tag == 0x74746366)
	{
		if(font_reads(p, "s4i", &num)) return FONTERR_READ;

		if(index >= num) return FONTERR_INDEX;

		p->ttcf_num = num;

		if(font_seek(p, index * 4)
			|| font_read32(p, &p->sfnt_offset)	//オフセットテーブルから読み込み
			|| font_seek_top(p, p->sfnt_offset)	//SFNT 位置へ
			|| font_read32(p, &tag))
			return FONTERR_READ;
	}

	//--- オフセットテーブル

	if(tag != 0x00010000 && tag != 0x4F54544F)
		return FONTERR_NOT_FONT;

	if(font_read16_array(p, u16, 4))
		return FONTERR_READ;

	p->tblrec_num = u16[0];

	//テーブルレコード

	if(font_read32ar_alloc(p, (void **)&p->tblrec_buf, 16 * p->tblrec_num))
		return FONTERR_READ;

	//---- 各テーブル

	//maxp: グリフ数

	ret = font_read_table_val(p, MAKE_TAG('m','a','x','p'), 4, 0, &p->maxp_gnum);
	if(ret) return ret;

	//head: loca オフセットサイズ

	font_read_loca_offset_size(p, &p->loca_offset_size);

	return 0;
}


//=============================
// main
//=============================


/** フォント閉じる */

void font_close(Font *p)
{
	if(!p) return;

	app_free(p->tblrec_buf);
	app_free(p->gidunimap);
	app_free(p->readbuf);

	fclose(p->fp);

	app_free(p);
}

/** フォントファイル読み込み */

int font_openfile(Font **dst,const char *filename,int index)
{
	Font *p;
	FILE *fp;
	int ret;

	fp = fopen(filename, "rb");
	if(!fp) return FONTERR_OPENFILE;

	//Font

	p = app_malloc0(sizeof(Font));
	if(!p)
	{
		fclose(fp);
		return FONTERR_ALLOC;
	}

	p->fp = fp;

	//ヘッダ

	ret = _read_header(p, index);
	if(ret)
	{
		font_close(p);
		return ret;
	}

	//GID -> Unicode マップ

	if(!p->is_cff_file)
		font_make_gidunimap(p);

	//

	*dst = p;

	return FONTERR_OK;
}

/** CFF ファイルか */

int font_is_cff_file(Font *p)
{
	return p->is_cff_file;
}

/** 各テーブルをファイルに出力 */

int font_output_tables(Font *p)
{
	int i,j,ret;
	uint32_t *ps = p->tblrec_buf,tag;
	char *path,fname[16],c;
	FILE *fp;

	if(p->is_cff_file) return 0;

	for(i = p->tblrec_num; i; i--, ps += 4)
	{
		tag = ps[0];
		
		ret = font_read_table(p, tag);
		if(ret) return ret;

		//ファイル名

		for(j = 24, path = fname; j >= 0; j -= 8)
		{
			c = (uint8_t)(tag >> j);
			if(c == '/') c = '_';

			if(c != ' ')
				*(path++) = c;
		}

		strcpy(path, ".dat");

		path = app_get_output_path(g_app.opt_output, g_app.in_filename, fname);
		if(!path) return FONTERR_ALLOC;

		//書き込み

		printf("%s\n", fname);

		fp = fopen(path, "wb");

		app_free(path);

		if(!fp) return FONTERR_OPENFILE;

		fwrite(p->readbuf, 1, p->readbuf_size, fp);

		fclose(fp);
	}

	return FONTERR_OK;
}

/** エラーメッセージ表示 */

void font_puterr(int err)
{
	const char *mes = NULL;

	switch(err)
	{
		case FONTERR_MES:
			return;
		case FONTERR_OPENFILE:
			mes = "can't open file";
			break;
		case FONTERR_ALLOC:
			mes = "memory alloc error";
			break;
		case FONTERR_READ:
			mes = "failed read. file is corrupted.";
			break;
		case FONTERR_INDEX:
			mes = "invalid index";
			break;
		case FONTERR_NOT_FONT:
			mes = "header error. file is not font.";
			break;
		case FONTERR_INVALID:
			mes = "invalid value";
			break;
		case FONTERR_UNFOUND:
			mes = "not exist";
			break;
	}

	if(mes)
		fprintf(stderr, "%s\n", mes);
}

/* NameRecord から nameID 4 or 1 の文字列を出力 */

static int _put_fontname(Font *p)
{
	uint8_t *pbuf,*name;
	uint16_t format,num,offstr,plat,enc,lang,nameid,len,off;
	int res_plat,res_len,res_lang,f;

	if(font_readbuf_format(p, "hhh", &format, &num, &offstr)
		|| font_readbuf_skip(p, num * 12, &pbuf))
		return FONTERR_READ;

	//対象の文字列を見つける

	res_plat = -1;
	res_len = 0;
	res_lang = 0;

	for(; num; num--, pbuf += 12)
	{
		font_getbuf_format(pbuf, "hhhhhh", &plat, &enc, &lang, &nameid, &len, &off);

		if(nameid == 1 || nameid == 4)
		{
			//nameID=4 の方が後に来るので、存在する場合はそちらを優先。
			//platform は 0 or 3 を優先。
			//plat = 3 の場合、lang = 1033 (En) を優先。なければ最初の言語。

			f = 1;
			
			if(res_plat == -1)
				//最初
				f = 1;
			else if(res_plat == 0 && plat != 1)
				//plat=0(Unicode) がある場合、plat=1 は除外
				f = 0;
			else if(res_plat == 3 && res_lang == 1033 && lang != 1033)
				//plat=3 で英語が来ている場合、他の言語は除外
				f = 0;

			//新しい文字列

			if(f)
			{
				if(font_readbuf_getposptr(p, offstr + off, len, &name))
					return FONTERR_READ;

				res_plat = plat;
				res_len = len;
				res_lang = lang;
			}
		}
	}

	//文字列出力

	if(res_plat == -1)
		printf("no name\n");
	else if(res_plat == 1)
		printf("%*s\n", res_len, name);
	else
	{
		p->put.fp = stdout;
		font_putbuf_utf16be(p, name, res_len);
		printf("\n");
	}

	return 0;
}

/** コレクションの各フォント名出力 */

int font_put_fontname(Font *p)
{
	int i,ret,fnum;
	uint16_t tnum;
	uint32_t off,tag,val[4];

	fnum = p->ttcf_num;

	if(!fnum)
	{
		fputs("not a collection font\n", stderr);
		return 0;
	}

	//

	for(i = 0; i < fnum; i++)
	{
		if(font_seek_top(p, 12 + i * 4)
			|| font_read32(p, &off)
			|| font_seek_top(p, off)	//オフセットテーブルへ
			|| font_read32(p, &tag)
			|| (tag != 0x00010000 && tag != 0x4F54544F)
			|| font_read16(p, &tnum)	//テーブル数
			|| font_seek(p, 6))
			return FONTERR_READ;

		//name テーブル検索

		for(; tnum; tnum--)
		{
			if(font_read32_array(p, val, 4)) return FONTERR_READ;

			if(val[0] == MAKE_TAG('n','a','m','e'))
				break;
		}

		if(!tnum) continue;

		//読み込み

		ret = font_read_to_readbuf(p, val[2], val[3]);
		if(ret) return ret;

		//name テーブル

		printf("[%d] ", i);

		ret = _put_fontname(p);
		if(ret) return ret;
	}

	return 0;
}

