/*$
fontanalz
Copyright (c) 2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

typedef struct _Font Font;
typedef int (*FontPutFunc)(Font *p);

enum
{
	FONTERR_OK = 0,
	FONTERR_MES,		//すでにエラー出力済み
	FONTERR_OPENFILE,
	FONTERR_ALLOC,
	FONTERR_READ,		//読み込みエラー
	FONTERR_INDEX,		//インデックス番号エラー
	FONTERR_NOT_FONT,	//フォントではない
	FONTERR_UNFOUND,	//見つからなかった (タグなど)
	FONTERR_INVALID		//無効な値
};

int font_openfile(Font **dst,const char *filename,int index);
void font_close(Font *p);

int font_is_cff_file(Font *p);
int font_output_tables(Font *p);
void font_puterr(int err);
int font_put_fontname(Font *p);

/* put */

int font_put_ttcf(Font *p);
int font_put_sfnt(Font *p);
int font_put_head(Font *p);
int font_put_os2(Font *p);
int font_put_maxp(Font *p);
int font_put_name(Font *p);
int font_put_post(Font *p);
int font_put_cmap(Font *p);
int font_put_cmap_uni(Font *p);
int font_put_hhea(Font *p);
int font_put_hmtx(Font *p);
int font_put_vhea(Font *p);
int font_put_vmtx(Font *p);
int font_put_vorg(Font *p);
int font_put_loca(Font *p);
int font_put_glyf(Font *p);
int font_put_glyf_comp(Font *p);
int font_put_eblc(Font *p);
int font_put_gdef(Font *p);
int font_put_base(Font *p);
int font_put_gpos(Font *p);
int font_put_gsub(Font *p);
int font_put_cff(Font *p);

