/*$
fontanalz
Copyright (c) 2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#define ARG_STACK_NUM  48
#define SUBR_NEST_NUM  10

typedef struct
{
	uint8_t *buf;
	uint32_t size;
}bufdat;

//CharString 処理中のデータ

typedef struct
{
	Font *font;
	FILE *fp;
	
	uint8_t *pcur;
	uint32_t cursize;
	
	int gid,
		arg_num,	//引数スタックの数
		stem_num,	//ステム数
		subr_num;	//サブルーチン ネスト数
	int32_t arg[ARG_STACK_NUM];		//引数スタック
	bufdat subrback[SUBR_NEST_NUM];	//サブルーチンからの戻り位置
}cff_charstring;

//CFF

struct _cffdat
{
	int glyph_num,			//グリフ数 (CharStrings INDEX の個数)
		offset_charset,		//Charsets データのオフセット値
		offset_charstrings,	//CharStrings データのオフセット値
		offset_fdselect,	//FDSelect のオフセット値
		offset_fdarray,		//FDArray のオフセット値
		private_size,		//Top DICT の private
		private_offset,
		offset_local_subr,	//Private DICT の subr オフセット
		offset_global_subr,	//Global Subr INDEX
		tmp_offset,
		tmp_size;

	uint8_t *cid_fdind;	//(CID) GID ごとの Font DICT index
	uint32_t *cid_subr;	//(CID) Font DICT ごとのローカルサブルーチンオフセット
	int cid_fd_num;		//(CID) Font DICT の数

	cff_charstring cs;
};

//

int font_cff_charstring_init(Font *p);
int font_proc_charstring(Font *p,int gid,uint8_t *buf,uint32_t size);

