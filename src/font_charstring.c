/*$
fontanalz
Copyright (c) 2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*************************************
 * フォント: CFF Type2 CharString
 *************************************/

#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "def.h"
#include "font.h"
#include "font_pv.h"
#include "font_cff.h"

//-----------------

#define CMD2(v)  ((12<<8) | (v))

//command 1byte (0-31)

static const char *g_comname1[] = {
	0, "hstem", 0, "vstem", "vmoveto", "rlineto", "hlineto", "vlineto", "rrcurveto", 0,
	"callsubr", "return", 0, 0, "endchar", 0,0,0,
	"hstemhm", "hintmask", "cntrmask", "rmoveto", "hmoveto", "vstemhm", "rcurveline",
	"rlinecurve", "vvcurveto", "hhcurveto", 0, "callgsubr", "vhcurveto", "hvcurveto"
};

//command 2byte (12 0-37)

static const char *g_comname2[] = {
	0, 0, 0, "and", "or", "not", 0,0,0, "abs", "add", "sub", "div", 0, "neg", "eq", 0,0,
	"drop", 0, "put", "get", "ifelse", "random", "mul", 0, "sqrt", "dup", "exch", "index", "roll",
	0,0,0, "hflex", "flex", "hflex1", "flex1"
};

//-----------------


//=======================
// 読み込み
//=======================


/* 1byte 読み込み */

static int _read8(cff_charstring *p,uint8_t *dst)
{
	if(p->cursize < 1) return 1;

	*dst = *(p->pcur++);

	p->cursize--;

	return 0;
}


//=======================
// 引数スタック
//=======================


/* 引数スタックに追加 */

static int _push_arg(cff_charstring *p,int32_t val)
{
	if(p->arg_num == ARG_STACK_NUM) return FONTERR_INVALID;

	p->arg[p->arg_num++] = val;

	return 0;
}

/* 引数スタッククリア */

static void _clear_arg(cff_charstring *p)
{
	p->arg_num = 0;
}


//=======================
// サブルーチン
//=======================


/* サブルーチンの index 値を実際の値にする
 *
 * num: サブルーチンの数 */

static int _conv_subr_index(int index,int num)
{
	if(num < 1239)
		return index + 107;
	else if(num < 33900)
		return index + 1131;
	else
		return index + 32768;
}

/* INDEX から、指定位置のデータ取得 */

static int _get_index_data(Font *p,uint32_t offset,int index,bufdat *dst)
{
	uint8_t *pbuf;
	uint16_t num;
	uint8_t offsize;
	uint32_t off1,off2;

	if(!offset) return FONTERR_INVALID;

	if(font_readbuf_setpos(p, offset)
		|| font_readbuf16(p, &num))
		return FONTERR_READ;

	index = _conv_subr_index(index, num);
	
	if(index >= num
		|| font_readbuf8(p, &offsize)
		|| font_readbuf_skip(p, offsize * (num + 1), &pbuf))
		return FONTERR_READ;

	fprintf(p->put.fp, "<%d>", index);

	//オフセット位置

	pbuf += index * offsize;

	font_getbuf_nsize(pbuf, offsize, &off1);
	font_getbuf_nsize(pbuf + offsize, offsize, &off2);

	//

	if(font_readbuf_getposptr(p,
		font_readbuf_getpos(p) + off1 - 1, off2 - off1, &dst->buf))
		return FONTERR_READ;

	dst->size = off2 - off1;

	return 0;
}

/* CID のローカルサブルーチンの位置取得 */

static int _get_subr_cid_local(Font *p,int gid,int index,bufdat *dst)
{
	cffdat *cff = p->pcffdat;
	int fd;

	if(!cff->cid_fdind) return FONTERR_INVALID;

	fd = cff->cid_fdind[gid];
	if(fd >= cff->cid_fd_num) return FONTERR_INVALID;

	return _get_index_data(p, cff->cid_subr[fd], index, dst);
}

/* サブルーチン呼び出し */

static int _proc_subr(cff_charstring *p,int global)
{
	cffdat *cff = p->font->pcffdat;
	int ret;
	int32_t index;
	bufdat subr,*psub;

	if(!p->arg_num || p->subr_num == SUBR_NEST_NUM)
		return FONTERR_INVALID;

	//index (直前の引数。まだ実際の値ではない)

	index = p->arg[--(p->arg_num)];

	//サブルーチンの位置

	if(global)
		ret = _get_index_data(p->font, cff->offset_global_subr, index, &subr);
	else
	{
		//ローカル
		
		if(cff->offset_fdarray)
			ret = _get_subr_cid_local(p->font, p->gid, index, &subr);
		else
		{
			if(!cff->offset_local_subr) return FONTERR_INVALID;
			
			ret = _get_index_data(p->font, cff->private_offset + cff->offset_local_subr, index, &subr);
		}
	}

	if(ret) return ret;

	//戻り位置

	psub = p->subrback + p->subr_num;

	psub->buf = p->pcur;
	psub->size = p->cursize;

	p->subr_num++;

	//移動

	p->pcur = subr.buf;
	p->cursize = subr.size;

	fputs(" {", p->fp);

	return 0;
}


//====================


/* hintmask, cntrmask */

static int _proc_hintmask(cff_charstring *p)
{
	uint8_t *ps = p->pcur;
	int n;

	//現在の引数は、vstem の値として扱う (幅の値は無視)
	// ".. hintmask" (hstem なし)
	// ".. hstemhm .. vstemhm .. hintmask" (vstem 複数)

	p->stem_num += p->arg_num / 2;
	p->arg_num = 0;

	//ステム数 x 1ビットのバイト値が続く

	n = (p->stem_num + 7) / 8;

	if(p->cursize < n) return FONTERR_READ;

	p->pcur += n;
	p->cursize -= n;

	//

	fputs(" [", p->fp);

	for(; n; n--, ps++)
		fprintf(p->fp, " 0x%02X", *ps);

	fputs(" ]", p->fp);

	//

	_clear_arg(p);

	return 0;
}

/* ステム処理 */

static void _proc_stem(cff_charstring *p)
{
	//奇数の場合、先頭に送り幅がある

	p->stem_num += p->arg_num / 2;

	_clear_arg(p);
}

/* コマンド処理 */

static int _proc_command(cff_charstring *p,int cmd)
{
	int ret = 0;

	switch(cmd)
	{
		case 1:  //hstem
		case 18: //hstemhm
			_proc_stem(p);
			break;
			
		case 3:  //vstem
		case 23: //vstemhm
			_proc_stem(p);
			break;

		case 19: //hintmask
		case 20: //cntrmask
			ret = _proc_hintmask(p);
			break;

		//[!] サブルーチン関連では、last_hstem はそのまま
		case 10: //callsubr
			ret = _proc_subr(p, 0);
			break;
		case 29: //callgsubr
			ret = _proc_subr(p, 1);
			break;
		case 11: //return
			if(p->subr_num)
			{
				p->subr_num--;

				p->pcur = p->subrback[p->subr_num].buf;
				p->cursize = p->subrback[p->subr_num].size;

				fputs(" }", p->fp);
			}
			break;
	
		default:
			_clear_arg(p);
			break;
	}

	return ret;
}

/* 数値 or コマンドを読み込み
 *
 * dst_cmd: -1 で数値 */

static int _read_val(cff_charstring *p,int *dst_cmd,int32_t *dst_val)
{
	uint8_t *ps;
	uint8_t b0,b1;
	int cmd;
	int32_t val = 0;

	_read8(p, &b0);

	ps = p->pcur;
	cmd = -1; //-1 で数値、それ以外でコマンド

	if(b0 == 28)
	{
		//3byte 数値 (int16)

		if(p->cursize < 2) return FONTERR_READ;

		val = (int16_t)((ps[0] << 8) | ps[1]);

		p->pcur += 2;
		p->cursize -= 2;
	}
	else if(b0 >= 32 && b0 <= 246)
	{
		//1byte数値 (-107〜107)

		val = b0 - 139;
	}
	else if(b0 >= 247 && b0 <= 254)
	{
		//2byte 数値 (108〜1131, -1131〜-108)

		if(p->cursize < 1) return FONTERR_READ;

		b1 = *(p->pcur++);
		p->cursize--;

		if(b0 >= 247 && b0 <= 250)
			val = (b0 - 247) * 256 + b1 + 108;
		else
			val = -((b0 - 251) * 256) - b1 - 108;
	}
	else if(b0 == 255)
	{
		//5byte 固定小数点数

		if(p->cursize < 4) return FONTERR_READ;

		val = (int32_t)(((uint32_t)ps[0] << 24) | (ps[1] << 16) | (ps[2] << 8) | ps[3]);

		p->pcur += 4;
		p->cursize -= 4;
	}
	else if(b0 == 12)
	{
		//2byte 命令

		if(_read8(p, &b1)) return FONTERR_READ;

		if(b1 > 37 || !g_comname2[b1])
			return FONTERR_INVALID;

		cmd = CMD2(b1);

		fprintf(p->fp, " %s", g_comname2[b1]);
	}
	else
	{
		//1byte 命令 (0-31)

		if(!g_comname1[b0]) return FONTERR_INVALID;

		cmd = b0;

		fprintf(p->fp, " %s", g_comname1[b0]);
	}

	//数値

	if(cmd == -1)
		fprintf(p->fp, " %d", val);

	//

	*dst_cmd = cmd;
	*dst_val = val;

	return 0;
}

/* メイン処理 */

static int _proc_main(cff_charstring *p)
{
	int ret,cmd;
	int32_t val;

	while(p->cursize)
	{
		ret = _read_val(p, &cmd, &val);
		if(ret) return ret;

		//

		if(cmd == -1)
		{
			//数値

			ret = _push_arg(p, val);
			if(ret) return ret;
		}
		else
		{
			//コマンド

			if(cmd == 14) return 0;

			ret = _proc_command(p, cmd);
			if(ret) return ret;
		}
	}

	return 0;
}

/** CharString 処理 (GID ごとに呼ばれる) */

int font_proc_charstring(Font *p,int gid,uint8_t *buf,uint32_t size)
{
	cff_charstring *cs = &p->pcffdat->cs;
	int ret;

	app_memset0(cs, sizeof(cff_charstring));

	cs->font = p;
	cs->fp = p->put.fp;
	cs->gid = gid;
	cs->pcur = buf;
	cs->cursize = size;

	ret = _proc_main(cs);

	fputc('\n', p->put.fp);	

	return ret;
}


//=============================================
// (CID) ローカルサブルーチン用のテーブル作成
//=============================================


/* FDSelect から Font DICT index のテーブル作成 (GID ごと) */

static int _create_cid_fdind(Font *p,cffdat *cff)
{
	uint8_t *pbuf,*buf;
	uint8_t format,ind;
	uint16_t num,first,next;
	int gnum;

	gnum = cff->glyph_num;

	buf = cff->cid_fdind = (uint8_t *)app_malloc0(gnum);
	if(!buf) return FONTERR_ALLOC;

	//

	if(font_readbuf_setpos(p, cff->offset_fdselect)
		|| font_readbuf8(p, &format)
		|| (format != 0 && format != 3))
		return FONTERR_READ;

	if(format == 0)
	{
		if(font_readbuf_skip(p, gnum, &pbuf)) return FONTERR_READ;
		
		memcpy(buf, pbuf, gnum);
	}
	else
	{
		//format 3
		
		if(font_readbuf16(p, &num)
			|| font_readbuf_skip(p, num * 3 + 2, &pbuf))
			return FONTERR_READ;

		font_getbuf16(pbuf, &first);
		pbuf += 2;

		for(; num; num--, pbuf += 3)
		{
			font_getbuf_format(pbuf, "bh", &ind, &next);

			for(; first < next; first++)
			{
				if(first >= gnum) return FONTERR_INVALID;
				
				buf[first] = ind;
			}
		}
	}

	return 0;
}

/* DICT データから値を取得
 *
 * 浮動小数点数は整数部分のみ。
 *
 * return: -1=error, 0=value, 1=key */

static int _get_dict_val(uint8_t **ppbuf,uint32_t *psize,int32_t *dst)
{
	uint8_t *ps = *ppbuf;
	uint32_t size = *psize;
	int32_t val,pos,v,fsig,fdot,ret = 0;
	uint8_t b0;

	b0 = *(ps++);
	size--;

	if(b0 <= 21)
	{
		//---- キー

		if(b0 != 12)
			//1byte
			val = b0;
		else
		{
			//2byte

			if(!size) return -1;

			b0 = *(ps++);
			size--;

			val = (12 << 8) | b0;
		}

		ret = 1;
	}
	else if(b0 == 30)
	{
		//---- 実数
		
		pos = 0;
		val = 0;
		fsig = fdot = 0;

		while(size)
		{
			//4bit 値
			
			if(pos & 1)
				v = b0 & 0xf;
			else
			{
				if(!size) return -1;
				
				b0 = *(ps++);
				size--;

				v = b0 >> 4;
			}

			pos ^= 1;

			if(v == 15)
				break;
			else if(v >= 0 && v <= 9)
			{
				if(!fdot)
					val = val * 10 + v;
			}
			else if(v == 10)
				fdot = 1;
			else if(v == 14)
				fsig = 1;
		}

		if(fsig) val = -val;
	}
	else if(b0 >= 32 && b0 <= 246)
	{
		//-107..107

		val = b0 - 139;
	}
	else if(b0 >= 247 && b0 <= 250)
	{
		//108..1131

		if(!size) return -1;

		val = ((b0 - 247) << 8) + ps[0] + 108;

		ps++;
		size--;
	}
	else if(b0 >= 251 && b0 <= 254)
	{
		//-1131..-108

		if(!size) return -1;

		val = -((b0 - 251) << 8) - ps[0] - 108;

		ps++;
		size--;
	}
	else if(b0 == 28)
	{
		//-32768..32767
		
		if(size < 2) return -1;

		val = (ps[0] << 8) | ps[1];
		
		ps += 2;
		size -= 2;
	}
	else if(b0 == 29)
	{
		//5byte

		if(size < 4) return -1;

		val = (int32_t)((uint32_t)ps[0] << 24) | (ps[1] << 16) | (ps[2] << 8) | ps[3];

		ps += 4;
		size -= 4;
	}
	else
		return -1;

	//

	*ppbuf = ps;
	*psize = size;

	*dst = val;

	return ret;
}

/* DICT データ処理
 *
 * (type 0) Private から、Private DICT のオフセット位置とサイズ取得 (2個の配列)
 * (type 1) Subr から、ローカルサブルーチンのオフセット取得 */

static int _proc_cid_dict(Font *p,uint32_t offset,uint32_t size,int type,uint32_t *dst)
{
	uint8_t *ps;
	int32_t ret,val,valbk[2] = {0,0};

	if(font_readbuf_getposptr(p, offset, size, &ps)) return 1;

	while(size)
	{
		ret = _get_dict_val(&ps, &size, &val);
		if(ret == -1) return 1;

		if(ret == 1)
		{
			//Private

			if(type == 0 && val == 18)
			{
				dst[0] = valbk[0];
				dst[1] = valbk[1];

				return 0;
			}
			
			//Subr

			if(type == 1 && val == 19)
			{
				*dst = offset + valbk[0];
				return 0;
			}
		}

		if(ret == 0)
		{
			valbk[1] = valbk[0];
			valbk[0] = val;
		}
	}

	return 0;
}

/* FDArray から Font DICT のサブルーチンテーブルを作成
 *
 * 絶対位置のオフセット */

static int _create_cid_subr(Font *p,cffdat *cff)
{
	uint32_t *pd;
	uint8_t *poff;
	uint16_t num;
	uint8_t offsize;
	uint32_t off1,off2,offdat,pv[2];

	//Font DICT INDEX

	if(font_readbuf_setpos(p, cff->offset_fdarray)
		|| font_readbuf_format(p, "hb", &num, &offsize)
		|| font_readbuf_skip(p, offsize * (num + 1), &poff))
		return FONTERR_READ;

	//

	pd = cff->cid_subr = (uint32_t *)app_malloc0(num * 4);
	if(!pd) return FONTERR_ALLOC;

	cff->cid_fd_num = num;

	//

	offdat = font_readbuf_getpos(p);

	font_getbuf_nsize(poff, offsize, &off1);
	poff += offsize;

	for(; num; num--, poff += offsize, pd++, off1 = off2)
	{
		font_getbuf_nsize(poff, offsize, &off2);

		//Font DICT / Private

		pv[0] = 0;

		if(_proc_cid_dict(p, offdat + off1 - 1, off2 - off1, 0, pv))
			return FONTERR_READ;

		//Private DICT / Subr

		if(pv[0] && _proc_cid_dict(p, pv[0], pv[1], 1, pd))
			return FONTERR_READ;
	}
	
	return 0;
}

/** グリフ処理の開始時、CharString 用のデータを用意 */

int font_cff_charstring_init(Font *p)
{
	cffdat *cff = p->pcffdat;
	int ret;

	//CID

	if(!cff->offset_fdselect || !cff->offset_fdarray)
		return 0;

	ret = _create_cid_fdind(p, cff);
	if(ret) return ret;

	return _create_cid_subr(p, cff);
}


