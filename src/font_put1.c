/*$
fontanalz
Copyright (c) 2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/**********************************
 * フォント: 情報出力
 **********************************/

#include <stdint.h>
#include <stdio.h>

#include "def.h"
#include "font.h"
#include "font_pv.h"


/* フォント内に情報がないときは、FONTERR_UNFOUND を返す */


/** ttcf 出力 */

int font_put_ttcf(Font *p)
{
	FILE *fp;
	uint16_t maj,min;
	uint32_t i,num,sig[3],*poff = NULL;
	int ret;

	if(!p->sfnt_offset) return FONTERR_UNFOUND;

	if(font_seek_top(p, 4)
		|| font_reads(p, "hhi", &maj, &min, &num)
		|| font_reads(p, "Ii3", &poff, num, sig))
	{
		app_free(poff);
		return FONTERR_READ;
	}

	//出力

	ret = font_openput(p, "ttcf.txt", "TTC header");

	if(ret == FONTERR_OK)
	{
		fp = p->put.fp;
	
		fprintf(fp, "u32 ttcTag       : 0x74746366 ('ttcf')\n");
		fprintf(fp, "u16 majorVersion : %d\n", maj);
		fprintf(fp, "u16 minorVersion : %d\n", min);
		fprintf(fp, "u32 numFonts     : %u\n", num);
		fprintf(fp, "u32 tableDirectoryOffsets[numFonts]:\n");

		for(i = 0; i < num; i++)
			fprintf(fp, "[%d] 0x%08X\n", i, poff[i]);

		//DSIG
		
		if(maj >= 2)
		{
			fprintf(fp, "--- DSIG (ver 2) ---\n");
			fprintf(fp, "u32 dsigTag    : 0x%08X\n", sig[0]);
			fprintf(fp, "u32 dsigLength : %u\n", sig[1]);
			fprintf(fp, "u32 dsigOffset : 0x%08X\n", sig[2]);
		}
	}

	font_closeput(p);

	app_free(poff);

	return ret;
}

/** sfnt 出力 */

int font_put_sfnt(Font *p)
{
	FILE *fp;
	uint32_t ver,*ps;
	uint16_t u16[4];
	int i,num,ret;

	if(font_seek_top(p, p->sfnt_offset)
		|| font_reads(p, "ih4", &ver, u16))
		return FONTERR_READ;

	num = u16[0];

	//---- 書き込み

	ret = font_openput(p, "sfnt.txt", "SFNT Offset Table");
	if(ret) return ret;

	fp = p->put.fp;

	fprintf(fp, "u32 sfntVersion   : 0x%08X\n", ver);
	fprintf(fp, "u16 numTables     : %d\n", u16[0]);
	fprintf(fp, "u16 searchRange   : %d\n", u16[1]);
	fprintf(fp, "u16 entrySelector : %d\n", u16[2]);
	fprintf(fp, "u16 rangeShift    : %d\n", u16[3]);
	fprintf(fp, "TableRecord tableRecords[numTables]:\n");
	fprintf(fp, "# u32 tableTag, checksum, offset, length\n");
	
	//record

	ps = p->tblrec_buf;

	for(i = 0; i < num; i++, ps += 4)
	{
		fprintf(fp, "[%2d] ", i);
		font_put_tagname(p, ps[0]);
		fprintf(fp, ", 0x%08X, 0x%08X, %u\n", ps[1], ps[2], ps[3]);
	}

	font_closeput(p);

	return 0;
}

/** head 出力 */

int font_put_head(Font *p)
{
	int ret;
	uint16_t u16;

	ret = font_read_table(p, MAKE_TAG('h','e','a','d'));
	if(ret) return ret;

	//書き込み

	ret = font_openput(p, "head.txt", "'head' table");
	if(ret) return ret;

	font_putbuf16(p, "+majorVersion      ", 0);
	font_putbuf16(p, "+minorVersion      ", 0);
	font_putbuf32(p, "%fontRevision      ", 0);
	font_putbuf32(p, "$checkSumAdjustment", 0);
	font_putbuf32(p, "$magicNumber       ", 0);
	font_putbuf16(p, "#flags             ", 0);
	font_putbuf16(p, "+unitsPerEm        ", 0);
	font_putbuf_date(p, "created           ");
	font_putbuf_date(p, "modified          ");
	font_putbuf16(p, "-xMin              ", 0);
	font_putbuf16(p, "-yMin              ", 0);
	font_putbuf16(p, "-xMax              ", 0);
	font_putbuf16(p, "-yMax              ", 0);
	font_putbuf16(p, "$macStyle          ", &u16);
	font_put_bits_name(p, u16, 7, "Bold;Italic;Underline;Outline;Shadow;Condensed;Extended");

	font_putbuf16(p, "+lowestRecPPEM     ", 0);
	font_putbuf16(p, "-fontDirectionHint ", 0);
	font_putbuf16(p, "-indexToLocFormat  ", 0);
	font_putbuf16(p, "-glyphDataFormat   ", 0);

	return font_closeput_ret(p);
}

/** OS/2 出力 */

int font_put_os2(Font *p)
{
	FILE *fp;
	int ret;
	uint16_t ver,u16,tmp;

	ret = font_read_table(p, MAKE_TAG('O','S','/','2'));
	if(ret) return ret;

	//------

	ret = font_openput(p, "OS_2.txt", " 'OS/2' table");
	if(ret) return ret;

	fp = p->put.fp;

	//ver 0

	font_putbuf16(p, "+version            ", &ver);
	font_putbuf16(p, "-xAvgCharWidth      ", 0);
	font_putbuf16(p, "+usWeightClass      ", 0);
	font_putbuf16(p, "+usWidthClass       ", 0);
	
	font_putbuf16(p, "$fsType             ", &u16);

	tmp = u16 & 15;
	fprintf(fp, "  bit0-3 (%d) ", tmp);

	if(tmp == 0)
		fputs("Installable embedding\n", fp);
	else if(tmp == 2)
		fputs("Restricted License embedding\n", fp);
	else if(tmp == 4)
		fputs("Preview & Print embedding\n", fp);
	else if(tmp == 8)
		fputs("Editable embedding\n", fp);
	else
		fputs("?\n", fp);
	
	fprintf(fp, "  bit8 (%d) No subsetting\n", ((u16 & 0x0100) != 0));
	fprintf(fp, "  bit9 (%d) Bitmap embedding only\n", ((u16 & 0x0200) != 0));

	font_putbuf16(p, "-ySubscriptXSize    ", 0);
	font_putbuf16(p, "-ySubscriptYSize    ", 0);
	font_putbuf16(p, "-ySubscriptXOffset  ", 0);
	font_putbuf16(p, "-ySubscriptYOffset  ", 0);

	font_putbuf16(p, "-ySuperscriptXSize  ", 0);
	font_putbuf16(p, "-ySuperscriptYSize  ", 0);
	font_putbuf16(p, "-ySuperscriptXOffset", 0);
	font_putbuf16(p, "-ySuperscriptYOffset", 0);

	font_putbuf16(p, "-yStrikeoutSize     ", 0);
	font_putbuf16(p, "-yStrikeoutPosition ", 0);

	font_putbuf16(p, "$sFamilyClass       ", &u16);
	fprintf(p->put.fp, "  ClassID=%d, SubclassID=%d\n", u16 >> 8, u16 & 0xff);

	font_putbuf8_ar(p, "panose[10]", 10);
	font_putbuf32(p, "$ulUnicodeRange1    ", 0);
	font_putbuf32(p, "$ulUnicodeRange2    ", 0);
	font_putbuf32(p, "$ulUnicodeRange3    ", 0);
	font_putbuf32(p, "$ulUnicodeRange4    ", 0);
	font_putbuf32(p, "*achVendID          ", 0);

	font_putbuf16(p, "$fsSelection        ", &u16);
	font_put_bits_name(p, u16, 10,
		"ITALIC;UNDERSCORE;NEGATIVE;OUTLINED;STRIKEOUT;BOLD;REGULAR;"
		"USE_TYPO_METRICS;WWS;OBLIQUE");
	
	font_putbuf16(p, "$usFirstCharIndex   ", 0);
	font_putbuf16(p, "$usLastCharIndex    ", 0);
	font_putbuf16(p, "-sTypoAscender      ", 0);
	font_putbuf16(p, "-sTypoDescender     ", 0);
	font_putbuf16(p, "-sTypoLineGap       ", 0);
	font_putbuf16(p, "+usWinAscent        ", 0);
	font_putbuf16(p, "+usWinDescent       ", 0);

	//ver 1

	if(ver >= 1)
	{
		fprintf(p->put.fp, "---- ver 1 ----\n");
		font_putbuf32(p, "$ulCodePageRange1   ", 0);
		font_putbuf32(p, "$ulCodePageRange2   ", 0);
	}

	//ver 2

	if(ver >= 2)
	{
		fprintf(p->put.fp, "---- ver 2 ----\n");

		font_putbuf16(p, "-sxHeight           ", 0);
		font_putbuf16(p, "-sCapHeight         ", 0);
		font_putbuf16(p, "$usDefaultChar      ", 0);
		font_putbuf16(p, "$usBreakChar        ", 0);
		font_putbuf16(p, "+usMaxContext       ", 0);
	}

	//ver 5

	if(ver >= 5)
	{
		fprintf(p->put.fp, "---- ver 5 ----\n");
		font_putbuf16(p, "+usLowerOpticalPointSize", 0);
		font_putbuf16(p, "+usUpperOpticalPointSize", 0);
	}

	return font_closeput_ret(p);
}

/** maxp 出力 */

int font_put_maxp(Font *p)
{
	int ret;
	int32_t ver;

	ret = font_read_table(p, MAKE_TAG('m','a','x','p'));
	if(ret) return ret;

	//------ 出力

	ret = font_openput(p, "maxp.txt", " 'maxp' table");
	if(ret) return ret;

	font_putbuf32(p, "$version              ", &ver);
	font_putbuf16(p, "+numGlyphs            ", 0);

	//ver 1.0

	if(ver >= 0x00010000)
	{
		fprintf(p->put.fp, "---- ver 1.0 ----\n");

		font_putbuf16(p, "+maxPoints            ", 0);
		font_putbuf16(p, "+maxContours          ", 0);
		font_putbuf16(p, "+maxCompositePoints   ", 0);
		font_putbuf16(p, "+maxCompositeContours ", 0);
		font_putbuf16(p, "+maxZones             ", 0);
		font_putbuf16(p, "+maxTwilightPoints    ", 0);
		font_putbuf16(p, "+maxStorage           ", 0);
		font_putbuf16(p, "+maxFunctionDefs      ", 0);
		font_putbuf16(p, "+maxInstructionDefs   ", 0);
		font_putbuf16(p, "+maxStackElements     ", 0);
		font_putbuf16(p, "+maxSizeOfInstructions", 0);
		font_putbuf16(p, "+maxComponentElements ", 0);
		font_putbuf16(p, "+maxComponentDepth    ", 0);
	}

	return font_closeput_ret(p);
}

/** name 出力
 *
 * [!] UTF-16BE は UTF-8 で出力される。
 *     Mac 文字列は、128 以上の文字が含まれる場合、バイナリで出力。 */

int font_put_name(Font *p)
{
	int ret;
	uint16_t i,format,cnt,offset,ar16[6];
	FILE *fp;
	uint8_t *pbuf;

	ret = font_read_table(p, MAKE_TAG('n','a','m','e'));
	if(ret) return ret;

	//------ 出力

	ret = font_openput(p, "name.txt", " 'name' table");
	if(ret) return ret;

	fp = p->put.fp;

	font_putbuf16(p, "+format      ", &format);
	font_putbuf16(p, "+count       ", &cnt);
	font_putbuf16(p, "+stringOffset", &offset);

	fprintf(fp, "NameRecord nameRecord[count]:\n");
	fprintf(fp, "# u16 platformID,encodingID,languageID,nameID,length,offset\n");

	//NameRecord

	if(font_readbuf_skip(p, cnt * 6 * 2, &pbuf))
		goto END;

	for(i = 0; i < cnt; i++, pbuf += 12)
	{
		font_getbuf16_ar(pbuf, 6, ar16);
	
		fprintf(fp, "\n--[%d] plat=%d, enc=%d, lang=%d(0x%04X), name=%d, len=%d, offset=%d\n",
			i, ar16[0], ar16[1], ar16[2], ar16[2], ar16[3], ar16[4], ar16[5]);

		font_putbuf_string(p, ar16[0], offset, ar16[5], ar16[4]);
	}

	//format 1

	if(format == 1)
	{
		fprintf(fp, "--- format 1 ---\n");

		font_putbuf16(p, "+langTagCount", &cnt);
		fprintf(fp, "LangTagRecord langTagRecord[langTagCount]:\n");
		fprintf(fp, "# u16 length, offset\n\n");

		//LangTagRecord

		if(font_readbuf_skip(p, cnt * 2 * 2, &pbuf))
			goto END;

		for(i = 0; i < cnt; i++, pbuf += 4)
		{
			font_getbuf16_ar(pbuf, 2, ar16);
		
			fprintf(fp, "[%d] len=%d, offset=%d\n", i, ar16[0], ar16[1]);

			font_putbuf_string(p, 0, offset, ar16[1], ar16[0]);
		}
	}

END:
	return font_closeput_ret(p);
}


//=========== post


/* post: 文字列の数を取得 */

static int _post_get_strnum(uint8_t *ps,uint32_t size)
{
	int cnt = 0,len;

	while(size)
	{
		len = *(ps++);
		size--;

		if(size < len) break;

		cnt++;
		ps += len;
		size -= len;
	}

	return cnt;
}

/* post: 文字列のオフセットバッファ作成 */

static uint32_t *_post_create_offset(uint8_t *ps,int cnt)
{
	uint32_t *buf,*pd;
	uint8_t *pstop = ps;
	int len;

	if(!cnt) return NULL;

	pd = buf = (uint32_t *)app_malloc(cnt * 4);
	if(!buf) return NULL;

	for(; cnt; cnt--)
	{
		*(pd++) = ps - pstop;
		
		len = *ps;
		ps += 1 + len;
	}

	return buf;
}

/** post 出力 */

int font_put_post(Font *p)
{
	int ret,offcnt;
	int32_t ver;
	uint32_t *offbuf = 0;
	uint16_t i,num,v;
	FILE *fp;
	uint8_t *strbuf,*pbuf,*pstr;

	ret = font_read_table(p, MAKE_TAG('p','o','s','t'));
	if(ret) return ret;

	//------ 出力

	ret = font_openput(p, "post.txt", " 'post' table");
	if(ret) return ret;

	fp = p->put.fp;

	font_putbuf32(p, "$version           ", &ver);
	font_putbuf32(p, "%italicAngle       ", 0);
	font_putbuf16(p, "-underlinePosition ", 0);
	font_putbuf16(p, "-underlineThickness", 0);
	font_putbuf32(p, "+isFixedPitch      ", 0);
	font_putbuf32(p, "+minMemType42      ", 0);
	font_putbuf32(p, "+maxMemType42      ", 0);
	font_putbuf32(p, "+minMemType1       ", 0);
	font_putbuf32(p, "+maxMemType1       ", 0);

	//ver 2.0

	if(ver == 0x00020000)
	{
		fputs("---- ver 2.0 ----\n", fp);
		
		font_putbuf16(p, "+numGlyphs", &num);
		fputs("u16 glyphNameIndex[numGlyphs]:\n", fp);
		
		//glyphNameIndex

		if(font_readbuf_skip(p, num * 2, &pbuf))
			goto END;

		strbuf = p->readbuf_cur;

		offcnt = _post_get_strnum(strbuf, p->readbuf_remain);
		offbuf = _post_create_offset(strbuf, offcnt);

		for(i = 0; i < num; i++, pbuf += 2)
		{
			font_getbuf16(pbuf, &v);
			
			fprintf(fp, "[%d] %d ", i, v);

			if(v < 258)
				fputs("(Mac standard)\n", fp);
			else
			{
				v -= 258;
				if(v >= offcnt)
					fputs("!corrupted\n", fp);
				else
				{
					pstr = strbuf + offbuf[v];
					fprintf(fp, "'%.*s'\n", *pstr, pstr + 1);
				}
			}
		}
	}

END:
	app_free(offbuf);

	return font_closeput_ret(p);
}
