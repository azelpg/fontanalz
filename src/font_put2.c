/*$
fontanalz
Copyright (c) 2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/**********************************
 * フォント: hhea など
 **********************************/

#include <stdint.h>
#include <stdio.h>

#include "def.h"
#include "font.h"
#include "font_pv.h"


/* hmtx/vmtx 出力
 *
 * type: 'h' or 'v' */

static int _put_hmtx_vmtx(Font *p,int type,int num_offset)
{
	FILE *fp;
	uint8_t *pbuf;
	uint16_t num;
	int16_t v1,v2;
	int ret,i;

	//hhea/vhea: 数

	ret = font_read_table_val(p, MAKE_TAG(0,'h','e','a') | (type << 24), num_offset, 0, &num);
	if(ret) return ret;

	//hmtx

	ret = font_read_table(p, MAKE_TAG(0,'m','t','x') | (type << 24));
	if(ret) return ret;

	//------ 出力

	ret = font_openput(p,
		(type == 'h')? "hmtx.txt": "vmtx.txt",
		(type == 'h')? " 'hmtx' table": " 'vmtx' table");

	if(ret) return ret;

	fp = p->put.fp;

	fprintf(fp, "(%chea) u16 number: %d\n", type, num);
	fprintf(fp, "(maxp) u16 numGlyphs: %d\n\n", p->maxp_gnum);

	fputs("MetricsTable [number]:\n# s16 ", fp);

	if(type == 'h')
		fputs("advanceWidth, lsb\n", fp);
	else
		fputs("advanceHeight, topSideBearing\n", fp);

	//Metrics

	if(font_readbuf_skip(p, num * 4, &pbuf)) goto ERR;

	for(i = 0; i < num; i++, pbuf += 4)
	{
		font_getbuf_format(pbuf, "hh", &v1, &v2);
		
		fprintf(fp, "[%d] %d, %d\n", i, v1, v2);
	}

	//left/topSideBearings

	i = p->maxp_gnum - num;

	if(i > 0)
	{
		if(type == 'h')
			fputs("\ns16 leftSideBearings", fp);
		else
			fputs("\ns16 topSideBearing", fp);

		fprintf(fp, "[numGlyphs - number (%d)]:\n", i);

		font_putbuf16_ar(p, i, 1);
	}

ERR:
	return font_closeput_ret(p);
}

/** hhea 出力 */

int font_put_hhea(Font *p)
{
	int ret;

	ret = font_read_table(p, MAKE_TAG('h','h','e','a'));
	if(ret) return ret;

	//------ 出力

	ret = font_openput(p, "hhea.txt", " 'hhea' table");
	if(ret) return ret;

	font_putbuf16(p, "+majorVersion       ", 0);
	font_putbuf16(p, "+minorVersion       ", 0);
	font_putbuf16(p, "-ascender           ", 0);
	font_putbuf16(p, "-descender          ", 0);
	font_putbuf16(p, "-lineGap            ", 0);
	font_putbuf16(p, "+advanceWidthMax    ", 0);
	font_putbuf16(p, "-minLeftSideBearing ", 0);
	font_putbuf16(p, "-minRightSideBearing", 0);
	font_putbuf16(p, "-xMaxExtent         ", 0);
	font_putbuf16(p, "-caretSlopeRise     ", 0);
	font_putbuf16(p, "-caretSlopeRun      ", 0);
	font_putbuf16(p, "-caretOffset        ", 0);

	fputs("s16 reserved[4]         :", p->put.fp);
	font_putbuf16_ar(p, 4, 0);

	font_putbuf16(p, "-metricDataFormat   ", 0);
	font_putbuf16(p, "+numberOfHMetrics   ", 0);

	return font_closeput_ret(p);
}

/** hmtx 出力 */

int font_put_hmtx(Font *p)
{
	return _put_hmtx_vmtx(p, 'h', 17 * 2);
}

/** vhea 出力 */

int font_put_vhea(Font *p)
{
	uint32_t ver;
	int ret;

	ret = font_read_table(p, MAKE_TAG('v','h','e','a'));
	if(ret) return ret;

	//------ 出力

	ret = font_openput(p, "vhea.txt", " 'vhea' table");
	if(ret) return ret;

	font_putbuf32(p, "$version             ", &ver);

	if(ver == 0x00010000)
	{
		font_putbuf16(p, "-ascent              ", 0);
		font_putbuf16(p, "-descent             ", 0);
		font_putbuf16(p, "-lineGap             ", 0);
	}
	else
	{
		font_putbuf16(p, "-vertTypoAscender    ", 0);
		font_putbuf16(p, "-vertTypoDescender   ", 0);
		font_putbuf16(p, "-vertTypoLineGap     ", 0);
	}

	font_putbuf16(p, "-advanceHeightMax    ", 0);
	font_putbuf16(p, "-minTopSideBearing   ", 0);
	font_putbuf16(p, "-minBottomSideBearing", 0);
	font_putbuf16(p, "-yMaxExtent          ", 0);
	font_putbuf16(p, "-caretSlopeRise      ", 0);
	font_putbuf16(p, "-caretSlopeRun       ", 0);
	font_putbuf16(p, "-caretOffset         ", 0);

	fputs("s16 reserved[4]          :", p->put.fp);
	font_putbuf16_ar(p, 4, 0);

	font_putbuf16(p, "-metricDataFormat    ", 0);
	font_putbuf16(p, "+numOfLongVerMetrics ", 0);

	return font_closeput_ret(p);
}

/** vmtx 出力 */

int font_put_vmtx(Font *p)
{
	return _put_hmtx_vmtx(p, 'v', 34);
}

/** VORG 出力 */

int font_put_vorg(Font *p)
{
	FILE *fp;
	uint8_t *pbuf;
	int i,ret;
	uint16_t num,gid;
	int16_t orig;

	ret = font_read_table(p, MAKE_TAG('V','O','R','G'));
	if(ret) return ret;

	//------ 出力

	ret = font_openput(p, "VORG.txt", " 'VORG' table");
	if(ret) return ret;

	fp = p->put.fp;

	font_putbuf16(p, "+majorVersion         ", 0);
	font_putbuf16(p, "+minorVersion         ", 0);
	font_putbuf16(p, "-defaultVertOriginY   ", 0);
	font_putbuf16(p, "+numVertOriginYMetrics", &num);
	fputs("vertOriginYMetrics[numVertOriginYMetrics]:\n", fp);
	fputs("# u16 glyphIndex, s16 vertOriginY\n", fp);

	if(font_readbuf_skip(p, num * 4, &pbuf)) goto ERR;

	for(i = 0; i < num; i++, pbuf += 4)
	{
		font_getbuf_format(pbuf, "hh", &gid, &orig);

		fprintf(fp, "[%d] ", i);
		font_put_gid_unicode(p, gid);
		fprintf(fp, ", %d\n", orig);
	}

ERR:
	return font_closeput_ret(p);
}

/** loca 出力 */

int font_put_loca(Font *p)
{
	FILE *fp;
	uint8_t *pbuf;
	int i,ret,size,num;
	uint32_t v32;
	uint16_t v16;

	ret = font_read_table(p, MAKE_TAG('l','o','c','a'));
	if(ret) return ret;

	size = p->loca_offset_size;

	//------ 出力

	ret = font_openput(p, "loca.txt", " 'loca' table");
	if(ret) return ret;

	fp = p->put.fp;
	num = p->maxp_gnum + 1;

	fprintf(fp, "(head) indexToLocFormat: Offset%d\n\n", size * 8);

	if(size == 2)
		fputs("# raw (offset)\n\n", fp);

	//offset
	
	if(font_readbuf_skip(p, num * size, &pbuf)) goto ERR;

	for(i = 0; i < num; i++, pbuf += size)
	{
		if(size == 2)
		{
			font_getbuf16(pbuf, &v16);
			fprintf(fp, "[%d] %d (0x%08X)\n", i, v16, v16 * 2);
		}
		else
		{
			font_getbuf32(pbuf, &v32);
			fprintf(fp, "[%d] 0x%08X\n", i, v32);
		}
	}

ERR:
	return font_closeput_ret(p);
}



