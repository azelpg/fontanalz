/*$
fontanalz
Copyright (c) 2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*************************************
 * フォント: BASE
 *************************************/

#include <stdint.h>
#include <stdio.h>

#include "def.h"
#include "font.h"
#include "font_pv.h"


//======================
// raw
//======================


/* BaseCoord (直接バッファ読み込み) */

static void _put_basecoord(Font *p,FILE *fp,uint32_t offset)
{
	uint8_t *pbuf;
	uint16_t format,v[2];
	int16_t coord;

	if(offset + 4 > p->readbuf_size) return;

	pbuf = p->readbuf + offset;

	font_getbuf_format(pbuf, "hh", &format, &coord);

	fprintf(fp, "{BaseCoord} format=%d, coord=%d", format, coord);

	switch(format)
	{
		case 2:
			if(offset + 8 > p->readbuf_size) return;

			font_getbuf_format(pbuf + 4, "hh", v, v + 1);
			fprintf(fp, ", refGID=%d, point=%d", v[0], v[1]);
			break;
		case 3:
			if(offset + 6 > p->readbuf_size) return;

			font_getbuf16(pbuf + 4, v);
			fprintf(fp, ", deviceTable=%d", v[0]);
			break;
	}

	fputc('\n', fp);
}

/* MinMax */

static void _put_minmax(Font *p,FILE *fp,uint32_t offset)
{
	uint16_t off,cnt,i,offar[2];
	uint32_t tag;

	if(font_readbuf_setpos(p, offset)) return;

	fputs("\n-- MinMax --\n", fp);

	font_putbuf16(p, "+minCoord       ", &off);
	if(off) _put_basecoord(p, fp, offset + off);

	font_putbuf16(p, "+maxCoord       ", &off);
	if(off) _put_basecoord(p, fp, offset + off);

	font_putbuf16(p, "+featMinMaxCount", &cnt);
	fputs("FeatMinMaxRecord [featMinMaxCount]:\n", fp);
	fputs("# u32 featureTableTag, u16 minCoord, u16 maxCoord\n", fp);

	for(i = 0; i < cnt; i++)
	{
		font_readbuf32(p, &tag);
		font_readbuf16(p, offar);
		font_readbuf16(p, offar + 1);

		fprintf(fp, "[%d] ", i);
		font_put_tagname(p, tag);
		fprintf(fp, ", %d, %d\n", offar[0], offar[1]);
	}
}

/* BaseValue */

static void _put_basevalue(Font *p,FILE *fp,uint32_t offset)
{
	uint16_t cnt,i,off;
	uint8_t *pbuf;

	if(font_readbuf_setpos(p, offset)) return;

	fputs("\n-- BaseValue --\n", fp);

	font_putbuf16(p, "+defaultBaselineIndex", 0);
	font_putbuf16(p, "+baseCoordCount      ", &cnt);
	fputs("u16 baseCoords[baseCoordCount]:\n", fp);

	if(font_readbuf_skip(p, cnt * 2, &pbuf)) return;

	for(i = 0; i < cnt; i++, pbuf += 2)
	{
		font_getbuf16(pbuf, &off);
		fprintf(fp, "[%d] %d\n", i, off);

		_put_basecoord(p, fp, offset + off);
	}
}

/* BaseScript */

static void _put_basescript(Font *p,FILE *fp,uint32_t offset)
{
	uint8_t *pbuf;
	uint16_t cnt,i,off16,off[2];
	uint32_t tag;

	if(font_readbuf_setpos(p, offset)) return;

	fputs("--- BaseScript ---\n", fp);

	font_putbuf16(p, "+baseValuesOffset   ", off);
	font_putbuf16(p, "+defaultMinMaxOffset", off + 1);
	font_putbuf16(p, "+baseLangSysCount   ", &cnt);

	//BaseLangSysRecord

	if(cnt)
	{
		fputs("BaseLangSysRecord [baseLangSysCount]:\n", fp);
		fputs("# u32 baseLangSysTag, u16 minMaxOffset\n", fp);

		//BaseLangSysRrecord

		if(font_readbuf_skip(p, cnt * 6, &pbuf)) return;

		for(i = 0; i < cnt; i++, pbuf += 6)
		{
			font_getbuf_format(pbuf, "ih", &tag, &off16);

			fprintf(fp, "[%d] ", i);
			font_put_tagname(p, tag);
			fprintf(fp, ", %d\n", off16);

			if(off16) _put_minmax(p, fp, offset + off16);
		}
	}

	//BaseValue

	if(off[0])
		_put_basevalue(p, fp, offset + off[0]);

	//defaultMinMax

	if(off[1])
		_put_minmax(p, fp, offset + off[1]);
}

/* BaseScriptList */

static void _put_basescriptlist(Font *p,FILE *fp,uint32_t offset)
{
	uint8_t *pbuf;
	uint32_t tag;
	uint16_t i,cnt,off16;

	if(font_readbuf_setpos(p, offset)) return;

	fputs("\n--- BaseScriptList ---\n\n", fp);

	font_putbuf16(p, "+baseScriptCount", &cnt);
	fputs("BaseScriptRecord [baseScriptCount]:\n", fp);
	fputs("# u32 baseScriptTag, u16 baseScriptOffset\n", fp);

	if(font_readbuf_skip(p, cnt * 6, &pbuf)) return;

	for(i = 0; i < cnt; i++, pbuf += 6)
	{
		font_getbuf_format(pbuf, "ih", &tag, &off16);

		fprintf(fp, "\n** [%d] ", i);
		font_put_tagname(p, tag);
		fprintf(fp, ", %d\n\n", off16);

		_put_basescript(p, fp, offset + off16);
	}
}

/* BaseTagList */

static void _put_basetaglist(Font *p,FILE *fp,uint32_t offset)
{
	uint16_t cnt;
	uint32_t tag;
	uint8_t *pbuf;

	if(font_readbuf_setpos(p, offset)) return;

	fputs("\n--- BaseTagList ---\n\n", fp);

	font_putbuf16(p, "+baseTagCount", &cnt);
	fputs("u32 baselineTags[baseTagCount]:\n", fp);

	if(font_readbuf_skip(p, cnt * 4, &pbuf)) return;

	for(; cnt; cnt--, pbuf += 4)
	{
		font_getbuf32(pbuf, &tag);

		fputc(' ', fp);
		font_put_tagname(p, tag);
	}

	fputc('\n', fp);
}

/* Axis */

static void _put_axis(Font *p,FILE *fp,uint32_t offset_top,const char *header)
{
	uint16_t tag,script;

	if(font_readbuf_setpos(p, offset_top)) return;

	font_put_header(p, header);

	font_putbuf16(p, "+baseTagListOffset   ", &tag);
	font_putbuf16(p, "+baseScriptListOffset", &script);

	//BaseTagList

	if(tag)
		_put_basetaglist(p, fp, offset_top + tag);

	//BaseScriptList

	_put_basescriptlist(p, fp, offset_top + script);
}


//=============================
// 通常 (ベースラインのリスト)
//=============================


/* BaseScriptList 以降*/

static void _put_basescriptlist_list(Font *p,FILE *fp,uint32_t offset_top,uint8_t *pbasetag)
{
	uint8_t *pbuf,*pbuf2,*pbuf3,*ptag;
	uint32_t tag,offset;
	uint16_t i,cnt,off16,v16,cnt2;

	//BaseScriptList

	if(font_readbuf_setpos(p, offset_top)) return;

	font_readbuf16(p, &cnt);

	if(font_readbuf_skip(p, cnt * 6, &pbuf)) return;

	for(i = 0; i < cnt; i++, pbuf += 6)
	{
		font_getbuf_format(pbuf, "ih", &tag, &off16);

		fprintf(fp, "---- [%d] ", i);
		font_put_tagname(p, tag);
		fprintf(fp, " ----\n\n");

		//BaseScript

		offset = offset_top + off16;

		if(font_readbuf_getposptr(p, offset, 2, &pbuf2)) return;

		font_getbuf16(pbuf2, &off16);
		if(!off16)
		{
			fprintf(fp, "no baseline\n\n");
			continue;
		}

		//BaseValue

		offset += off16;

		if(font_readbuf_setpos(p, offset)) return;

		font_readbuf16(p, &v16);
		font_readbuf16(p, &cnt2);

		fprintf(fp, "default index: %d\n", v16);

		if(font_readbuf_skip(p, cnt2 * 2, &pbuf2)) return;

		ptag = pbasetag;

		for(; cnt2; cnt2--, pbuf2 += 2, ptag += 4)
		{
			font_getbuf16(pbuf2, &off16);

			//BaseCoord

			if(font_readbuf_getposptr(p, offset + off16, 4, &pbuf3)) return;

			font_getbuf16(pbuf3, &v16);

			if(v16 >= 1 && v16 <= 3)
			{
				font_getbuf16(pbuf3 + 2, &v16);
				font_getbuf32(ptag, &tag);
				
				font_put_tagname(p, tag);
				fprintf(fp, " : %d\n", (int16_t)v16);
			}
		}

		if(i != cnt - 1) fputc('\n', fp);
	}
}

/* Axis */

static void _put_axis_list(Font *p,FILE *fp,uint32_t offset_top,const char *header)
{
	uint16_t tag,script,cnt;
	uint8_t *pbasetag;

	if(font_readbuf_setpos(p, offset_top)) return;

	font_put_header(p, header);

	font_readbuf16(p, &tag);
	font_readbuf16(p, &script);

	if(!tag)
		fprintf(fp, "no baseline\n");

	//BaseTagList
	//(タグの位置を記録)

	if(font_readbuf_setpos(p, offset_top + tag))
		return;

	font_readbuf16(p, &cnt);

	if(font_readbuf_skip(p, cnt * 4, &pbasetag)) return;

	//BaseScriptList

	_put_basescriptlist_list(p, fp, offset_top + script, pbasetag);
}



//======================
// main
//======================


/** BASE 出力 */

int font_put_base(Font *p)
{
	int ret;
	uint16_t min,off16[2];
	FILE *fp;

	ret = font_read_table(p, MAKE_TAG('B','A','S','E'));
	if(ret) return ret;

	//------

	ret = font_openput(p, "BASE.txt", " 'BASE' table");
	if(ret) return ret;

	fp = p->put.fp;

	font_putbuf16(p, "+major          ", 0);
	font_putbuf16(p, "+minor          ", &min);
	font_putbuf16(p, "+horizAxisOffset", off16);
	font_putbuf16(p, "+vertAxisOffset ", off16 + 1);

	if(min >= 1)
		font_putbuf32(p, "+itemVarStoreOffset", 0);

	//

	if(g_app.optflags & OPTF_RAW)
	{
		//生データ
		
		if(off16[0])
			_put_axis(p, fp, off16[0], "Axis (horz)");
		
		if(off16[1])
			_put_axis(p, fp, off16[1], "Axis (vert)");
	}
	else
	{
		//ベースラインのリスト
		
		if(off16[0])
			_put_axis_list(p, fp, off16[0], "horz");

		if(off16[1])
			_put_axis_list(p, fp, off16[1], "vert");
	}
	
	return font_closeput_ret(p);
}
