/*$
fontanalz
Copyright (c) 2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*************************************
 * フォント: CFF
 *************************************/

#include <stdint.h>
#include <stdio.h>

#include "def.h"
#include "font.h"
#include "font_pv.h"
#include "font_cff.h"


//-----------------

#define CFF_KEY2(k2)  (0x0c00 | (k2))

//INDEX

typedef struct
{
	int no;
	uint32_t offset1,offset2,	//データの INDEX でのオフセット位置
		size;
	uint8_t *data;
}cff_index;

//DICT key

typedef struct
{
	uint16_t key;
	const char *name;
}keyname;

static const keyname g_keyname_topdict[] = {
	{0, "version"}, {1, "Notice"}, {2, "FullName"}, {3, "FamilyName"}, {4, "Weight"},
	{5, "FontBBox"}, {13,"UniqueID"}, {14,"XUID"}, {15,"charset"}, {16,"Encoding"},
	{17,"CharStrings"}, {18,"Private"},

	{CFF_KEY2(0),"Copyright"}, {CFF_KEY2(1),"isFixedPitch"}, {CFF_KEY2(2),"ItalicAngle"},
	{CFF_KEY2(3),"UnderlinePosition"}, {CFF_KEY2(4),"UnderlineThickness"}, {CFF_KEY2(5),"PaintType"},
	{CFF_KEY2(6),"CharstringType"}, {CFF_KEY2(7),"FontMatrix"}, {CFF_KEY2(8),"StrokeWidth"},
	{CFF_KEY2(20),"SyntheticBase"}, {CFF_KEY2(21),"PostScript"}, {CFF_KEY2(22),"BaseFontName"},
	{CFF_KEY2(23),"BaseFontBlend"},

	{CFF_KEY2(30),"ROS"}, {CFF_KEY2(31),"CIDFontVersion"}, {CFF_KEY2(32),"CIDFontRevision"},
	{CFF_KEY2(33),"CIDFontType"}, {CFF_KEY2(34),"CIDCount"}, {CFF_KEY2(35),"UIDBase"},
	{CFF_KEY2(36),"FDArray"}, {CFF_KEY2(37),"FDSelect"}, {CFF_KEY2(38),"FontName"},
	{0,0}
};

static const keyname g_keyname_private[] = {
	{6,"BlueValues"}, {7,"OtherBlues"}, {8,"FamilyBlues"}, {9,"FamilyOtherBlues"}, {10,"StdHW"}, {11,"StdVW"},
	{19,"Subrs"}, {20,"defaultWidthX"}, {21,"nominalWidthX"},

	{CFF_KEY2(9),"BlueScale"}, {CFF_KEY2(10),"BlueShift"}, {CFF_KEY2(11),"BlueFuzz"},
	{CFF_KEY2(12),"StemSnapH"}, {CFF_KEY2(13),"StemSnapV"}, {CFF_KEY2(14),"ForceBold"},
	{CFF_KEY2(17),"LanguageGroup"}, {CFF_KEY2(18),"ExpansionFactor"}, {CFF_KEY2(19),"initialRandomSeed"},
	{0,0}
};

//-----------------


/* keyname から検索して出力 */

static void _put_dict_keyname(FILE *fp,const keyname *list,uint16_t key)
{
	for(; list->name; list++)
	{
		if(list->key == key)
		{
			fprintf(fp, " <%s>", list->name);
			break;
		}
	}
}

/* サブルーチンのデータ出力 */

static void _put_subr_data(Font *p,uint8_t *buf,uint32_t size)
{
	FILE *fp = p->put.fp;

	for(; size; size--, buf++)
		fprintf(fp, " %02X", *buf);

	fputc('\n', fp);
}

/* INDEX 読み込み
 *
 * func: データごとに呼ばれる関数 */

static int _read_INDEX(Font *p,
	int (*func)(Font *p,FILE *fp,cff_index *dat),const char *header)
{
	FILE *fp = p->put.fp;
	uint8_t *data_top,*poff;
	uint16_t cnt,i;
	uint8_t offsize;
	uint32_t off1,off2,offdata,offdatasize;
	int ret;
	cff_index idat;

	if(header)
		font_put_header(p, header);
	
	font_putbuf16(p, "+count  ", &cnt);
	if(cnt == 0) return 0;
	
	font_putbuf8(p,  "+offsize", &offsize);

	//

	offdatasize = (cnt + 1) * offsize;

	data_top = p->readbuf_cur + offdatasize;
	offdata = font_readbuf_getpos(p) + offdatasize;

	if(font_readbuf_skip(p, offdatasize, &poff))
		return FONTERR_READ;

	//

	font_getbuf_nsize(poff, offsize, &off1);
	poff += offsize;

	for(i = 0; i < cnt; i++, poff += offsize)
	{
		font_getbuf_nsize(poff, offsize, &off2);
	
		if(offdata + off2 - 1 > p->readbuf_size)
			return FONTERR_READ;

		idat.no = i;
		idat.offset1 = off1;
		idat.offset2 = off2;
		idat.size = off2 - off1;
		idat.data = data_top + off1 - 1;

		//関数内で readbuf の位置を変更しても良い

		ret = (func)(p, fp, &idat);
		if(ret) return ret;

		off1 = off2;
	}

	//次の位置へ
	//[!] Private DICT の subr の場合は位置が終端になる

	off1 = offdata + off2 - 1;

	if(font_readbuf_setpos(p, off1))
		return FONTERR_READ;

	return 0;
}

/* バッファから、DICT データを処理して表示
 *
 * キーと値のペアが複数。
 *
 * func: キーの値が確定した時に呼ばれる関数。0 でなし
 * - key: 上位バイトが 12 で 2byte key
 * - val1: キーの直前の整数値
 * - val2: val1 の前の整数値 */

static int _put_DICT(Font *p,FILE *fp,uint8_t *buf,uint32_t size,
	void (*func)(Font *p,uint16_t key,int val1,int val2))
{
	int32_t val = 0,val2 = 0,pos;
	uint16_t key;
	uint8_t b0;

	while(size)
	{
		b0 = *(buf++);
		size--;

		//---- キー

		if(b0 <= 21)
		{
			fprintf(fp, "key{%d", b0);
		
			if(b0 != 12)
				//1byte
				key = b0;
			else
			{
				//2byte

				if(!size) return FONTERR_READ;

				b0 = *(buf++);
				size--;

				key = (12 << 8) | b0;

				fprintf(fp, ",%d", b0);
			}

			fputs("}", fp);

			if(func)
				(func)(p, key, val, val2);

			fputc('\n', fp);
			
			continue;
		}

		//---- 実数

		if(b0 == 30)
		{
			pos = 0;

			while(size)
			{
				//4bit 値
				
				if(pos & 1)
					val = b0 & 0xf;
				else
				{
					if(!size) return FONTERR_READ;
					
					b0 = *(buf++);
					size--;

					val = b0 >> 4;
				}

				pos ^= 1;

				//

				if(val == 15)
					break;
				else if(val >= 0 && val <= 9)
					fputc('0' + val, fp);
				else if(val == 10)
					fputc('.', fp);
				else if(val == 11)
					fputc('E', fp);
				else if(val == 12)
					fputs("E-", fp);
				else if(val == 14)
					fputc('-', fp);
			}

			fputs(", ", fp);
			continue;
		}

		//---- 整数値

		val2 = val;

		if(b0 >= 32 && b0 <= 246)
			//-107..107
			val = b0 - 139;
		else if(b0 >= 247 && b0 <= 250)
		{
			//108..1131

			if(!size) return FONTERR_READ;

			val = ((b0 - 247) << 8) + buf[0] + 108;

			buf++;
			size--;
		}
		else if(b0 >= 251 && b0 <= 254)
		{
			//-1131..-108

			if(!size) return FONTERR_READ;

			val = -((b0 - 251) << 8) - buf[0] - 108;

			buf++;
			size--;
		}
		else if(b0 == 28)
		{
			//-32768..32767
			
			if(size < 2) return FONTERR_READ;

			val = (buf[0] << 8) | buf[1];
			buf += 2;
			size -= 2;
		}
		else if(b0 == 29)
		{
			//5byte

			if(size < 4) return FONTERR_READ;

			val = (int32_t)((uint32_t)buf[0] << 24) | (buf[1] << 16) | (buf[2] << 8) | buf[3];

			buf += 4;
			size -= 4;
		}

		fprintf(fp, "%d, ", val);
	}

	return 0;
}


//=======================
// INDEX/DICT 関数
//=======================


/* Name INDEX */

static int _func_index_name(Font *p,FILE *fp,cff_index *dat)
{
	fprintf(fp, "-- [%d] offset=%u, size=%u\n", dat->no, dat->offset1, dat->size);
	fwrite(dat->data, 1, dat->size, fp);
	fputc('\n', fp);

	return 0;
}

/* String INDEX */

static int _func_index_string(Font *p,FILE *fp,cff_index *dat)
{
	fprintf(fp, "-- [%d] <SID %d> offset=%u, size=%u\n", dat->no, dat->no + 391, dat->offset1, dat->size);
	fwrite(dat->data, 1, dat->size, fp);
	fputc('\n', fp);

	return 0;
}

/* Top DICT データ */

static void _func_dict_topDICT(Font *font,uint16_t key,int val,int val2)
{
	cffdat *p = font->pcffdat;

	_put_dict_keyname(font->put.fp, g_keyname_topdict, key);

	//各オフセット値などを取得

	if(key == 15)
		p->offset_charset = val;
	else if(key == 17)
		p->offset_charstrings = val;
	else if(key == 18)
	{
		p->private_offset = val;
		p->private_size = val2;
	}
	else if(key == CFF_KEY2(36))
		p->offset_fdarray = val;
	else if(key == CFF_KEY2(37))
		p->offset_fdselect = val;
}

/* Top DICT INDEX */

static int _func_index_topDICT(Font *p,FILE *fp,cff_index *dat)
{
	fprintf(fp, "--- [%d] --- (offset=%u, size=%u)\n", dat->no, dat->offset1, dat->size);

	return _put_DICT(p, fp, dat->data, dat->size, _func_dict_topDICT);
}

/* Subr INDEX */

static int _func_index_subr(Font *p,FILE *fp,cff_index *dat)
{
	fprintf(fp, "-- [%d] offset=%u, size=%u\n", dat->no, dat->offset1, dat->size);

	_put_subr_data(p, dat->data, dat->size);

	return 0;
}

/* Private DICT データ */

static void _func_dict_private(Font *p,uint16_t key,int val,int val2)
{
	_put_dict_keyname(p->put.fp, g_keyname_private, key);

	if(key == 19) //Subr
		p->pcffdat->offset_local_subr = val;
}

/* Font DICT データ */

static void _func_dict_font(Font *p,uint16_t key,int val,int val2)
{
	_put_dict_keyname(p->put.fp, g_keyname_topdict, key);

	if(key == 18) //Private
	{
		p->pcffdat->tmp_offset = val;
		p->pcffdat->tmp_size = val2;
	}
}

/* (FDArray) Font DICT INDEX */

static int _func_index_fontdict(Font *p,FILE *fp,cff_index *dat)
{
	cffdat *cff = p->pcffdat;
	uint8_t *pbuf;
	int ret;

	fprintf(fp, "\n--- [%d] --- (offset=%u, size=%u)\n\n", dat->no, dat->offset1, dat->size);

	cff->tmp_offset = 0;

	ret = _put_DICT(p, fp, dat->data, dat->size, _func_dict_font);
	if(ret) return ret;

	//Private

	if(cff->tmp_offset)
	{
		fputs("\n== Private DICT ==\n", fp);

		if(font_readbuf_getposptr(p, cff->tmp_offset, cff->tmp_size, &pbuf))
			return FONTERR_READ;

		cff->offset_local_subr = 0;
		
		ret = _put_DICT(p, fp, pbuf, cff->tmp_size, _func_dict_private);
		if(ret) return ret;

		//Subr

		if(cff->offset_local_subr
			&& (g_app.optflags & OPTF_VERBOSE))
		{
			fputs("\n== Local Subr INDEX ==\n", fp);

			if(font_readbuf_setpos(p, cff->tmp_offset + cff->offset_local_subr))
				return FONTERR_READ;
		
			ret = _read_INDEX(p, _func_index_subr, 0);
			if(ret) return ret;
		}
	}

	return 0;
}

/* CharStrings INDEX */

static int _func_index_charstring(Font *p,FILE *fp,cff_index *dat)
{
	int ret = 0;

	if(g_app.gid_top < 0 || (g_app.gid_top <= dat->no && dat->no <= g_app.gid_end))
	{
		if(g_app.optflags & OPTF_RAW)
			ret = _func_index_subr(p, fp, dat);
		else
		{
			fputs("\n--- [", fp);
			font_put_gid_unicode(p, dat->no);
			fprintf(fp, "] --- (offset=%u, size=%u)\n\n", dat->offset1, dat->size);

			ret = font_proc_charstring(p, dat->no, dat->data, dat->size);
		}
	}

	return ret;
}


//===================


/* グリフ数取得 */

static int _get_glyph_num(Font *p)
{
	uint8_t *pbuf;
	uint16_t v16;

	if(font_readbuf_getposptr(p, p->pcffdat->offset_charstrings, 2, &pbuf))
		return 1;

	font_getbuf16(pbuf, &v16);

	p->pcffdat->glyph_num = v16;

	return 0;
}

/* Charsets */

static int _put_charsets(Font *p,cffdat *dat,int fraw)
{
	FILE *fp = p->put.fp;
	uint8_t *pbuf;
	uint16_t u16[2];
	uint8_t format,u8;
	int num,i,j,id;

	if(dat->offset_charset <= 2) return 0;

	font_put_header(p, "Charsets");

	fprintf(fp, "<glyph num: %d>\n\n", dat->glyph_num);

	if(font_readbuf_setpos(p, dat->offset_charset)
		|| font_putbuf8(p, "+format", &format)
		|| format > 2)
		return FONTERR_READ;

	//

	num = dat->glyph_num;

	if(!fraw)
		fputs("# GID: SID/CID\n", fp);

	if(format == 0)
	{
		//format 0: 配列

		if(fraw)
		{
			fputs("u16 glyph[nGlyphs - 1]:\n", fp);

			if(font_putbuf16_ar(p, num - 1, 0))
				return FONTERR_READ;
		}
		else
		{
			if(font_readbuf_skip(p, (num - 1) * 2, &pbuf))
				return FONTERR_READ;

			for(i = 1; i < num; i++, pbuf += 2)
			{
				font_getbuf16(pbuf, u16);
				fprintf(fp, "%d: %d\n", i, u16[0]);
			}
		}
	}
	else if(format == 1)
	{
		//format 1: 範囲 (8bit)

		if(fraw)
		{
			fputs("struct Range1[]:\n# u16 first, u8 nLeft\n", fp);

			for(i = 1; i < num; i += u8 + 1)
			{
				if(font_readbuf16(p, u16)
					|| font_readbuf8(p, &u8))
					return FONTERR_READ;

				fprintf(fp, "%d, %d\n", u16[0], u8);
			}
		}
		else
		{
			for(i = 1; i < num; )
			{
				if(font_readbuf16(p, u16)
					|| font_readbuf8(p, &u8))
					return FONTERR_READ;

				for(j = u8 + 1, id = u16[0]; j; j--, i++, id++)
					fprintf(fp, "%d: %d\n", i, id);
			}
		}
	}
	else
	{
		//format 2: 範囲 (16bit)

		if(fraw)
		{
			fputs("struct Range2[]:\n# u16 first, u16 nLeft\n", fp);

			for(i = 1; i < num; i += u16[1] + 1)
			{
				if(font_readbuf_format(p, "hh", u16, u16 + 1))
					return FONTERR_READ;

				fprintf(fp, "%d, %d\n", u16[0], u16[1]);
			}
		}
		else
		{
			for(i = 1; i < num; )
			{
				if(font_readbuf_format(p, "hh", u16, u16 + 1))
					return FONTERR_READ;

				for(j = u16[1] + 1, id = u16[0]; j; j--, i++, id++)
					fprintf(fp, "%d: %d\n", i, id);
			}
		}
	}

	return 0;
}

/* Private DICT */

static int _put_private_dict(Font *p,uint32_t offset,int size)
{
	uint8_t *pbuf;
	int ret;

	if(font_readbuf_setpos(p, offset)
		|| font_readbuf_skip(p, size, &pbuf))
		return FONTERR_READ;
	
	font_put_header(p, "Private DICT");

	ret = _put_DICT(p, p->put.fp, pbuf, size, _func_dict_private);
	if(ret) return ret;

	//Local サブルーチン

	if(p->pcffdat->offset_local_subr
		&& (g_app.optflags & OPTF_VERBOSE))
	{
		if(font_readbuf_setpos(p, offset + p->pcffdat->offset_local_subr))
			return FONTERR_READ;
	
		ret = _read_INDEX(p, _func_index_subr, "Local Subr INDEX");
		if(ret) return ret;
	}

	return 0;
}

/* FDSelect */

static int _put_fdselect(Font *p,int fraw)
{
	FILE *fp = p->put.fp;
	uint8_t *pbuf;
	int i,gnum;
	uint16_t num,first,next;
	uint8_t format,fd;

	font_put_header(p, "FDSelect");

	gnum = p->pcffdat->glyph_num;

	fprintf(fp, "<glyph num: %d>\n\n", gnum);

	if(font_putbuf8(p, "+format", &format))
		return FONTERR_READ;

	if(!fraw) fputs("\n# GID: FD index\n", fp);

	//

	if(format == 0)
	{
		//format 0

		if(fraw)
		{
			fputs("u8 fds[nGlyphs]:\n", fp);
			font_putbuf8_ar(p, 0, gnum);
		}
		else
		{
			if(font_readbuf_skip(p, gnum, &pbuf)) return FONTERR_READ;

			for(i = 0; i < gnum; i++, pbuf++)
				fprintf(fp, "%d: %d\n", i, *pbuf);
		}
	}
	else if(format == 3)
	{
		//format 3

		if(fraw)
		{
			if(font_putbuf16(p, "+nRanges", &num)
				|| font_readbuf_skip(p, num * 3, &pbuf))
				return FONTERR_READ;

			fputs("struct Range3[nRanges]:\n# u16 first, u8 fd\n", fp);

			for(i = 0; i < num; i++, pbuf += 3)
			{
				font_getbuf_format(pbuf, "hb", &first, &fd);

				fprintf(fp, "[%d] %d, %d\n", i, first, fd);
			}

			font_putbuf16(p, "+sentinel", 0);
		}
		else
		{
			if(font_readbuf16(p, &num)
				|| font_readbuf_skip(p, num * 3 + 2, &pbuf))
				return FONTERR_READ;

			font_getbuf16(pbuf, &first);
			pbuf += 2;

			for(; num; num--, pbuf += 3)
			{
				font_getbuf_format(pbuf, "bh", &fd, &next);

				fprintf(fp, "%d-%d: %d\n", first, next - 1, fd);

				first = next;
			}
		}
	}
	else
		return FONTERR_INVALID;

	return 0;
}


//===========================
// main
//===========================


/* メイン処理 (基本) */

static int _main_proc(Font *p,cffdat *cff)
{
	int ret,fraw,fverbose;
	uint8_t hs;

	fverbose = g_app.optflags & OPTF_VERBOSE;
	fraw = g_app.optflags & OPTF_RAW;

	//ヘッダ

	font_putbuf8(p, "+major  ", 0);
	font_putbuf8(p, "+minor  ", 0);
	font_putbuf8(p, "+hdrSize", &hs);

	if(hs != 4) return FONTERR_INVALID;
	
	font_putbuf8(p, "+offSize", 0);

	//---- 順に並んでいる

	//Name INDEX

	ret = _read_INDEX(p, _func_index_name, "Name INDEX");
	if(ret) return ret;

	//Top DICT INDEX

	ret = _read_INDEX(p, _func_index_topDICT, "Top DICT INDEX");
	if(ret) return ret;

	//String INDEX

	ret = _read_INDEX(p, _func_index_string, "String INDEX");
	if(ret) return ret;

	//Global Subr INDEX

	cff->offset_global_subr = font_readbuf_getpos(p);

	if(fverbose)
	{
		ret = _read_INDEX(p, _func_index_subr, "Global Subr INDEX");
		if(ret) return ret;
	}

	//--------- オフセットから

	if(_get_glyph_num(p))
		return FONTERR_READ;

	//Private DICT

	if(cff->private_offset)
	{
		ret = _put_private_dict(p, cff->private_offset, cff->private_size);
		if(ret) return ret;
	}

	//Charsets

	ret = _put_charsets(p, cff, fraw);
	if(ret) return ret;

	//(CID) Font DICT INDEX

	if(cff->offset_fdarray)
	{
		if(font_readbuf_setpos(p, cff->offset_fdarray)) return FONTERR_READ;

		ret = _read_INDEX(p, _func_index_fontdict, "FDArray (Font DICT INDEX)");
		if(ret) return ret;
	}

	//(CID) FDSelect

	if(cff->offset_fdselect)
	{
		if(font_readbuf_setpos(p, cff->offset_fdselect)) return FONTERR_READ;

		ret = _put_fdselect(p, fraw);
		if(ret) return ret;
	}

	return 0;
}

/* CharStrings INDEX */

static int _put_charstrings(Font *p)
{
	int ret;

	ret = font_cff_charstring_init(p);
	if(ret) return ret;

	//CharString

	if(font_readbuf_setpos(p, p->pcffdat->offset_charstrings))
		return FONTERR_READ;

	ret = _read_INDEX(p, _func_index_charstring, 0);

	return ret;
}

/* 出力処理 */

static int _proc_output(Font *p,cffdat *cff)
{
	int ret;

	//---- 基本

	ret = font_openput(p, "CFF.txt", " 'CFF' table");
	if(ret) return ret;

	ret = _main_proc(p, cff);

	font_closeput(p);

	if(ret) return ret;

	//---- CharStrings

	if((g_app.optflags & OPTF_VERBOSE)
		&& cff->offset_charstrings)
	{
		ret = font_openput(p, "CFF-glyph.txt", " 'CFF' table (CharStrings)");
		if(ret) return ret;

		ret = _put_charstrings(p);

		font_closeput(p);
	}

	return ret;
}

/** CFF 出力 */

int font_put_cff(Font *p)
{
	cffdat *dat;
	int ret;

	//読み込み

	if(p->is_cff_file)
		ret = font_readfull_to_readbuf(p);
	else
		ret = font_read_table(p, MAKE_TAG('C','F','F',' '));

	if(ret) return ret;

	//

	dat = (cffdat *)app_malloc0(sizeof(cffdat));
	if(!dat) return FONTERR_ALLOC;
	
	p->pcffdat = dat;

	//

	ret = _proc_output(p, dat);

	if(!ret && p->readbuf_err) ret = FONTERR_READ;

	//

	app_free(dat->cid_fdind);
	app_free(dat->cid_subr);
	app_free(dat);

	p->pcffdat = NULL;

	return ret;
}

