/*$
fontanalz
Copyright (c) 2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/**********************************
 * フォント: cmap
 **********************************/

#include <stdint.h>
#include <stdio.h>

#include "def.h"
#include "font.h"
#include "font_pv.h"


/* 同じサブテーブルのオフセットを指定している場合があるので、
 * readbuf の内容は変更しないこと。 */

//---------------------

static const char *g_name_plat[] = {
	"Unicode", "Macintosh", "ISO", "Windows", "Custom"
};

//Unicode
static const char *g_name_plat0_enc[] = {
	"Unicode 1.0", "Unicode 1.1", "ISO/IEC 10646", "Unicode 2.0(BMP)",
	"Unicode 2.0(full)", "Unicode Variation Sequences", "Unicode full"
};

//Windows
static const char *g_name_plat3_enc[] = {
	"Symbol", "Unicode BMP", "ShiftJIS", "PRC", "Big5", "Wansung", "Johab",
	"-", "-", "-", "Unicode full"
};

//---------------------


//=========== format 0

/* format 0: Byte エンコーディング */

static int _put_format0(Font *p,FILE *fp)
{
	int i;
	uint8_t *ps;

	fputs("<Byte encoding>\n\n", fp);

	font_putbuf16(p, "+format  ", 0);
	font_putbuf16(p, "+length  ", 0);
	font_putbuf16(p, "+language", 0);
	fputs("u8  glyphIdArray[256]:\n# code = GID\n", fp);

	if(font_readbuf_skip(p, 256, &ps)) return 1;

	for(i = 0; i < 256; i++, ps++)
		fprintf(fp, "%3d = %d\n", i, *ps);

	return 0;
}


//=========== format 2


/* format 2: High-byte mapping (生データ) */

static int _put_format2_raw(Font *p,FILE *fp)
{
	uint8_t *pbuf;
	uint16_t keymax,v16,first,cnt,rangeoff;
	int16_t delta;
	uint32_t offset;
	int i,j,shcnt;

	fputs("<High-byte mapping>\n\n", fp);

	font_putbuf16(p, "+format  ", 0);
	font_putbuf16(p, "+length  ", 0);
	font_putbuf16(p, "+language", 0);
	fputs("u16 subHeaderKeys[256]:\n", fp);

	//subHeaderKeys

	if(font_readbuf_skip(p, 256 * 2, &pbuf))
		return 1;

	for(i = 0, keymax = 0; i < 32; i++)
	{
		fprintf(fp, "[%3d] ", i * 8);
		
		for(j = 0; j < 8; j++, pbuf += 2)
		{
			font_getbuf16(pbuf, &v16);
			
			fprintf(fp, " %4d", v16);

			if(keymax < v16)
				keymax = v16;
		}

		fputc('\n', fp);
	}

	//subHeader

	fputs("\nSubHeader subHeaders[]:\n", fp);
	fputs("# u16 firstCode, u16 entryCount, s16 idDelta, u16 idRangeOffset\n", fp);

	offset = font_readbuf_getpos(p);
	shcnt = keymax / 8 + 1; //SubHeader の数

	for(i = 0; i < shcnt; i++, offset += 8)
	{
		font_readbuf16(p, &first);
		font_readbuf16(p, &cnt);
		font_readbuf16(p, &delta);
		font_readbuf16(p, &rangeoff);
	
		fprintf(fp, "\n[%d] %d, %d, %d, %d\n<glyphIdArray>",
			i * 8, first, cnt, delta, rangeoff);

		//glyphIdArray

		if(font_readbuf_getposptr(p, offset + 6 + rangeoff, cnt * 2, &pbuf))
			return 1;

		for(; cnt; cnt--, pbuf += 2)
		{
			font_getbuf16(pbuf, &v16);

			fprintf(fp, " %d", v16);
		}

		fputc('\n', fp);
	}

	return 0;
}

/* format 2: High-byte mapping (code) */

static int _put_format2_code(Font *p,FILE *fp)
{
	uint16_t v16,first,cnt,rangeoff;
	int16_t delta;
	uint8_t *pbuf1,*pbuf2;
	uint32_t offset;
	int i,hicode,lowcode;

	fputs("<High-byte mapping>\n\n", fp);

	font_putbuf16(p, "+format  ", 0);
	font_putbuf16(p, "+length  ", 0);
	font_putbuf16(p, "+language", 0);
	fputs("\n# code = GID\n", fp);

	//subHeaderKeys (subHeader のオフセット)

	if(font_readbuf_skip(p, 512, &pbuf1))
		return 1;

	offset = font_readbuf_getpos(p);

	for(i = 0; i < 256; i++, pbuf1 += 2)
	{
		font_getbuf16(pbuf1, &v16);

		//subHeader

		if(font_readbuf_getposptr(p, offset + v16, 8, &pbuf2)) return 1;

		font_getbuf_format(pbuf2, "hhhh", &first, &cnt, &delta, &rangeoff);

		if(font_readbuf_getposptr(p, offset + v16 + 6 + rangeoff, cnt * 2, &pbuf2)) //glyphIdArray
			return 1;

		hicode = i << 8;
		lowcode = first;

		for(; cnt; cnt--, lowcode++, pbuf2 += 2)
		{
			font_getbuf16(pbuf2, &v16);

			if(lowcode > 255)
				v16 = 0;
			else
				v16 = (v16 + delta) & 0xffff;

			fprintf(fp, "0x%04X = %d\n", hicode | lowcode, v16);
		}
	}

	return 0;
}


//=========== format 4


/* format 4: Unicode BMP (raw) */

static int _put_format4_raw(Font *p,FILE *fp)
{
	uint16_t segcnt2,end,start,rangeoff;
	int16_t delta;
	uint32_t offset;
	uint8_t *pbuf;
	int i,cnt;

	fputs("<Segment mapping to delta values>\n\n", fp);

	font_putbuf16(p, "+format       ", 0);
	font_putbuf16(p, "+length       ", 0);
	font_putbuf16(p, "+language     ", 0);
	font_putbuf16(p, "+segCountX2   ", &segcnt2);
	font_putbuf16(p, "+searchRange  ", 0);
	font_putbuf16(p, "+entrySelector", 0);
	font_putbuf16(p, "+rangeShift   ", 0);
	fputs("\n# u16 endCode, u16 startCode, s16 idDelta, u16 idRangeOffset\n\n", fp);

	//

	offset = font_readbuf_getpos(p);

	if(font_readbuf_getposptr(p, offset, segcnt2 * 4 + 2, &pbuf))
		return 1;

	cnt = segcnt2 / 2;

	for(i = 0; i < cnt; i++, pbuf += 2, offset += 2)
	{
		font_getbuf16(pbuf, &end);
		font_getbuf16(pbuf + segcnt2 + 2, &start);
		font_getbuf16(pbuf + segcnt2 * 2 + 2, &delta);
		font_getbuf16(pbuf + segcnt2 * 3 + 2, &rangeoff);

		fprintf(fp, "[%d] 0x%06X, 0x%06X, %d, %d\n", i, end, start, delta, rangeoff);

		//glyphIDArray (rangeoff のポインタ位置からの相対位置)

		if(rangeoff)
		{
			fputs("<glyphIDArray>", fp);
			
			if(font_readbuf_setpos(p, offset + segcnt2 * 3 + 2 + rangeoff)
				|| font_putbuf16_ar(p, end - start + 1, 0))
				return 1;

			fputc('\n', fp);
		}
	}

	return 0;
}

/* format 4: Unicode BMP (code) */

static int _put_format4_code(Font *p,FILE *fp)
{
	uint8_t *pbuf,*pgid;
	uint16_t segcnt2,end,start,rangeoff,v16;
	int16_t delta;
	uint32_t offset;
	int i;

	fputs("<Segment mapping to delta values>\n\n", fp);

	font_putbuf16(p, "+format       ", 0);
	font_putbuf16(p, "+length       ", 0);
	font_putbuf16(p, "+language     ", 0);
	font_putbuf16(p, "+segCountX2   ", &segcnt2);
	font_putbuf16(p, "+searchRange  ", 0);
	font_putbuf16(p, "+entrySelector", 0);
	font_putbuf16(p, "+rangeShift   ", 0);
	fputs("\n# code(char) = GID\n", fp);

	//

	offset = font_readbuf_getpos(p);

	if(font_readbuf_getposptr(p, offset, segcnt2 * 4 + 2, &pbuf))
		return 1;

	for(i = segcnt2 / 2; i; i--, pbuf += 2, offset += 2)
	{
		//end,start,delta,rangeoff

		font_getbuf16(pbuf, &end);
		if(end == 0xffff) break;
		
		font_getbuf16(pbuf + segcnt2 + 2, &start);
		font_getbuf16(pbuf + segcnt2 * 2 + 2, &delta);
		font_getbuf16(pbuf + segcnt2 * 3 + 2, &rangeoff);

		//

		if(!rangeoff)
		{
			//rangeoff = 0
			
			for(; start <= end; start++)
			{
				fprintf(fp, "0x%06X(", start);
				font_put_uni_utf8(p, start);
				fprintf(fp, ") = %d\n", (start + delta) & 0xffff);
			}
		}
		else
		{
			//glyphIDArray (rangeoff のポインタ位置からの相対位置)

			if(font_readbuf_getposptr(p, offset + segcnt2 * 3 + 2 + rangeoff, (end - start + 1) * 2, &pgid))
				return 1;

			for(; start <= end; start++, pgid += 2)
			{
				font_getbuf16(pgid, &v16);
				
				fprintf(fp, "0x%06X(", start);
				font_put_uni_utf8(p, start);
				fprintf(fp, ") = %d\n", v16);
			}
		}
	}

	return 0;
}


//=========== format 6


/* format 6: Trimmed table mapping (raw) */

static int _put_format6_raw(Font *p,FILE *fp)
{
	uint16_t first,cnt;

	fputs("<Trimmed table mapping>\n\n", fp);

	font_putbuf16(p, "+format    ", 0);
	font_putbuf16(p, "+length    ", 0);
	font_putbuf16(p, "+language  ", 0);
	font_putbuf16(p, "+firstCode ", &first);
	font_putbuf16(p, "+entryCount", &cnt);
	fputs("u16 glyphIdArray[entryCount]:\n", fp);
	font_putbuf16_ar(p, cnt, 0);

	return 0;
}

/* format 6: Trimmed table mapping (code) */

static int _put_format6_code(Font *p,FILE *fp)
{
	uint8_t *pbuf;
	uint16_t gid,cnt,first;

	fputs("<Trimmed table mapping>\n\n", fp);

	font_putbuf16(p, "+format    ", 0);
	font_putbuf16(p, "+length    ", 0);
	font_putbuf16(p, "+language  ", 0);
	font_putbuf16(p, "+firstCode ", &first);
	font_putbuf16(p, "+entryCount", &cnt);
	fputs("\n# code = GID\n", fp);

	if(font_readbuf_skip(p, cnt * 2, &pbuf))
		return 1;

	for(; cnt; cnt--, first++, pbuf += 2)
	{
		font_getbuf16(pbuf, &gid);
		fprintf(fp, "0x%04X = %d\n", first, gid);
	}

	return 0;
}


//=========== format 12


/* format 12: Unicode full (raw) */

static int _put_format12_raw(Font *p,FILE *fp)
{
	uint32_t i,num,start,end,gid;
	uint8_t *pbuf;

	fputs("<Segmented coverage>\n\n", fp);

	font_putbuf16(p, "+format   ", 0);
	font_putbuf16(p, "+reserved ", 0);
	font_putbuf32(p, "+length   ", 0);
	font_putbuf32(p, "+language ", 0);
	font_putbuf32(p, "+numGroups", &num);
	fputs("SequentialMapGroup groups[numGroups]:\n", fp);
	fputs("# u32 startCharCode, endCharCode, startGlyphID\n", fp);

	if(font_readbuf_skip(p, 4 * 3 * num, &pbuf))
		return 1;

	for(i = 0; i < num; i++, pbuf += 12)
	{
		font_getbuf_format(pbuf, "iii", &start, &end, &gid);
	
		fprintf(fp, "[%d] 0x%06X, 0x%06X, %u\n", i, start, end, gid);
	}

	return 0;
}

/* format 12: Unicode full (code) */

static int _put_format12_code(Font *p,FILE *fp)
{
	uint32_t i,num,start,end,gid;
	uint8_t *pbuf;

	fputs("<Segmented coverage>\n\n", fp);

	font_putbuf16(p, "+format   ", 0);
	font_putbuf16(p, "+reserved ", 0);
	font_putbuf32(p, "+length   ", 0);
	font_putbuf32(p, "+language ", 0);
	font_putbuf32(p, "+numGroups", &num);
	fputs("\n# code(char) = GID\n", fp);

	if(font_readbuf_skip(p, num * 4 * 3, &pbuf))
		return 1;

	for(i = 0; i < num; i++, pbuf += 12)
	{
		font_getbuf_format(pbuf, "iii", &start, &end, &gid);

		for(; start <= end; start++, gid++)
		{
			fprintf(fp, "0x%06X(", start);
			font_put_uni_utf8(p, start);
			fprintf(fp, ") = %d\n", gid);
		}
	}

	return 0;
}


//=========== format 14


/* format 14: UVS (raw) */

static int _put_format14_raw(Font *p,FILE *fp)
{
	uint32_t i,j,num,num2;
	uint32_t offset_top,off1,off2;
	int32_t i32;
	uint16_t v16;
	uint8_t bt;

	offset_top = font_readbuf_getpos(p);

	fputs("<Unicode Variation Sequences>\n\n", fp);

	font_putbuf16(p, "+format", 0);
	font_putbuf32(p, "+length", 0);
	font_putbuf32(p, "+numVarSelectorRecords", &num);
	fputs("VariationSelector varSelector[numVarSelectorRecords]:\n", fp);

	for(i = 0; i < num; i++)
	{
		fprintf(fp, "\n----------------\n [%d]\n----------------\n\n", i);
		
		font_putbuf24(p, "$varSelector        ", 0);
		font_putbuf32(p, "+defaultUVSOffset   ", &off1);
		font_putbuf32(p, "+nonDefaultUVSOffset", &off2);

		//DefaultUVS

		if(off1)
		{
			if(font_readbuf_push_pos_goto(p, offset_top + off1)) return 1;

			fputs("\n<Default UVS table>\n", fp);
			font_putbuf32(p, "+numUnicodeValueRanges", &num2);
			fputs("UnicodeRange ranges[numUnicodeValueRanges]:\n", fp);
			fputs("# u24 startUnicodeValue, u8 additionalCount\n", fp);

			for(j = 0; j < num2; j++)
			{
				font_readbuf24(p, &i32);
				font_readbuf8(p, &bt);

				fprintf(fp, "[%d] 0x%06X, %d\n", j, i32, bt);
			}

			font_readbuf_pop_pos(p);
		}

		//nonDefaultUVS

		if(font_readbuf_push_pos_goto(p, offset_top + off2)) return 1;

		fputs("\n<Non-Default UVS table>\n", fp);
		font_putbuf32(p, "+numUVSMappings", &num2);
		fputs("UVSMapping [numUVSMappings]:\n", fp);
		fputs("# u24 unicodeValue, u16 glyphID\n", fp);

		for(j = 0; j < num2; j++)
		{
			font_readbuf24(p, &i32);
			font_readbuf16(p, &v16);

			fprintf(fp, "[%d] 0x%06X, %d\n", j, i32, v16);
		}

		font_readbuf_pop_pos(p);
	}

	return 0;
}

/* format 14: UVS (code) */

static int _put_format14_code(Font *p,FILE *fp)
{
	uint32_t i,num,num2;
	uint32_t offset_top,off1,off2;
	int32_t ucsel,code;
	uint16_t gid;
	uint8_t *pbuf;

	offset_top = font_readbuf_getpos(p);

	fputs("<Unicode Variation Sequences>\n\n", fp);

	font_putbuf16(p, "+format", 0);
	font_putbuf32(p, "+length", 0);
	font_putbuf32(p, "+numVarSelectorRecords", &num);
	fputs("\n# code(char) = GID\n", fp);

	//

	if(font_readbuf_getposptr(p, offset_top + 10, num * 11, &pbuf))
		return 1;

	for(i = 0; i < num; i++, pbuf += 11)
	{
		fprintf(fp, "---- [%u] ----\n", i);
		
		font_getbuf_format(pbuf, "kii", &ucsel, &off1, &off2);

		if(!off2) continue;

		//nonDefaultUVS

		if(font_readbuf_setpos(p, offset_top + off2))
			return 1;

		font_readbuf32(p, &num2);

		for(; num2; num2--)
		{
			font_readbuf24(p, &code);
			font_readbuf16(p, &gid);

			fprintf(fp, "U+%06X U+%06X (", code, ucsel);
			font_put_uni_utf8(p, code);
			font_put_uni_utf8(p, ucsel);
			fprintf(fp, ") = %d\n", gid);
		}
	}

	return 0;
}


//==============================
// main
//==============================


/* 基本情報
 *
 * dst_num: サブテーブルの数
 * dst_prec: EncodingRecord の位置 */

static int _put_basic(Font *p,int *dst_num,uint8_t **dst_prec)
{
	FILE *fp;
	uint8_t *pbuf,*pbuf2;
	uint16_t num,plat,enc,format;
	uint32_t offset;
	int ret,i;

	ret = font_openput(p, "cmap.txt", " 'cmap' table");
	if(ret) return ret;

	fp = p->put.fp;

	font_putbuf16(p, "+version  ", 0);
	font_putbuf16(p, "+numTables", &num);
	fputs("EncodingRecord encodingRecords[numTables]:\n", fp);
	fputs("# u16 platformID, u16 encodingID, u32 offset\n", fp);

	//EncodingRecord

	if(font_readbuf_skip(p, num * 8, &pbuf))
		goto ERR;

	*dst_num = num;
	*dst_prec = pbuf;

	for(i = 0; i < num; i++, pbuf += 8)
	{
		font_getbuf_format(pbuf, "hhi", &plat, &enc, &offset);

		fprintf(fp, "\n[%i] plat=%d, enc=%d, offset=%u\n  plat=%s",
			i, plat, enc, offset, (plat < 5)? g_name_plat[plat]: "?");

		if(plat == 0)
			fprintf(fp, ", enc=%s\n", (enc < 7)? g_name_plat0_enc[enc]: "?");
		else if(plat == 3)
			fprintf(fp, ", enc=%s\n", (enc < 11)? g_name_plat3_enc[enc]: "?");
		else
			fputc('\n', fp);

		//subtable format

		if(font_readbuf_getposptr(p, offset, 2, &pbuf2))
			goto ERR;

		font_getbuf16(pbuf2, &format);

		fprintf(fp, "  format = %d\n", format);
	}

ERR:
	return font_closeput_ret(p);
}

/* (詳細) サブテーブル */

static int _put_verbose(Font *p,int num,uint8_t *pbuf)
{
	FILE *fp;
	uint16_t plat,enc,format;
	uint32_t offset;
	int ret,i,is_raw;

	is_raw = g_app.optflags & OPTF_RAW;

	for(i = 0; i < num; i++, pbuf += 8)
	{
		ret = font_openput_fno(p, "cmap-", ".txt", i);
		if(ret) return ret;

		fp = p->put.fp;

		//EncodingRecord
	
		font_getbuf_format(pbuf, "hhi", &plat, &enc, &offset);

		fprintf(fp, "===================================\n");
		fprintf(fp, "  cmap subtable [%d]\n", i);
		fprintf(fp, "  plat=%d, enc=%d, offset=%u\n", plat, enc, offset);
		fprintf(fp, "===================================\n\n");

		//SubTable

		if(font_readbuf_setpos(p, offset)
			|| p->readbuf_remain < 2)
		{
			font_closeput(p);
			return FONTERR_READ;
		}

		font_getbuf16(p->readbuf_cur, &format);

		switch(format)
		{
			case 0:
				ret = _put_format0(p, fp);
				break;
			case 2:
				if(is_raw)
					ret = _put_format2_raw(p, fp);
				else
					ret = _put_format2_code(p, fp);
				break;
			case 4:
				if(is_raw)
					ret = _put_format4_raw(p, fp);
				else
					ret = _put_format4_code(p, fp);
				break;
			case 6:
				if(is_raw)
					ret = _put_format6_raw(p, fp);
				else
					ret = _put_format6_code(p, fp);
				break;
			case 12:
				if(is_raw)
					ret = _put_format12_raw(p, fp);
				else
					ret = _put_format12_code(p, fp);
				break;
			case 14:
				if(is_raw)
					ret = _put_format14_raw(p, fp);
				else
					ret = _put_format14_code(p, fp);
				break;
			default:
				fprintf(fp, "format = %d <unknown>\n", format);
				ret = 0;
				break;
		}

		font_closeput(p);

		if(ret || p->readbuf_err)
			return FONTERR_READ;
	}

	return 0;
}

/** cmap 出力
 *
 * -v: 各サブテーブルを出力 (ファイル出力時は、ファイルを分ける)
 * -r: サブテーブルを生データで出力 */

int font_put_cmap(Font *p)
{
	int ret,num;
	uint8_t *prec;

	ret = font_read_table(p, MAKE_TAG('c','m','a','p'));
	if(ret) return ret;

	//基本情報

	ret = _put_basic(p, &num, &prec);
	if(ret) return ret;

	//-v,-r (各サブテーブル)

	if(g_app.optflags & (OPTF_RAW | OPTF_VERBOSE))
	{
		ret = _put_verbose(p, num, prec);
		if(ret) return ret;
	}

	return 0;
}

/** cmap-uni 出力 (GID -> Unicode マップ) */

int font_put_cmap_uni(Font *p)
{
	int i,num,ret;
	uint32_t *ps,c;
	FILE *fp;

	if(!p->gidunimap) return FONTERR_UNFOUND;

	//

	ret = font_openput(p, "cmap-uni.txt", "cmap : GID -> Unicode mapping");
	if(ret) return ret;

	fp = p->put.fp;
	
	fputs("# GID: Unicode (char)\n\n", fp);

	ps = p->gidunimap;
	num = p->maxp_gnum;

	for(i = 0; i < num; i++)
	{
		c = *(ps++);
	
		if(!c)
			fprintf(fp, "%d: -\n", i);
		else
		{
			fprintf(fp, "%d: U+%04X", i, c);

			if(c >= 0x20)
			{
				fputs(" (", fp);
				font_put_uni_utf8(p, c);
				fputc(')', fp);
			}

			fputc('\n', fp);
		}
	}

	font_closeput(p);

	return 0;
}

