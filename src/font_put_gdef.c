/*$
fontanalz
Copyright (c) 2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*************************************
 * フォント: GDEF
 *************************************/

#include <stdint.h>
#include <stdio.h>

#include "font.h"
#include "font_pv.h"



/* AttachmentList */

static void _put_attachment(Font *p,FILE *fp,uint32_t offset_top)
{
	uint16_t offset,i,cnt,pcnt;
	uint8_t *pbuf;

	if(font_readbuf_setpos(p, offset_top)) return;

	font_put_header(p, "AttachmentList");

	font_putbuf16(p, "+coverageOffset", &offset);
	font_putbuf16(p, "+glyphCount    ", &cnt);
	fputs("u16 attachPointOffsets[glyphCount]:\n", fp);

	if(font_readbuf_skip(p, cnt * 2, &pbuf)) return;

	for(i = 0; i < cnt; i++, pbuf += 2)
	{
		font_getbuf16(pbuf, &offset);
		fprintf(fp, "[%d] %d\n", i, offset);

		if(font_readbuf_setpos(p, offset_top + offset)) return;

		font_putbuf16(p, "+pointCount", &pcnt);
		fputs("u16 pointIndices[pointCount]:\n", fp);
		font_putbuf16_ar(p, pcnt, 0);
	}
}

/* LigatureCaretList */

static void _put_ligature_caret(Font *p,FILE *fp,uint32_t offset_top)
{
	uint8_t *pbuf;
	uint16_t i,offset,cnt;

	if(font_readbuf_setpos(p, offset_top)) return;

	font_put_header(p, "LigatureCaretList");

	font_putbuf16(p, "+coverageOffset", &offset);

	if(font_put_Coverage(p, offset_top + offset, 0)) return;
	fputc('\n', fp);

	font_putbuf16(p, "+ligGlyphCount ", &cnt);
	fputs("u16 ligGlyphOffsets[ligGlyphCount]:\n", fp);

	if(font_readbuf_skip(p, cnt * 2, &pbuf)) return;

	for(i = 0; i < cnt; i++, pbuf += 2)
	{
		font_getbuf16(pbuf, &offset);
		fprintf(fp, "[%d] %d\n", i, offset);

		if(font_readbuf_setpos(p, offset_top + offset)) return;

		/* LigGlyph: 空データのフォントが多いので、省略 */
	}
}

/* MarkGlyphSets */

static void _put_markglyphset(Font *p,FILE *fp,uint32_t offset_top)
{
	uint8_t *pbuf;
	uint16_t format,cnt,i;
	uint32_t offset;

	if(font_readbuf_setpos(p, offset_top)) return;

	font_put_header(p, "MarkGlyphSets");

	font_putbuf16(p, "+format", &format);
	if(format != 1) return;
	
	font_putbuf16(p, "+markGlyphSetCount", &cnt);
	fputs("u32 coverageOffsets[markGlyphSetCount]:\n", fp);

	if(font_readbuf_skip(p, cnt * 4, &pbuf)) return;

	for(i = 0; i < cnt; i++, pbuf += 4)
	{
		font_getbuf32(pbuf, &offset);
		fprintf(fp, "\n[%d] %u\n", i, offset);

		if(font_put_Coverage(p, offset_top + offset, 0)) return;
	}
}

/** GDEF 出力 */

int font_put_gdef(Font *p)
{
	int ret;
	uint16_t min,off16[5];
	uint32_t off32;
	FILE *fp;

	ret = font_read_table(p, MAKE_TAG('G','D','E','F'));
	if(ret) return ret;

	//------

	ret = font_openput(p, "GDEF.txt", " 'GDEF' table");
	if(ret) return ret;

	fp = p->put.fp;

	font_putbuf16(p, "+majorVersion", 0);
	font_putbuf16(p, "+minorVersion", &min);

	font_putbuf16(p, "+glyphClassDefOffset     ", off16);
	font_putbuf16(p, "+attachListOffset        ", off16 + 1);
	font_putbuf16(p, "+ligCaretListOffset      ", off16 + 2);
	font_putbuf16(p, "+markAttachClassDefOffset", off16 + 3);

	if(min >= 2)
	{
		fputs("--- ver 1.2 ---\n", fp);
		font_putbuf16(p, "+markGlyphSetsDefOffset  ", off16 + 4);
	}

	if(min >= 3)
	{
		fputs("--- ver 1.3 ---\n", fp);
		font_putbuf32(p, "+itemVarStoreOffset      ", &off32);
	}

	//GlyphClassDef

	if(off16[0])
	{
		font_put_header(p, "GlyphClassDef");
		font_put_ClassDef(p, off16[0]);
	}

	//AttachmentList

	if(off16[1])
		_put_attachment(p, fp, off16[1]);

	//LigatureCaretList

	if(off16[2])
		_put_ligature_caret(p, fp, off16[2]);

	//MarkAttachClassDef

	if(off16[3])
	{
		font_put_header(p, "MarkAttachClassDef");
		font_put_ClassDef(p, off16[3]);
	}

	//MarkGlyphSets

	if(off16[4])
		_put_markglyphset(p, fp, off16[4]);

	return font_closeput_ret(p);
}
