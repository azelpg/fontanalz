/*$
fontanalz
Copyright (c) 2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/**********************************
 * フォント: glyf
 **********************************/

#include <stdint.h>
#include <stdio.h>

#include "def.h"
#include "font.h"
#include "font_pv.h"


//-------------------

typedef struct
{
	uint8_t flags,
		repeat;			//flags の繰り返し回数
	int16_t x,y;
	uint16_t cont_no;	//輪郭の番号
}_coord_dat;

//-------------------


/* x or y 座標を取得してセット
 *
 * type: 0=x, 1=y */

static int _set_coord_xy(Font *p,_coord_dat *pco,int num,int type)
{
	int i;
	uint8_t f,sf1,sf2,bt;
	int16_t s16;

	sf1 = (type)? 1<<2: 1<<1;
	sf2 = sf1 << 3;

	for(i = 0; i < num; i++, pco++)
	{
		f = pco->flags;

		if(f & sf1)
		{
			//uint8

			if(font_readbuf8(p, &bt)) return 1;

			if(f & sf2)
				s16 = bt;
			else
				s16 = -bt;
		}
		else
		{
			//int16

			if(f & sf2)
				s16 = 0;
			else
			{
				if(font_readbuf16(p, &s16)) return 1;
			}
		}

		if(type)
			pco->y = s16;
		else
			pco->x = s16;
	}

	return 0;
}

/* 座標データ読み込み */

static int _read_coord(Font *p,int num,uint8_t *pend,_coord_dat **dst)
{
	_coord_dat *buf,*pco;
	int i,curcont;
	uint16_t endind;
	uint8_t f,repcnt;

	buf = pco = (_coord_dat *)app_malloc(sizeof(_coord_dat) * num);
	if(!buf) return 1;

	//flags
	
	repcnt = 0;
	curcont = 0;

	font_getbuf16(pend, &endind);
	pend += 2;

	for(i = 0; i < num; i++, pco++)
	{
		if(repcnt)
			repcnt--;
		else
		{
			if(font_readbuf8(p, &f)) goto ERR;
		}

		pco->flags = f;
		pco->cont_no = curcont;

		//repeat

		if(f & (1<<3))
		{
			if(font_readbuf8(p, &repcnt)) goto ERR;

			pco->repeat = repcnt;

			f &= ~(1<<3);
		}
		else
			pco->repeat = 0;

		//輪郭番号
		
		if(i == endind)
		{
			curcont++;

			font_getbuf16(pend, &endind);
			pend += 2;
		}
	}

	//x,y

	if(_set_coord_xy(p, buf, num, 0)
		|| _set_coord_xy(p, buf, num, 1))
		goto ERR;

	*dst = buf;
	
	return 0;

ERR:
	app_free(buf);
	return 1;
}

/* (raw) 単純なアウトライン */

static int _put_glyph_raw_simple(Font *p,FILE *fp,int cnum)
{
	uint16_t v16;
	int i,num,curcont,px,py;
	uint8_t *pend,f;
	_coord_dat *buf,*pco;

	pend = p->readbuf_cur;

	fputs("u16 endPtsOfContours[numberOfContours]:\n", fp);
	if(font_putbuf16_ar(p, cnum, 0)) return 1;

	//座標の数
	font_getbuf16(p->readbuf_cur - 2, &v16);
	num = v16 + 1;

	font_putbuf16(p, "+instructionLength", &v16);
	font_putbuf8_ar(p, "instructions[instructionLength]", v16);

	//---- 座標データ読み込み

	if(_read_coord(p, num, pend, &buf))
		return 1;
	
	//------ 出力

	pco = buf;
	curcont = -1;
	px = py = 0;

	for(i = 0; i < num; i++, pco++)
	{
		if(curcont != pco->cont_no)
		{
			curcont++;
			fprintf(fp, "\n==== <%d> ====\n\n", curcont);
		}

		fprintf(fp, "-- [%d] --\n", i);

		//flags

		f = pco->flags;

		fprintf(fp, "u8 flags: 0x%02X (", f);
		font_put_bits_on(p, f, 8);
		fputc(')', fp);

		if(pco->repeat)
			fprintf(fp, " <repeat %d>", pco->repeat);

		fputc('\n', fp);

		//

		px += pco->x;
		py += pco->y;

		fprintf(fp, "x = %d (%+d), y = %d (%+d)\n", px, pco->x, py, pco->y);
	}

	app_free(buf);

	return 0;
}

/* (raw) 複合グリフの出力 */

static int _put_glyph_raw_comb(Font *p,FILE *fp)
{
	uint16_t flags,v16;

	do
	{
		font_putbuf16(p, "$flags", &flags);
		font_put_bits_name(p, flags, 13,
			"ARG_1_AND_2_ARE_WORDS;ARGS_ARE_XY_VALUES;ROUND_XY_TO_GRID;WE_HAVE_A_SCALE;Reversed;"
			"MORE_COMPONENTS;WE_HAVE_AN_X_AND_Y_SCALE;WE_HAVE_A_TWO_BY_TWO;"
			"WE_HAVE_INSTRUCTIONS;USE_MY_METRICS;OVERLAP_COMPOUND;SCALED_COMPONENT_OFFSET;"
			"UNSCALED_COMPONENT_OFFSET");
		
		font_putbuf16(p, "+glyphIndex", 0);

		//arg

		if(flags & 1)
		{
			font_putbuf16(p, "-argument1", 0);
			font_putbuf16(p, "-argument2", 0);
		}
		else
		{
			font_putbuf8(p, "+argument1", 0);
			font_putbuf8(p, "+argument2", 0);
		}

		if(flags & (1<<3))
			font_putbuf16(p, "&scale   ", 0);

		if(flags & (1<<6))
		{
			font_putbuf16(p, "-xscale  ", 0);
			font_putbuf16(p, "-yscale  ", 0);
		}
		
		if(flags & (1<<7))
		{
			font_putbuf16(p, "-xscale  ", 0);
			font_putbuf16(p, "-scale01 ", 0);
			font_putbuf16(p, "-scale10 ", 0);
			font_putbuf16(p, "-yscale  ", 0);
		}

		//次

		if(!(flags & (1<<5)))
			break;
		else
			fputs("--------------\n", fp);

	}while(1);

	if(flags & (1<<8))
	{
		font_putbuf16(p, "+instructionLength", &v16);
		font_putbuf8_ar(p, "instructions[instructionLength]", v16);
	}

	return 0;
}

/* (raw) gid のグリフデータ出力
 *
 * offset: loca テーブルのオフセット (2個の配列)
 * only_comp: 0 以外で、複合グリフのみ
 * return: 1 で処理した (複合グリフのみ時、0 で処理しなかった)
 *
 * ※エラーの場合は、readbuf_err = 1 にする */

static int _put_glyph_raw(Font *p,int gid,uint32_t *offset,int only_comp)
{
	FILE *fp = p->put.fp;
	int16_t num;
	int ret;

	if(font_readbuf16(p, &num)) return 0;

	//複合グリフのみ

	if(only_comp && num >= 0) return 0;

	//------------

	fprintf(fp, "# ");
	font_put_gid_unicode(p, gid);
	fputc('\n', fp);
	
	fprintf(fp, "# offset = 0x%08X, size = %u\n\n", offset[0], offset[1] - offset[0]);

	fprintf(fp, "s16 numberOfContours : %d\n", num);
	font_putbuf16(p, "-xMin", 0);
	font_putbuf16(p, "-yMin", 0);
	font_putbuf16(p, "-xMax", 0);
	font_putbuf16(p, "-yMax", 0);
	fputc('\n', fp);

	if(num == 0)
		ret = 0;
	else if(num < 0)
		//複合
		ret = _put_glyph_raw_comb(p, fp);
	else
		//通常
		ret = _put_glyph_raw_simple(p, fp, num);

	if(ret) p->readbuf_err = 1;

	return 1;
}


//=======================


/* 共通処理 */

static int _glyf_common(Font *p)
{
	FontTbl dat;
	int ret;

	//glyf をバッファに読み込み

	ret = font_read_table(p, MAKE_TAG('g','l','y','f'));
	if(ret) return ret;

	//ファイル位置を loca へ

	ret = font_goto_table(p, MAKE_TAG('l','o','c','a'), &dat);
	if(ret) return ret;

	if(dat.size < p->loca_offset_size * (p->maxp_gnum + 1))
		return FONTERR_READ;

	return 0;
}

/* loca のオフセット読み込み */

static void _read_loca_offset(Font *p,uint32_t *dst)
{
	uint16_t v16;

	if(p->loca_offset_size == 4)
		font_read32(p, dst);
	else
	{
		font_read16(p, &v16);
		*dst = v16 * 2;
	}
}

/** glyf 出力
 *
 * -g で GID 指定 (負の値ですべて) */

int font_put_glyf(Font *p)
{
	int i,ret,top,end,gnum;
	uint32_t off[2];

	ret = _glyf_common(p);
	if(ret) return ret;

	ret = font_openput(p, "glyf.txt", " 'glyf' table");
	if(ret) return ret;

	//GID 範囲

	gnum = p->maxp_gnum;

	if(g_app.gid_top < 0)
		top = 0, end = gnum - 1;
	else
	{
		top = g_app.gid_top, end = g_app.gid_end;

		if(top >= gnum) top = gnum - 1;
		if(end >= gnum) end = gnum - 1;
	}

	//グリフごと

	font_seek(p, p->loca_offset_size * top);

	_read_loca_offset(p, off);

	for(i = top; i <= end; i++)
	{
		_read_loca_offset(p, off + 1);

		//glyf

		if(off[0] != off[1])
		{
			if(font_readbuf_setpos(p, off[0])) break;

			_put_glyph_raw(p, i, off, 0);

			fputs("\n********************\n\n", p->put.fp);

			if(p->readbuf_err) break;
		}

		off[0] = off[1];
	}

	return font_closeput_ret(p);
}

/** glyf-comp 出力 (すべての複合グリフ) */

int font_put_glyf_comp(Font *p)
{
	int i,ret,gnum;
	uint32_t off[2];

	ret = _glyf_common(p);
	if(ret) return ret;

	ret = font_openput(p, "glyf-comp.txt", " 'glyf' table (Composite Glyphs)");
	if(ret) return ret;

	gnum = p->maxp_gnum;

	//グリフごと

	_read_loca_offset(p, off);

	for(i = 0; i < gnum; i++)
	{
		_read_loca_offset(p, off + 1);

		//glyf

		if(off[0] != off[1])
		{
			if(font_readbuf_setpos(p, off[0])) break;

			if(_put_glyph_raw(p, i, off, 1))
				fputs("\n********************\n\n", p->put.fp);

			if(p->readbuf_err) break;
		}

		off[0] = off[1];
	}

	return font_closeput_ret(p);
}

