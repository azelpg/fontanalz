/*$
fontanalz
Copyright (c) 2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/**********************************
 * フォント: GPOS - 通常
 **********************************/

#include <stdint.h>
#include <stdio.h>

#include "def.h"
#include "font.h"
#include "font_pv.h"

//----------------

int __gpos_raw_lookup_subtable(Font *p,uint32_t offset,int type);

//----------------


/** ValueRecord
 *
 * fenter: 改行を出力するか */

void font_put_ValueRecord(Font *p,uint16_t flags,int fenter)
{
	FILE *fp = p->put.fp;
	int16_t s;
	uint16_t u;

	if(flags & (1<<0))
	{
		font_readbuf16(p, &s);
		fprintf(fp, " xPlacement=%d", s);
	}

	if(flags & (1<<1))
	{
		font_readbuf16(p, &s);
		fprintf(fp, " yPlacement=%d", s);
	}

	if(flags & (1<<2))
	{
		font_readbuf16(p, &s);
		fprintf(fp, " xAdvance=%d", s);
	}

	if(flags & (1<<3))
	{
		font_readbuf16(p, &s);
		fprintf(fp, " yAdvance=%d", s);
	}

	if(flags & (1<<4))
	{
		font_readbuf16(p, &u);
		fprintf(fp, " xPlaDeviceOffset=%d", u);
	}

	if(flags & (1<<5))
	{
		font_readbuf16(p, &u);
		fprintf(fp, " yPlaDeviceOffset=%d", u);
	}

	if(flags & (1<<6))
	{
		font_readbuf16(p, &u);
		fprintf(fp, " xAdvDeviceOffset=%d", u);
	}

	if(flags & (1<<7))
	{
		font_readbuf16(p, &u);
		fprintf(fp, " yAdvDeviceOffset=%d", u);
	}

	if(fenter) fputc('\n', fp);
}

/** Anchor */

int font_put_Anchor(Font *p,uint32_t offset)
{
	FILE *fp = p->put.fp;
	uint8_t *pbuf;
	uint16_t format,v[2];
	int16_t x,y;

	if(font_readbuf_getposptr(p, offset, 6, &pbuf)) return 1;

	font_getbuf_format(pbuf, "hhh", &format, &x, &y);

	fprintf(fp, "format=%d, x=%d, y=%d", format, x, y);

	if(format == 2)
	{
		if(offset + 8 >= p->readbuf_size) return 1;
		
		font_getbuf16(pbuf + 6, v);
		fprintf(fp, ", point=%d", v[0]);
	}
	else if(format == 3)
	{
		if(offset + 10 >= p->readbuf_size) return 1;
		
		font_getbuf_format(pbuf + 6, "hh", v, v + 1);
		fprintf(fp, ", xDeviceOffset=%d, yDeviceOffset=%d", v[0], v[1]);
	}

	fputc('\n', fp);

	return 0;
}

/** Anchor 出力 (0 対応) */

int font_put_Anchor_for0(Font *p,uint32_t offset,uint16_t offrel)
{
	if(!offrel)
	{
		fputs("-\n", p->put.fp);
		return 0;
	}
	else
		return font_put_Anchor(p, offset + offrel);
}

/* MarkArray */

static int _put_markarray(Font *p,uint32_t offset,uint16_t *pgid)
{
	uint16_t cnt,class,off;
	uint8_t *pbuf;
	FILE *fp = p->put.fp;

	if(font_readbuf_setpos(p, offset)
		|| font_readbuf16(p, &cnt)
		|| font_readbuf_skip(p, cnt * 4, &pbuf))
		return 1;

	fputs(">>> MarkGlyph\n", fp);

	for(; cnt; cnt--, pbuf += 4, pgid++)
	{
		font_getbuf_format(pbuf, "hh", &class, &off);

		font_put_gid_unicode(p, *pgid);
		fprintf(fp, ": class=%d, <Anchor> ", class);

		if(font_put_Anchor_for0(p, offset, off)) return 1;
	}

	return 0;
}

/* lookup4,6 共通 */

static int _lookup4_6(Font *p,FILE *fp,uint32_t offset,const char *name2)
{
	uint16_t format,off[4],classcnt,cnt,*pid,i;
	uint32_t offset2;
	uint8_t *pbuf;
	ArrayBuf markbuf,basebuf;
	int ret = 1;

	if(font_readbuf_format(p, "hhhhhh", &format, off, off + 1, &classcnt, off + 2, off + 3)
		|| font_get_Coverage_gid(p, offset + off[0], &markbuf))
		return 1;

	if(font_get_Coverage_gid(p, offset + off[1], &basebuf)) goto ERR;

	//MarkArray

	if(_put_markarray(p, offset + off[2], (uint16_t *)markbuf.buf)) goto ERR;

	//BaseArray/Mark2Array

	offset2 = offset + off[3];

	if(font_readbuf_setpos(p, offset2)
		|| font_readbuf16(p, &cnt)
		|| font_readbuf_skip(p, cnt * 2, &pbuf))
		goto ERR;

	fprintf(fp, "\n>>> %s\n", name2);

	pid = (uint16_t *)basebuf.buf;

	for(; cnt; cnt--, pid++)
	{
		fputc('{', fp);
		font_put_gid_unicode(p, *pid);
		fputs("}\n", fp);
	
		for(i = 0; i < classcnt; i++, pbuf += 2)
		{
			font_getbuf16(pbuf, off);

			fprintf(fp, " markclass[%d]: <Anchor> ", i);

			font_put_Anchor_for0(p, offset2, off[0]);
		} 
	}

	//

	ret = 0;
ERR:
	app_free(markbuf.buf);
	app_free(basebuf.buf);

	return ret;
}


//======================


/* lookup1: 単一グリフ */

static int _lookup1(Font *p,FILE *fp,uint32_t offset)
{
	uint16_t format,off,flags,cnt,*pid;
	ArrayBuf buf;
	int i;

	if(font_readbuf_format(p, "hhh", &format, &off, &flags)
		|| font_get_Coverage_gid(p, offset + off, &buf))
		return 1;

	fprintf(fp, "format: %d\n", format);

	pid = (uint16_t *)buf.buf;

	if(format == 1)
	{
		//すべてのグリフで同じ値

		fputs("<ValueRecord>", fp);
		font_put_ValueRecord(p, flags, 1);

		for(i = buf.num; i; i--, pid++)
		{
			font_put_gid_unicode(p, *pid);
			fputc('\n', fp);
		}
	}
	else
	{
		//配列

		font_readbuf16(p, &cnt);
	
		for(; cnt; cnt--, pid++)
		{
			font_put_gid_unicode(p, *pid);
			fputc(':', fp);

			font_put_ValueRecord(p, flags, 1);
		}
	}

	app_free(buf.buf);

	return 0;
}

/* lookup2: ペアグリフ */

static int _lookup2(Font *p,FILE *fp,uint32_t offset)
{
	uint16_t format,flags1,flags2,off,cnt,cnt2,gid;
	uint16_t offclass[2],classcnt[2],i,j;
	uint8_t *pbuf;
	uint16_t *pid;
	ArrayBuf buf;

	if(font_readbuf_format(p, "hhhh", &format, &off, &flags1, &flags2)
		|| font_get_Coverage_gid(p, offset + off, &buf))
		return 1;

	fprintf(fp, "format: %d\n", format);

	if(format == 1)
	{
		//---- format 1

		if(font_readbuf16(p, &cnt)
			|| font_readbuf_skip(p, cnt * 2, &pbuf))
				goto ERR;

		pid = (uint16_t *)buf.buf;

		for(; cnt; cnt--, pbuf += 2, pid++)
		{
			font_getbuf16(pbuf, &off);

			//PairSet

			if(font_readbuf_setpos(p, offset + off)
				|| font_readbuf16(p, &cnt2))
				goto ERR;

			//PairValueRecord

			fputc('{', fp);
			font_put_gid_unicode(p, *pid);
			fputs("}\n", fp);

			for(; cnt2; cnt2--)
			{
				if(font_readbuf16(p, &gid)) goto ERR;

				fputs(" + ", fp);
				font_put_gid_unicode(p, gid);
				fputc(':', fp);
				
				if(flags1)
				{
					fputs(" <glyph1>", fp);
					font_put_ValueRecord(p, flags1, 0);
				}

				if(flags2)
				{
					fputs(" <glyph2>", fp);
					font_put_ValueRecord(p, flags2, 0);
				}

				fputc('\n', fp);
			}
		}
	}
	else
	{
		//----- format 2

		if(font_readbuf_format(p, "hhhh", offclass, offclass + 1, classcnt, classcnt + 1))
			goto ERR;

		//ClassDef

		fputs("\n>>> ClassDef (glyph1)\n", fp);
		if(font_put_ClassDef_gid(p, offset + offclass[0])) goto ERR;

		fputs("\n>>> ClassDef (glyph2)\n", fp);
		if(font_put_ClassDef_gid(p, offset + offclass[1])) goto ERR;

		//各クラスの値

		fputs("\n----------\n", fp);

		for(i = 0; i < classcnt[0]; i++)
		{
			for(j = 0; j < classcnt[1]; j++)
			{
				fprintf(fp, "class1=%d, class2=%d:", i, j);
				
				if(flags1)
				{
					fputs(" <glyph1>", fp);
					font_put_ValueRecord(p, flags1, 0);
				}

				if(flags2)
				{
					fputs(" <glyph2>", fp);
					font_put_ValueRecord(p, flags2, 0);
				}

				fputc('\n', fp);
			}
		}
	}

ERR:
	app_free(buf.buf);

	return 0;
}

/* lookup3: グリフの接続 */

static int _lookup3(Font *p,FILE *fp,uint32_t offset)
{
	uint16_t format,off,cnt,v1,v2,*pid;
	uint8_t *pbuf;
	ArrayBuf buf;

	if(font_readbuf_format(p, "hhh", &format, &off, &cnt)
		|| font_readbuf_skip(p, cnt * 4, &pbuf)
		|| font_get_Coverage_gid(p, offset + off, &buf))
		return 1;

	pid = (uint16_t *)buf.buf;

	for(; cnt; cnt--, pbuf += 4, pid++)
	{
		font_getbuf_format(pbuf, "hh", &v1, &v2);

		font_put_gid_unicode(p, *pid);
		fputc('\n', fp);

		if(v1)
		{
			fputs(" <Anchor(entry)> ", fp);
			font_put_Anchor(p, offset + v1);
		}

		if(v2)
		{
			fputs(" <Anchor(exit)> ", fp);
			font_put_Anchor(p, offset + v2);
		}
	}

	app_free(buf.buf);

	return 0;
}

/* lookup4: ベースグリフにマークを配置 */

static int _lookup4(Font *p,FILE *fp,uint32_t offset)
{
	return _lookup4_6(p, fp, offset, "BaseGlyph");
}

/* lookup5: 合字グリフにマークを配置 */

static int _lookup5(Font *p,FILE *fp,uint32_t offset)
{
	uint16_t format,off[4],classcnt,cnt,cnt2,*pid,i,j;
	uint32_t offset2,offset3;
	uint8_t *pbuf,*pbuf2;
	ArrayBuf markbuf,ligbuf;
	int ret = 1;

	if(font_readbuf_format(p, "hhhhhh", &format, off, off + 1, &classcnt, off + 2, off + 3)
		|| font_get_Coverage_gid(p, offset + off[0], &markbuf))
		return 1;

	if(font_get_Coverage_gid(p, offset + off[1], &ligbuf)) goto ERR;

	//MarkArray

	if(_put_markarray(p, offset + off[2], (uint16_t *)markbuf.buf)) goto ERR;

	//LigatureArray

	offset2 = offset + off[3];

	if(font_readbuf_setpos(p, offset2)
		|| font_readbuf16(p, &cnt)
		|| font_readbuf_skip(p, cnt * 2, &pbuf))
		goto ERR;

	fputs("\n>>> LigatureGlyph\n", fp);

	pid = (uint16_t *)ligbuf.buf;

	for(; cnt; cnt--, pid++, pbuf += 2)
	{
		fputc('{', fp);
		font_put_gid_unicode(p, *pid);
		fputs("}\n", fp);

		//LigatureAttach

		font_getbuf16(pbuf, off);

		offset3 = offset2 + off[0];

		if(font_readbuf_setpos(p, offset3)
			|| font_readbuf16(p, &cnt2)
			|| font_readbuf_skip(p, cnt2 * classcnt * 2, &pbuf2))
			goto ERR;

		for(i = 0; i < cnt2; i++)
		{
			for(j = 0; j < classcnt; j++, pbuf2 += 2)
			{
				font_getbuf16(pbuf2, off);
			
				fprintf(fp, " component[%d], markclass[%d]: <Anchor> ", i, j);

				font_put_Anchor_for0(p, offset3, off[0]);
			}
		}
	}

	//

	ret = 0;
ERR:
	app_free(markbuf.buf);
	app_free(ligbuf.buf);

	return ret;
}

/* lookup6 */

static int _lookup6(Font *p,FILE *fp,uint32_t offset)
{
	return _lookup4_6(p, fp, offset, "Mark2Glyph");
}

/* lookup7 */

static int _lookup7(Font *p,FILE *fp,uint32_t offset)
{
	fputs("<unsupported format>\n", fp);
	return 0;
}

/* lookup8 */

static int _lookup8(Font *p,FILE *fp,uint32_t offset)
{
	fputs("<unsupported format>\n", fp);
	return 0;
}


//------------

static int _lookup9(Font *p,FILE *fp,uint32_t offset);

static int (*g_lookup_funcs[])(Font *p,FILE *fp,uint32_t offset) = {
	_lookup1, _lookup2, _lookup3, _lookup4, _lookup5,
	_lookup6, _lookup7, _lookup8, _lookup9
};

//------------


/* lookup9 (Offset32) */

static int _lookup9(Font *p,FILE *fp,uint32_t offset)
{
	uint16_t format,type;
	uint32_t off;

	if(font_readbuf_format(p, "hhi", &format, &type, &off)) return 1;

	fprintf(fp, "<lookupType = %d>\n\n", type);
	
	if(type < 1 || type > 8) return 0;

	offset += off;

	if(font_readbuf_setpos(p, offset)) return 1;

	return (g_lookup_funcs[type - 1])(p, fp, offset);
}

/* Lookup SubTable */

static int _lookup_subtable(Font *p,uint32_t offset,int type)
{
	if(type >= 1 && type <= 9)
		return (g_lookup_funcs[type - 1])(p, p->put.fp, offset);
	else
	{
		fputs("# unknown lookupType\n", p->put.fp);
		return 0;
	}
}

/** GPOS 出力 */

int font_put_gpos(Font *p)
{
	int ret;
	uint16_t min,off16[3];

	ret = font_read_table(p, MAKE_TAG('G','P','O','S'));
	if(ret) return ret;

	//------ 出力

	ret = font_openput(p, "GPOS.txt", " 'GPOS' table");
	if(ret) return ret;

	font_putbuf16(p, "+majorVersion     ", 0);
	font_putbuf16(p, "+minorVersion     ", &min);
	font_putbuf16(p, "+scriptListOffset ", off16);
	font_putbuf16(p, "+featureListOffset", off16 + 1);
	font_putbuf16(p, "+lookupListOffset ", off16 + 2);

	if(min >= 1)
		font_putbuf32(p, "+featureVariationsOffset", 0);

	//

	if(g_app.optflags & OPTF_RAW)
	{
		font_put_ScriptList(p, off16[0]);
		font_put_FeatureList(p, off16[1]);
		font_put_LookupList(p, off16[2], __gpos_raw_lookup_subtable, off16[1]);
	}
	else
	{
		font_put_LookupList(p, off16[2], _lookup_subtable, off16[1]);
	}

	return font_closeput_ret(p);
}

