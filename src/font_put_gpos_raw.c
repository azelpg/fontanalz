/*$
fontanalz
Copyright (c) 2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/**********************************
 * フォント: GPOS - 生データ
 **********************************/

#include <stdint.h>
#include <stdio.h>

#include "font.h"
#include "font_pv.h"


/* MarkArray */

static int _put_markarray(Font *p,uint32_t offset)
{
	FILE *fp = p->put.fp;
	uint8_t *pbuf;
	uint16_t cnt,i,class,off;

	if(font_readbuf_setpos(p, offset)) return 1;

	fputs("\n>>> {MarkArray}\n", fp);

	font_putbuf16(p, "+markCount", &cnt);
	fputs("MarkRecord markRecords[markCount]:\n", fp);
	fputs("# u16 markClass, markAnchorOffset\n", fp);

	if(font_readbuf_skip(p, cnt * 4, &pbuf)) return 1;

	for(i = 0; i < cnt; i++, pbuf += 4)
	{
		font_getbuf_format(pbuf, "hh", &class, &off);

		fprintf(fp, "[%d] %d, %d\n <Anchor> ", i, class, off);
		font_put_Anchor_for0(p, offset, off);
	}

	fputs("<<< {MarkArray}\n", fp);

	return 0;
}

/* BaseArray */

static int _put_basearray(Font *p,uint32_t offset,int markcnt)
{
	FILE *fp = p->put.fp;
	uint8_t *pbuf;
	uint16_t cnt,i,off;
	int j;

	if(font_readbuf_setpos(p, offset)) return 1;

	fputs("\n>>> {BaseArray}\n", fp);

	font_putbuf16(p, "+baseCount", &cnt);
	fputs("BaseRecord baseRecords[baseCount]:\n", fp);
	fputs("# BaseRecord = u16 baseAnchorOffset[markClassCount]\n", fp);

	if(font_readbuf_skip(p, cnt * markcnt * 2, &pbuf)) return 1;

	for(i = 0; i < cnt; i++)
	{
		fprintf(fp, "--- BaseRecord[%d]\n", i);
		
		for(j = 0; j < markcnt; j++, pbuf += 2)
		{
			font_getbuf16(pbuf, &off);

			fprintf(fp, "mark[%d] offset=%d, <Anchor> ", j, off);
			font_put_Anchor_for0(p, offset, off);
		}
	}

	fputs("<<< {BaseArray}\n", fp);

	return 0;
}

/* LigatureArray */

static int _put_ligarray(Font *p,uint32_t offset,int markcnt)
{
	FILE *fp = p->put.fp;
	uint8_t *pbuf,*pbuf2;
	uint16_t cnt,ccnt,off,i,j;
	int k;
	uint32_t offset2;

	if(font_readbuf_setpos(p, offset)) return 1;

	fputs("\n>>> {LigatureArray}\n", fp);

	font_putbuf16(p, "+ligatureCount", &cnt);
	fputs("u16 ligatureAttachOffsets[ligatureCount]:\n", fp);
	fputs("# <LigatureAttach>\n# u16 componentCount\n# ComponentRecord componentRecords[componentCount]\n"
		"# (ComponentRecord = ligatureAnchorOffsets[markClassCount])\n", fp);

	if(font_readbuf_skip(p, cnt * 2, &pbuf)) return 1;

	for(i = 0; i < cnt; i++, pbuf += 2)
	{
		font_getbuf16(pbuf, &off);

		fprintf(fp, "-- LigatureAttach[%d] offset=%d ---\n", i, off);

		//LigatureAttach

		offset2 = offset + off;

		if(font_readbuf_setpos(p, offset2)) return 1;

		font_readbuf16(p, &ccnt);

		if(font_readbuf_skip(p, ccnt * markcnt * 2, &pbuf2)) return 1;

		for(j = 0; j < ccnt; j++)
		{
			fprintf(fp, "ComponentRecord[%d]\n", j);

			for(k = 0; k < markcnt; k++, pbuf2 += 2)
			{
				font_getbuf16(pbuf2, &off);

				fprintf(fp, "  mark[%d] offset=%d, <Anchor> ", k, off);
				font_put_Anchor_for0(p, offset2, off);
			}
		}
	}

	fputs("<<< {LigatureArray}\n", fp);

	return 0;
}

/* Mark2Array */

static int _put_mark2array(Font *p,uint32_t offset,int markcnt)
{
	FILE *fp = p->put.fp;
	uint8_t *pbuf;
	uint16_t cnt,i,off;
	int j;

	if(font_readbuf_setpos(p, offset)) return 1;

	fputs("\n>>> {Mark2Array}\n", fp);

	font_putbuf16(p, "+mark2Count", &cnt);
	fputs("Mark2Record mark2Records[mark2Count]:\n", fp);
	fputs("# Mark2Record = u16 mark2AnchorOffsets[markClassCount]\n", fp);

	if(font_readbuf_skip(p, cnt * markcnt * 2, &pbuf)) return 1;

	for(i = 0; i < cnt; i++)
	{
		fprintf(fp, "--- Mark2Record[%d]\n", i);
		
		for(j = 0; j < markcnt; j++, pbuf += 2)
		{
			font_getbuf16(pbuf, &off);

			fprintf(fp, "mark[%d] offset=%d, <Anchor> ", j, off);
			font_put_Anchor_for0(p, offset, off);
		}
	}

	fputs("<<< {Mark2Array}\n", fp);

	return 0;
}


//===================


/* lookup 1 (単一グリフ) */

static int _lookup1(Font *p,FILE *fp,uint32_t offset)
{
	uint16_t format,off,flags,cnt,i;

	font_putbuf16(p, "+posFormat", &format);
	if(format < 1 || format > 2) return 0;
	
	font_putbuf16(p, "+coverageOffset", &off);
	font_putbuf16(p, "$valueFormat", &flags);

	if(format == 1)
	{
		fputs("ValueRecord valueRecord:", fp);
		font_put_ValueRecord(p, flags, 1);
	}
	else
	{
		font_putbuf16(p, "+valueCount", &cnt);
		fputs("ValueRecord valueRecords[valueCount]:\n", fp);

		for(i = 0; i < cnt; i++)
		{
			fprintf(fp, "[%d]", i);
			font_put_ValueRecord(p, flags, 1);
		}
	}

	fputc('\n', fp);

	return font_put_Coverage(p, offset + off, 0);
}

/* lookup2: ペアグリフ*/

static int _lookup2(Font *p,FILE *fp,uint32_t offset)
{
	uint16_t format,offcov,flags1,flags2,cnt,i,j,off,cnt2;
	uint16_t offclass[2],classcnt[2];
	uint8_t *pbuf;

	font_putbuf16(p, "+posFormat", &format);
	if(format < 1 || format > 2) return 0;
	
	font_putbuf16(p, "+coverageOffset", &offcov);
	font_putbuf16(p, "$valueFormat1  ", &flags1);
	font_putbuf16(p, "$valueFormat2  ", &flags2);

	if(format == 1)
	{
		//---- format 1
		
		font_putbuf16(p, "+pairSetCount  ", &cnt);
		fputs("u16 pairSetOffsets[pairSetCount]:\n", fp);

		if(font_readbuf_skip(p, cnt * 2, &pbuf)) return 1;

		for(i = 0; i < cnt; i++, pbuf += 2)
		{
			font_getbuf16(pbuf, &off);

			fprintf(fp, "-- PairSet[%d] offset=%d\n", i, off);

			//PairSet

			if(font_readbuf_setpos(p, offset + off)) return 1;

			font_putbuf16(p, "+pairValueCount", &cnt2);
			fputs("PairValueRecord pairValueRecords[pairValueCount]:\n", fp);

			for(j = 0; j < cnt2; j++)
			{
				fprintf(fp, "<PairValueRecord[%d]>\n", j);
				font_putbuf16(p, "+secondGlyph", 0);

				if(flags1)
				{
					fputs("ValueRecord valueRecord1:", fp);
					font_put_ValueRecord(p, flags1, 1);
				}

				if(flags2)
				{
					fputs("ValueRecord valueRecord2:", fp);
					font_put_ValueRecord(p, flags2, 1);
				}
			}
		}
	}
	else
	{
		//----- format 2
		
		font_putbuf16(p, "+classDef1Offset", offclass);
		font_putbuf16(p, "+classDef2Offset", offclass + 1);
		font_putbuf16(p, "+class1Count    ", classcnt);
		font_putbuf16(p, "+class2Count    ", classcnt + 1);

		for(i = 0; i < classcnt[0]; i++)
		{
			fprintf(fp, "-- Class1[%d]\n", i);
			
			for(j = 0; j < classcnt[1]; j++)
			{
				fprintf(fp, " <Class2[%d]>\n", j);
				
				if(flags1)
				{
					fputs(" ValueRecord valueRecord1:", fp);
					font_put_ValueRecord(p, flags1, 1);
				}

				if(flags2)
				{
					fputs(" ValueRecord valueRecord2:", fp);
					font_put_ValueRecord(p, flags2, 1);
				}
			}
		}

		fputc('\n', fp);

		if(font_put_ClassDef(p, offset + offclass[0])
			|| font_put_ClassDef(p, offset + offclass[1]))
			return 1;
	}

	fputc('\n', fp);

	return font_put_Coverage(p, offset + offcov, 0);
}

/* lookup3: グリフの接続 */

static int _lookup3(Font *p,FILE *fp,uint32_t offset)
{
	uint16_t off,cnt,i,val[2];
	uint8_t *pbuf;

	font_putbuf16(p, "+posFormat     ", 0);
	font_putbuf16(p, "+coverageOffset", &off);
	font_putbuf16(p, "+entryExitCount", &cnt);
	fputs("EntryExitRecord entryExitRecord[entryExitCount]:\n", fp);
	fputs("# u16 entryAnchorOffset, exitAnchorOffset\n", fp);

	if(font_readbuf_skip(p, cnt * 4, &pbuf)) return 1;

	for(i = 0; i < cnt; i++, pbuf += 4)
	{
		font_getbuf_format(pbuf, "hh", val, val + 1);

		fprintf(fp, "[%d] %d, %d\n", i, val[0], val[1]);

		if(val[0])
		{
			fputs(" <Anchor(entry)> ", fp);
			font_put_Anchor(p, offset + val[0]);
		}

		if(val[1])
		{
			fputs(" <Anchor(exit)> ", fp);
			font_put_Anchor(p, offset + val[1]);
		}
	}

	fputc('\n', fp);

	return font_put_Coverage(p, offset + off, 0);
}

/* lookup4: ベースグリフにマークを配置 */

static int _lookup4(Font *p,FILE *fp,uint32_t offset)
{
	uint16_t off[4],cnt;

	font_putbuf16(p, "+posFormat         ", 0);
	font_putbuf16(p, "+markCoverageOffset", off);
	font_putbuf16(p, "+baseCoverageOffset", off + 1);
	font_putbuf16(p, "+markClassCount    ", &cnt);
	font_putbuf16(p, "+markArrayOffset   ", off + 2);
	font_putbuf16(p, "+baseArrayOffset   ", off + 3);

	//Coverage

	fputc('\n', fp);
	if(font_put_Coverage(p, offset + off[0], "<mark>")) return 1;

	fputc('\n', fp);
	if(font_put_Coverage(p, offset + off[1], "<base>")) return 1;

	//MarkArray

	if(_put_markarray(p, offset + off[2])) return 1;

	//BaseArray

	return _put_basearray(p, offset + off[3], cnt);
}

/* lookup5: 合字にマークを配置 */

static int _lookup5(Font *p,FILE *fp,uint32_t offset)
{
	uint16_t off[4],cnt;

	font_putbuf16(p, "+posFormat          ", 0);
	font_putbuf16(p, "+markCoverageOffset ", off);
	font_putbuf16(p, "+ligatureCoverageOffset", off + 1);
	font_putbuf16(p, "+markClassCount     ", &cnt);
	font_putbuf16(p, "+markArrayOffset    ", off + 2);
	font_putbuf16(p, "+ligatureArrayOffset", off + 3);

	//Coverage

	fputc('\n', fp);
	if(font_put_Coverage(p, offset + off[0], "<mark>")) return 1;

	fputc('\n', fp);
	if(font_put_Coverage(p, offset + off[1], "<ligature>")) return 1;

	//MarkArray

	if(_put_markarray(p, offset + off[2])) return 1;

	//LigatureArray

	return _put_ligarray(p, offset + off[3], cnt);
}

/* lookup6: マークにマークを配置 */

static int _lookup6(Font *p,FILE *fp,uint32_t offset)
{
	uint16_t off[4],cnt;

	font_putbuf16(p, "+posFormat          ", 0);
	font_putbuf16(p, "+mark1CoverageOffset", off);
	font_putbuf16(p, "+mark2CoverageOffset", off + 1);
	font_putbuf16(p, "+markClassCount     ", &cnt);
	font_putbuf16(p, "+mark1ArrayOffset   ", off + 2);
	font_putbuf16(p, "+mark2ArrayOffset   ", off + 3);

	//Coverage

	fputc('\n', fp);
	if(font_put_Coverage(p, offset + off[0], "<mark1>")) return 1;

	fputc('\n', fp);
	if(font_put_Coverage(p, offset + off[1], "<mark2>")) return 1;

	//MarkArray

	if(_put_markarray(p, offset + off[2])) return 1;

	//Mark2Array

	return _put_mark2array(p, offset + off[3], cnt);
}

/* lookup7 */

static int _lookup7(Font *p,FILE *fp,uint32_t offset)
{
	fputs("<unsupported format>\n", fp);
	return 0;
}

/* lookup8 */

static int _lookup8(Font *p,FILE *fp,uint32_t offset)
{
	fputs("<unsupported format>\n", fp);
	return 0;
}

//------------

static int _lookup9(Font *p,FILE *fp,uint32_t offset);

static int (*g_lookup_funcs[])(Font *p,FILE *fp,uint32_t offset) = {
	_lookup1, _lookup2, _lookup3, _lookup4, _lookup5,
	_lookup6, _lookup7, _lookup8, _lookup9
};

//------------

/* lookup 9 (Offset32) */

int _lookup9(Font *p,FILE *fp,uint32_t offset)
{
	uint16_t type;
	uint32_t off;

	font_putbuf16(p, "+posFormat", 0);
	font_putbuf16(p, "+extensionLookupType", &type);
	font_putbuf32(p, "+extensionOffset", &off);

	if(type < 1 || type > 8) return 0;

	offset += off;

	if(font_readbuf_setpos(p, offset)) return 1;

	fputs("---------------\n", fp);

	return (g_lookup_funcs[type - 1])(p, fp, offset);
}

/** Lookup SubTable (生データ) */

int __gpos_raw_lookup_subtable(Font *p,uint32_t offset,int type)
{
	if(type >= 1 && type <= 9)
		return (g_lookup_funcs[type - 1])(p, p->put.fp, offset);
	else
	{
		fputs("# unknown lookupType\n", p->put.fp);
		return 0;
	}
}


