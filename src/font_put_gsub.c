/*$
fontanalz
Copyright (c) 2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/**********************************
 * フォント: GSUB - 通常
 **********************************/

#include <stdint.h>
#include <stdio.h>

#include "def.h"
#include "font.h"
#include "font_pv.h"

//----------------

int __gsub_raw_lookup_subtable(Font *p,uint32_t offset,int type);

//----------------


/* lookup1: 単一置き換え */

static int _lookup1(Font *p,FILE *fp,uint32_t offset)
{
	uint8_t *pbuf;
	uint16_t format,covoff,cnt,gid,*pid;
	int16_t delta;
	ArrayBuf buf;
	int i;

	if(font_readbuf_format(p, "hh", &format, &covoff)
		|| font_get_Coverage_gid(p, offset + covoff, &buf))
		return 1;

	fprintf(fp, "format: %d\n\n", format);

	pid = (uint16_t *)buf.buf;

	if(format == 1)
	{
		if(font_readbuf16(p, &delta)) goto ERR;

		for(i = buf.num; i; i--, pid++)
		{
			font_put_gid_unicode(p, *pid);
			fputs(" -> ", fp);
			font_put_gid_unicode(p, (*pid + delta) & 0xffff);
			fputc('\n', fp);
		}
	}
	else if(format == 2)
	{
		if(font_readbuf16(p, &cnt)
			|| font_readbuf_skip(p, cnt * 2, &pbuf))
			goto ERR;

		for(; cnt; cnt--, pbuf += 2, pid++)
		{
			font_getbuf16(pbuf, &gid);

			font_put_gid_unicode(p, *pid);
			fputs(" -> ", fp);
			font_put_gid_unicode(p, gid);
			fputc('\n', fp);
		}
	}

ERR:
	app_free(buf.buf);

	return 0;
}

/* lookup2,3: 単一から複数へ/代替 */

static int _lookup2_3(Font *p,FILE *fp,uint32_t offset)
{
	uint16_t format,off,cnt,cnt2,gid,*pid;
	uint8_t *pbuf,*pbuf2;
	ArrayBuf buf;

	if(font_readbuf16(p, &format)) return 1;
	if(format != 1) return 0;

	if(font_readbuf_format(p, "hh", &off, &cnt)
		|| font_readbuf_skip(p, cnt * 2, &pbuf)
		|| font_get_Coverage_gid(p, offset + off, &buf))
		return 1;

	pid = (uint16_t *)buf.buf;

	for(; cnt; cnt--, pbuf += 2, pid++)
	{
		font_getbuf16(pbuf, &off);

		font_put_gid_unicode(p, *pid);
		fputs(" -> ", fp);

		//Sequence/AlternateSet

		if(font_readbuf_setpos(p, offset + off)
			|| font_readbuf16(p, &cnt2)
			|| font_readbuf_skip(p, cnt2 * 2, &pbuf2))
			goto ERR;

		for(; cnt2; cnt2--, pbuf2 += 2)
		{
			font_getbuf16(pbuf2, &gid);

			font_put_gid_unicode(p, gid);
			if(cnt2 != 1) fputs("; ", fp);
		}

		fputc('\n', fp);
	}

ERR:
	app_free(buf.buf);
	
	return 0;
}

/* lookup4: 合字置き換え */

static int _lookup4(Font *p,FILE *fp,uint32_t offset)
{
	uint16_t format,off,cnt,cnt2,cnt3,ligid,gid,*pid;
	uint8_t *pbuf,*pbuf2,*pbuf3;
	uint32_t offset2;
	ArrayBuf buf;

	if(font_readbuf16(p, &format)) return 1;
	if(format != 1) return 0;

	if(font_readbuf_format(p, "hh", &off, &cnt)
		|| font_readbuf_skip(p, cnt * 2, &pbuf)
		|| font_get_Coverage_gid(p, offset + off, &buf))
		return 1;

	//

	pid = (uint16_t *)buf.buf;

	for(; cnt; cnt--, pbuf += 2, pid++)
	{
		font_getbuf16(pbuf, &off);

		//LigatureSet

		offset2 = offset + off;

		if(font_readbuf_setpos(p, offset2)
			|| font_readbuf16(p, &cnt2)
			|| font_readbuf_skip(p, cnt2 * 2, &pbuf2))
			goto ERR;

		for(; cnt2; cnt2--, pbuf2 += 2)
		{
			font_getbuf16(pbuf2, &off);

			//Ligature

			if(font_readbuf_setpos(p, offset2 + off)
				|| font_readbuf_format(p, "hh", &ligid, &cnt3)
				|| font_readbuf_skip(p, (cnt3 - 1) * 2, &pbuf3))
				goto ERR;

			font_put_gid_unicode(p, *pid);

			for(cnt3--; cnt3; cnt3--, pbuf3 += 2)
			{
				font_getbuf16(pbuf3, &gid);
			
				fputs(" + ", fp);
				font_put_gid_unicode(p, gid);
			}

			fputs(" -> ", fp);
			font_put_gid_unicode(p, ligid);
			fputc('\n', fp);
		}
	}

ERR:
	app_free(buf.buf);

	return 0;
}

/* unsupport: lookup5,6,8 */

static int _lookup_unsupport(Font *p,FILE *fp,uint32_t offset)
{
	fputs("<unsupported format>\n", fp);
	return 0;
}

//------------

static int _lookup7(Font *p,FILE *fp,uint32_t offset);

static int (*g_lookup_funcs[])(Font *p,FILE *fp,uint32_t offset) = {
	_lookup1, _lookup2_3, _lookup2_3, _lookup4,
	_lookup_unsupport, _lookup_unsupport, _lookup7, _lookup_unsupport
};

//------------


/* lookup7 (Offset32) */

static int _lookup7(Font *p,FILE *fp,uint32_t offset)
{
	uint16_t format,type;
	uint32_t off;

	if(font_readbuf_format(p, "hhi", &format, &type, &off)) return 1;

	fprintf(fp, "<lookupType = %d>\n\n", type);
	
	if(type < 1 || type > 8 || type == 7) return 0;

	offset += off;

	if(font_readbuf_setpos(p, offset)) return 1;

	return (g_lookup_funcs[type - 1])(p, fp, offset);
}

/* Lookup SubTable */

static int _lookup_subtable(Font *p,uint32_t offset,int type)
{
	if(type >= 1 && type <= 8)
		return (g_lookup_funcs[type - 1])(p, p->put.fp, offset);
	else
	{
		fputs("# unknown lookupType\n", p->put.fp);
		return 0;
	}
}

/** GSUB 出力 */

int font_put_gsub(Font *p)
{
	int ret;
	uint16_t min,off16[3];

	ret = font_read_table(p, MAKE_TAG('G','S','U','B'));
	if(ret) return ret;

	//------ 出力

	ret = font_openput(p, "GSUB.txt", " 'GSUB' table");
	if(ret) return ret;

	font_putbuf16(p, "+majorVersion     ", 0);
	font_putbuf16(p, "+minorVersion     ", &min);
	font_putbuf16(p, "+scriptListOffset ", off16);
	font_putbuf16(p, "+featureListOffset", off16 + 1);
	font_putbuf16(p, "+lookupListOffset ", off16 + 2);

	if(min >= 1)
		font_putbuf32(p, "+featureVariationsOffset", 0);

	//

	if(g_app.optflags & OPTF_RAW)
	{
		font_put_ScriptList(p, off16[0]);
		font_put_FeatureList(p, off16[1]);
		font_put_LookupList(p, off16[2], __gsub_raw_lookup_subtable, off16[1]);
	}
	else
	{
		font_put_LookupList(p, off16[2], _lookup_subtable, off16[1]);
	}

	return font_closeput_ret(p);
}

