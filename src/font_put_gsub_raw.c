/*$
fontanalz
Copyright (c) 2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/**********************************
 * フォント: GSUB - 生データ
 **********************************/

#include <stdint.h>
#include <stdio.h>

#include "font.h"
#include "font_pv.h"


/* lookup1: 単一置き換え */

static int _lookup1(Font *p,FILE *fp,uint32_t offset)
{
	uint16_t format,covoff,cnt;
	int delta;

	font_putbuf16(p, "+substFormat   ", &format);
	font_putbuf16(p, "+coverageOffset", &covoff);

	if(format == 1)
	{
		font_putbuf16(p, "-deltaGlyphID  ", &delta);
	}
	else if(format == 2)
	{
		font_putbuf16(p, "+glyphCount    ", &cnt);
		fputs("u16 substituteGlyphIDs[GlyphCount]:\n", fp);

		font_putbuf16_ar(p, cnt, 0);
	}

	fputc('\n', fp);

	return font_put_Coverage(p, offset + covoff, 0);
}

/* lookup2: 単一から複数へ */

static int _lookup2(Font *p,FILE *fp,uint32_t offset)
{
	uint16_t format,covoff,cnt,off,cnt2,i;
	uint8_t *pbuf;

	font_putbuf16(p, "+substFormat   ", &format);
	font_putbuf16(p, "+coverageOffset", &covoff);
	font_putbuf16(p, "+sequenceCount ", &cnt);
	fputs("u16 sequenceOffsets[sequenceCount]:\n", fp);

	if(font_readbuf_skip(p, cnt * 2, &pbuf)) return 1;

	//Sequence

	for(i = 0; i < cnt; i++, pbuf += 2)
	{
		font_getbuf16(pbuf, &off);

		fprintf(fp, "-- [%d] offset=%d\n", i, off);

		if(font_readbuf_setpos(p, offset + off)) return 1;

		font_putbuf16(p, "+glyphCount", &cnt2);
		fputs("u16 substituteGlyphIDs[glyphCount]:\n", fp);

		font_putbuf16_ar(p, cnt2, 0);
	}

	fputc('\n', fp);

	return font_put_Coverage(p, offset + covoff, 0);
}

/* lookup3: 代替置き換え */

static int _lookup3(Font *p,FILE *fp,uint32_t offset)
{
	uint16_t format,covoff,cnt,off,cnt2,i;
	uint8_t *pbuf;

	font_putbuf16(p, "+substFormat      ", &format);
	font_putbuf16(p, "+coverageOffset   ", &covoff);
	font_putbuf16(p, "+alternateSetCount", &cnt);
	fputs("u16 alternateSetOffsets[alternateSetCount]:\n", fp);

	if(font_readbuf_skip(p, cnt * 2, &pbuf)) return 1;

	//AlternateSet

	for(i = 0; i < cnt; i++, pbuf += 2)
	{
		font_getbuf16(pbuf, &off);

		fprintf(fp, "-- [%d] offset=%d\n", i, off);

		if(font_readbuf_setpos(p, offset + off)) return 1;

		font_putbuf16(p, "+glyphCount", &cnt2);
		fputs("u16 alternateGlyphIDs[glyphCount]:\n", fp);

		font_putbuf16_ar(p, cnt2, 0);
	}

	fputc('\n', fp);

	return font_put_Coverage(p, offset + covoff, 0);
}

/* lookup4: 合字置き換え */

static int _lookup4(Font *p,FILE *fp,uint32_t offset)
{
	uint16_t format,covoff,cnt,off,cnt2,cnt3,i,j;
	uint8_t *pbuf,*pbuf2;
	uint32_t offset2;

	font_putbuf16(p, "+substFormat     ", &format);
	font_putbuf16(p, "+coverageOffset  ", &covoff);
	font_putbuf16(p, "+ligatureSetCount", &cnt);
	fputs("u16 ligatureSetOffsets[ligatureSetCount]:\n", fp);

	if(font_readbuf_skip(p, cnt * 2, &pbuf)) return 1;

	//

	for(i = 0; i < cnt; i++, pbuf += 2)
	{
		font_getbuf16(pbuf, &off);

		fprintf(fp, "\n-- LigatureSet[%d] offset=%d --\n", i, off);

		//LigatureSet

		offset2 = offset + off;

		if(font_readbuf_setpos(p, offset2)) return 1;

		font_putbuf16(p, "+ligatureCount", &cnt2);
		fputs("u16 ligatureOffsets[LigatureCount]:\n", fp);

		if(font_readbuf_skip(p, cnt2 * 2, &pbuf2)) return 1;

		for(j = 0; j < cnt2; j++, pbuf2 += 2)
		{
			font_getbuf16(pbuf2, &off);

			//Ligature

			if(font_readbuf_setpos(p, offset2 + off)) return 1;

			fprintf(fp, "{Ligature[%d] offset=%d}\n", j, off);

			font_putbuf16(p, "+ligatureGlyph ", 0);
			font_putbuf16(p, "+componentCount", &cnt3);
			fputs("u16 componentGlyphIDs[componentCount - 1]:\n", fp);

			font_putbuf16_ar(p, cnt3 - 1, 0);
		}
	}

	fputc('\n', fp);

	return font_put_Coverage(p, offset + covoff, 0);
}

/* unsupport: lookup5,6,8 */

static int _lookup_unsupport(Font *p,FILE *fp,uint32_t offset)
{
	fputs("<unsupported format>\n", fp);
	return 0;
}

//------------

static int _lookup7(Font *p,FILE *fp,uint32_t offset);

static int (*g_lookup_funcs[])(Font *p,FILE *fp,uint32_t offset) = {
	_lookup1, _lookup2, _lookup3, _lookup4,
	_lookup_unsupport, _lookup_unsupport, _lookup7, _lookup_unsupport
};

//------------

/* lookup 7 (Offset32) */

int _lookup7(Font *p,FILE *fp,uint32_t offset)
{
	uint16_t type;
	uint32_t off;

	font_putbuf16(p, "+posFormat", 0);
	font_putbuf16(p, "+extensionLookupType", &type);
	font_putbuf32(p, "+extensionOffset", &off);

	if(type < 1 || type > 8 || type == 7) return 0;

	offset += off;

	if(font_readbuf_setpos(p, offset)) return 1;

	fputs("---------------\n", fp);

	return (g_lookup_funcs[type - 1])(p, fp, offset);
}

/** Lookup SubTable (生データ) */

int __gsub_raw_lookup_subtable(Font *p,uint32_t offset,int type)
{
	if(type >= 1 && type <= 8)
		return (g_lookup_funcs[type - 1])(p, p->put.fp, offset);
	else
	{
		fputs("# unknown lookupType\n", p->put.fp);
		return 0;
	}
}

