/*$
fontanalz
Copyright (c) 2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*************************************
 * フォント: OpenType レイアウト関連
 *************************************/

#include <stdint.h>
#include <stdio.h>

#include "def.h"
#include "font.h"
#include "font_pv.h"


//========================
// ScriptList
//========================


/* LangSys */

static int _put_langsys(Font *p,uint32_t offset)
{
	uint16_t cnt;

	if(font_readbuf_setpos(p, offset)) return 1;

	font_putbuf16(p, "+lookupOrder", 0);
	font_putbuf16(p, "+requiredFeatureIndex", 0);
	font_putbuf16(p, "+featureIndexCount   ", &cnt);
	fputs("u16 featureIndices[featureIndexCount]:\n", p->put.fp);

	font_putbuf16_ar(p, cnt, 0);

	return 0;
}

/** ScriptList テーブル */

int font_put_ScriptList(Font *p,uint32_t offset_top)
{
	FILE *fp = p->put.fp;
	uint8_t *pbuf,*psysrec,*pbuf2;
	uint16_t cnt,i,j,off16,defsys,syscnt;
	uint32_t tag,offset;

	if(!offset_top) return 0;

	if(font_readbuf_setpos(p, offset_top)) return 1;

	font_put_header(p, "ScriptList");

	font_putbuf16(p, "+scriptCount", &cnt);
	fputs("ScriptRecord scriptRecords[scriptCount]:\n", fp);

	if(font_readbuf_skip(p, cnt * 6, &pbuf)) return 1;

	for(i = 0; i < cnt; i++, pbuf += 6)
	{
		//ScriptRecord
		
		font_getbuf_format(pbuf, "ih", &tag, &off16);

		fprintf(fp, "\n--- [%d] ", i);
		font_put_tagname(p, tag);
		fprintf(fp, ", offset=%d ---\n\n", off16);

		//Script

		offset = offset_top + off16;

		if(font_readbuf_setpos(p, offset)) return 1;

		font_putbuf16(p, "+defaultLangSys", &defsys);
		font_putbuf16(p, "+langSysCount  ", &syscnt);
		fputs("LangSysRecord langSysRecords[langSysCount]:\n", fp);

		if(syscnt)
			fputs("# u32 langSysTag, u16 langSysOffset\n", fp);

		if(font_readbuf_skip(p, syscnt * 6, &psysrec)) return 1;

		pbuf2 = psysrec;

		for(j = 0; j < syscnt; j++, pbuf2 += 6)
		{
			//LangSysRecord

			font_getbuf_format(pbuf2, "ih", &tag, &off16);

			fprintf(fp, "[%d] ", j);
			font_put_tagname(p, tag);
			fprintf(fp, ", %d\n", off16);
		}

		//defaultLangSys

		if(defsys)
		{
			fputs("\n< LangSys (defaultLangSys) >\n", fp);
			_put_langsys(p, offset + defsys);
		}

		//LangSysRecord::langSysOffset

		pbuf2 = psysrec;

		for(j = 0; j < syscnt; j++, pbuf2 += 6)
		{
			font_getbuf_format(pbuf2, "ih", &tag, &off16);

			fprintf(fp, "\n< LangSys (langSysRecord[%d]:", j);
			font_put_tagname(p, tag);
			fprintf(fp, ") >\n");

			_put_langsys(p, offset + off16);
		}
	}

	return 0;
}


//========================
// FeatureList
//========================


/** FeatureList */

int font_put_FeatureList(Font *p,uint32_t offset)
{
	FILE *fp = p->put.fp;
	uint8_t *pbuf;
	uint16_t i,cnt,off16,cnt2;
	uint32_t tag;

	if(!offset) return 0;

	if(font_readbuf_setpos(p, offset)) return 1;

	font_put_header(p, "FeatureList");

	font_putbuf16(p, "+featureCount", &cnt);
	fputs("FeatureRecord featureRecords[featureCount]:\n", fp);

	if(font_readbuf_skip(p, cnt * 6, &pbuf)) return 1;

	for(i = 0; i < cnt; i++, pbuf += 6)
	{
		//FeatureRecord
		
		font_getbuf_format(pbuf, "ih", &tag, &off16);

		fprintf(fp, "\n--- [%d] ", i);
		font_put_tagname(p, tag);
		fprintf(fp, ", offset=%d ---\n\n", off16);

		//Feature

		if(font_readbuf_setpos(p, offset + off16)) return 1;

		font_putbuf16(p, "+featureParamsOffset", 0);
		font_putbuf16(p, "+lookupIndexCount   ", &cnt2);
		fputs("u16 lookupListIndices[lookupIndexCount]:\n", fp);
		font_putbuf16_ar(p, cnt2, 0);
	}

	return 0;
}


//========================
// LookupList
//========================


/* FeatureList から、最初に lookup index が含まれるタグを取得 */

static int _search_feature_lookup(Font *p,uint32_t offset,int index,uint32_t *dst_tag)
{
	uint8_t *pbuf,*pbuf2;
	uint16_t cnt,off,cnt2,ind;
	uint32_t tag;

	*dst_tag = 0;

	if(font_readbuf_setpos(p, offset)
		|| font_readbuf16(p, &cnt)
		|| font_readbuf_skip(p, cnt * 6, &pbuf))
		return 1;

	//FeatureRecord

	for(; cnt; cnt--, pbuf += 6)
	{
		font_getbuf_format(pbuf, "ih", &tag, &off);

		//Feature

		if(font_readbuf_setpos(p, offset + off)
			|| font_readbuf_format(p, "s2h", &cnt2)
			|| font_readbuf_skip(p, cnt2 * 2, &pbuf2))
			return 1;

		for(; cnt2; cnt2--, pbuf2 += 2)
		{
			font_getbuf16(pbuf2, &ind);

			if(ind == index)
			{
				*dst_tag = tag;
				return 0;
			}
		}
	}

	return 1;
}

/** LookupList
 *
 * feature_offset: feature タグを表示するために必要 */

int font_put_LookupList(Font *p,uint32_t offset,FontFunc_LookupSubTable func,int feature_offset)
{
	FILE *fp = p->put.fp;
	uint8_t *pbuf,*pbuf2;
	uint16_t cnt,i,j,off16,type,subcnt,flag;
	uint32_t offset2,featag;

	if(!offset) return 0;

	if(font_readbuf_setpos(p, offset)) return 1;

	font_put_header(p, "LookupList");

	font_putbuf16(p, "+lookupCount", &cnt);
	fputs("u16 lookups[lookupCount]:\n", fp);

	if(font_readbuf_skip(p, cnt * 2, &pbuf)) return 1;

	for(i = 0; i < cnt; i++, pbuf += 2)
	{
		font_getbuf16(pbuf, &off16);

		_search_feature_lookup(p, feature_offset, i, &featag);

		fputs("\n------------------------------\n", fp);
		fprintf(fp, " Lookup[%d] offset=%d ", i, off16);
		if(featag) font_put_tagname(p, featag);
		fputs("\n------------------------------\n\n", fp);

		//Lookup

		offset2 = offset + off16;

		if(font_readbuf_setpos(p, offset2)) return 1;

		font_putbuf16(p, "+lookupType   ", &type);
		font_putbuf16(p, "#lookupFlag   ", &flag);
		font_putbuf16(p, "+subTableCount", &subcnt);

		fputs("u16 subtableOffsets[subTableCount]:\n", fp);
		pbuf2 = p->readbuf_cur;

		if(font_putbuf16_ar(p, subcnt, 0)) return 1;

		if(flag & 0x10)
			font_putbuf16(p, "+markFilteringSet", 0);

		//subtable

		for(j = 0; j < subcnt; j++, pbuf2 += 2)
		{
			font_getbuf16(pbuf2, &off16);

			if(font_readbuf_setpos(p, offset2 + off16)) return 1;

			fprintf(fp, "\n--- SubTable[%d] lookupType=%d, offset=%d ---\n\n", j, type, off16);

			if((func)(p, offset2 + off16, type))
			{
				p->readbuf_err = 1;
				return 1;
			}
		}
	}

	return 0;
}


//========================
// ほか
//========================


/** Coverage テーブル:生データ <PUSH>
 *
 * mes: 0 以外で、追加ヘッダ文字列 */

int font_put_Coverage(Font *p,uint32_t offset,const char *mes)
{
	FILE *fp = p->put.fp;
	uint8_t *pbuf;
	uint16_t i,format,cnt,v[3];

	if(font_readbuf_push_pos_goto(p, offset)) return 1;

	fputs(">>> {Coverage}", fp);
	if(mes) fprintf(fp, " %s", mes);
	fputc('\n', fp);

	font_putbuf16(p, "+coverageFormat", &format);

	if(format == 1)
	{
		//配列
		
		font_putbuf16(p, "+glyphCount", &cnt);
		fputs("u16 glyphArray[glyphCount]:\n", fp);
		font_putbuf16_ar(p, cnt, 0);
	}
	else if(format == 2)
	{
		//範囲
		
		font_putbuf16(p, "+rangeCount", &cnt);
		
		fputs("RangeRecord rangeRecords[rangeCount]:\n", fp);
		fputs("# u16 startGlyphID, endGlyphID, startCoverageIndex\n", fp);

		if(font_readbuf_skip(p, cnt * 3 * 2, &pbuf)) goto ERR;

		for(i = 0; i < cnt; i++, pbuf += 6)
		{
			font_getbuf16_ar(pbuf, 3, v);
			fprintf(fp, "[%d] %d, %d, %d\n", i, v[0], v[1], v[2]);
		}
	}
	else
		fputs("# unknown coverageFormat\n", fp);

	fputs("<<< {Coverage}\n", fp);

ERR:
	font_readbuf_pop_pos(p);

	return p->readbuf_err;
}

/** Coverage テーブル:GID の配列取得 (u16) <PUSH>
 *
 * coverage index に対応した GID の配列を作成する。 */

int font_get_Coverage_gid(Font *p,uint32_t offset,ArrayBuf *dst)
{
	uint8_t *pbuf,*pbk;
	uint16_t format,cnt,start,end,ind,*pd;
	int num,i,n;

	app_memset0(dst, sizeof(ArrayBuf));

	if(font_readbuf_push_pos_goto(p, offset)) return 1;

	if(font_readbuf16(p, &format)) goto ERR;

	if(format == 1)
	{
		//配列

		if(font_readbuf16(p, &cnt)
			|| font_readbuf_arraybuf16(p, cnt, dst))
			goto ERR;
	}
	else if(format == 2)
	{
		//---- 範囲

		if(font_readbuf16(p, &cnt)
			|| font_readbuf_skip(p, cnt * 3 * 2, &pbuf))
			goto ERR;

		pbk = pbuf;

		//個数計算

		num = 0;
		
		for(i = cnt; i; i--, pbuf += 6)
		{
			font_getbuf_format(pbuf, "hhh", &start, &end, &ind);

			n = ind + (end - start + 1);
			if(num < n) num = n;
		}

		//セット

		if(font_alloc_arraybuf(dst, num, num * 2)) goto ERR;

		pd = (uint16_t *)dst->buf;

		for(pbuf = pbk, i = cnt; i; i--, pbuf += 6)
		{
			font_getbuf_format(pbuf, "hhh", &start, &end, &ind);

			for(; start <= end; start++, ind++)
				pd[ind] = start;
		}
	}

	font_readbuf_pop_pos(p);
	return 0;

ERR:
	font_readbuf_pop_pos(p);
	return 1;
}

/** ClassDef テーブル <PUSH>
 *
 * グリフのクラス定義 */

int font_put_ClassDef(Font *p,uint32_t offset)
{
	FILE *fp = p->put.fp;
	uint8_t *pbuf;
	uint16_t format,start,cnt,i,v[3];

	if(font_readbuf_push_pos_goto(p, offset)) return 1;

	fputs(">>> {ClassDef}\n", fp);

	font_putbuf16(p, "+classFormat", &format);

	if(format == 1)
	{
		//配列
		
		font_putbuf16(p, "+startGlyphID", &start);
		font_putbuf16(p, "+glyphCount  ", &cnt);
		fputs("u16 classValueArray[glyphCount]:\n", fp);
		font_putbuf16_ar(p, cnt, 0);
	}
	else if(format == 2)
	{
		//範囲

		font_putbuf16(p, "+classRangeCount", &cnt);
		
		fputs("ClassRangeRecord classRangeRecords[classRangeCount]:\n", fp);
		fputs("# u16 startGlyphID, endGlyphID, class\n", fp);

		if(font_readbuf_skip(p, cnt * 3 * 2, &pbuf)) goto ERR;

		for(i = 0; i < cnt; i++, pbuf += 6)
		{
			font_getbuf16_ar(pbuf, 3, v);
			fprintf(fp, "[%d] %d, %d, %d\n", i, v[0], v[1], v[2]);
		}
	}
	else
		fputs("# unknown classFormat\n", fp);

	fputs("<<< {ClassDef}\n", fp);

ERR:
	font_readbuf_pop_pos(p);

	return p->readbuf_err;
}

/** ClassDef から、各GIDのクラスを出力 <PUSH> */

int font_put_ClassDef_gid(Font *p,uint32_t offset)
{
	FILE *fp = p->put.fp;
	uint8_t *pbuf;
	uint16_t format,start,end,cnt,class;

	if(font_readbuf_push_pos_goto(p, offset)) return 1;
	
	if(font_readbuf16(p, &format)) goto ERR;

	if(format == 1)
	{
		//配列

		if(font_readbuf_format(p, "hh", &start, &cnt)
			|| font_readbuf_skip(p, cnt * 2, &pbuf))
			goto ERR;

		for(; cnt; cnt--, pbuf += 2, start++)
		{
			font_getbuf16(pbuf, &class);

			font_put_gid_unicode(p, start);
			fprintf(fp, ": class %d\n", class);
		}
	}
	else if(format == 2)
	{
		//範囲

		if(font_readbuf16(p, &cnt)
			|| font_readbuf_skip(p, cnt * 6, &pbuf))
			goto ERR;

		for(; cnt; cnt--, pbuf += 6)
		{
			font_getbuf_format(pbuf, "hhh", &start, &end, &class);

			for(; start <= end; start++)
			{
				font_put_gid_unicode(p, start);
				fprintf(fp, ": class %d\n", class);
			}
		}
	}

ERR:
	font_readbuf_pop_pos(p);

	return p->readbuf_err;
}
