/*$
fontanalz
Copyright (c) 2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/**********************************
 * フォント: EBLC
 **********************************/

#include <stdint.h>
#include <stdio.h>

#include "font.h"
#include "font_pv.h"


/* BigGlyphMetrics 出力 */

static void _put_big_metrics(Font *p)
{
	fputs("{<BigGlyphMetrics>\n", p->put.fp);
	font_putbuf8(p, "+height      ", 0);
	font_putbuf8(p, "+width       ", 0);
	font_putbuf8(p, "-horiBearingX", 0);
	font_putbuf8(p, "-horiBearingY", 0);
	font_putbuf8(p, "+horiAdvance ", 0);
	font_putbuf8(p, "-vertBearingX", 0);
	font_putbuf8(p, "-vertBearingY", 0);
	font_putbuf8(p, "+vertAdvance ", 0);
	fputs("}\n", p->put.fp);
}


/* SbitLineMetrics 出力 */

static void _put_line_metrics(Font *p)
{
	font_putbuf8(p, "-ascender             ", 0);
	font_putbuf8(p, "-descender            ", 0);
	font_putbuf8(p, "+widthMax             ", 0);
	font_putbuf8(p, "-caretSlopeNumerator  ", 0);
	font_putbuf8(p, "-caretSlopeDenominator", 0);
	font_putbuf8(p, "-caretOffset          ", 0);
	font_putbuf8(p, "-minOriginSB          ", 0);
	font_putbuf8(p, "-minAdvanceSB         ", 0);
	font_putbuf8(p, "-maxBeforeBL          ", 0);
	font_putbuf8(p, "-minAfterBL           ", 0);
	font_putbuf16(p, "-pad                  ", 0);
	fputc('\n', p->put.fp);
}

/* IndexSubTable */

static void _put_subtable(Font *p,FILE *fp,int gnum)
{
	uint16_t format,v16[2];
	uint32_t i,num;

	fputs("<IndexSubTable>\n", fp);

	font_putbuf16(p, "+indexFormat    ", &format);
	font_putbuf16(p, "+imageFormat    ", 0);
	font_putbuf32(p, "+imageDataOffset", 0);

	switch(format)
	{
		case 1:
			fprintf(fp, "u32 offsetArray[%d]:\n", gnum);
			font_putbuf32_ar(p, gnum, 0);
			break;
		case 2:
			font_putbuf32(p, "+imageSize      ", 0);
			_put_big_metrics(p);
			break;
		case 3:
			fprintf(fp, "u16 offsetArray[%d]:\n", gnum);
			font_putbuf16_ar(p, gnum, 0);
			break;
		case 4:
			font_putbuf32(p, "+numGlyphs", &num);
			fputs("GlyphIdOffsetPair glyphArray[numGlyphs + 1]:\n# u16 glyphID, offset\n", fp);

			for(i = 0; i <= num; i++)
			{
				font_readbuf16(p, v16);
				font_readbuf16(p, v16 + 1);
				fprintf(fp, "[%u] %d, %d\n", i, v16[0], v16[1]);
			}
			break;
		case 5:
			font_putbuf32(p, "+imageSize      ", 0);
			_put_big_metrics(p);
			font_putbuf32(p, "+numGlyphs", &num);
			fputs("u16 glyphIdArray[numGlyphs]:\n", fp);
			font_putbuf16_ar(p, num, 0);
			break;
		default:
			fputs("{unknown}\n", fp);
	}
}

/** EBLC 出力 */

int font_put_eblc(Font *p)
{
	FILE *fp;
	int ret;
	uint32_t i,j,num,offset,offset2,stnum;
	uint16_t first,last;

	ret = font_read_table(p, MAKE_TAG('E','B','L','C'));
	if(ret) return ret;

	//------ 出力

	ret = font_openput(p, "EBLC.txt", " 'EBLC' table");
	if(ret) return ret;

	fp = p->put.fp;

	font_putbuf16(p, "+majorVersion", 0);
	font_putbuf16(p, "+minorVersion", 0);
	font_putbuf32(p, "+numSizes    ", &num);

	for(i = 0; i < num; i++)
	{
		fputs("\n-----------------------\n", fp);
		fprintf(fp, "  BitmapSize[%d]\n", i);
		fputs("-----------------------\n\n", fp);
	
		font_putbuf32(p, "+indexSubTableArrayOffset", &offset);
		font_putbuf32(p, "+indexTablesSize         ", 0);
		font_putbuf32(p, "+numberOfIndexSubTables  ", &stnum);
		font_putbuf32(p, "+colorRef                ", 0);
		
		fputs("\n<SbitLineMetrics hori>\n", fp);
		_put_line_metrics(p);
		
		fputs("<SbitLineMetrics vert>\n", fp);
		_put_line_metrics(p);

		font_putbuf16(p, "+startGlyphIndex", 0);
		font_putbuf16(p, "+endGlyphIndex  ", 0);
		font_putbuf8(p,  "+ppemX          ", 0);
		font_putbuf8(p,  "+ppemY          ", 0);
		font_putbuf8(p,  "+bitDepth       ", 0);
		font_putbuf8(p,  "$flags          ", 0);

		//indexSubTableArray

		if(font_readbuf_push_pos_goto(p, offset)) goto ERR;

		fprintf(fp, "\n<IndexSubTableArray> offset:%u\n", offset);

		for(j = 0; j < stnum; j++)
		{
			fprintf(fp, "\n---- [%d] ----\n", j);
			font_putbuf16(p, "+firstGlyphIndex", &first);
			font_putbuf16(p, "+lastGlyphIndex ", &last);
			font_putbuf32(p, "+additionalOffsetToIndexSubtable", &offset2);

			//

			if(font_readbuf_push_pos_goto(p, offset + offset2)) goto ERR;

			_put_subtable(p, fp, last - first + 1);

			font_readbuf_pop_pos(p);
		}

		font_readbuf_pop_pos(p);
	}

ERR:
	return font_closeput_ret(p);
}

