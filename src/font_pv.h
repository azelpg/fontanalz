/*$
fontanalz
Copyright (c) 2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

typedef struct _Font Font;
typedef struct _cffdat cffdat;
typedef int (*FontFunc_LookupSubTable)(Font *p,uint32_t offset,int lookup_type);

#define MAKE_TAG(a,b,c,d)  ((a << 24) | (b << 16) | (c << 8) | d)
#define FONT_READBUF_OFFSET_NUM  2

//出力
typedef struct
{
	FILE *fp;
	int fclose;
}FontPut;

//テーブル
typedef struct
{
	uint32_t offset,size;
}FontTbl;

//Font
struct _Font
{
	FILE *fp;
	FontPut put;
	
	uint32_t *tblrec_buf,	//テーブルレコード (32bit x 4: tag,checksum,offset,len)
			*gidunimap;		//GID -> Unicode 変換マップ
	uint8_t *readbuf,		//読み込んだバッファ
		*readbuf_cur;		//現在の読み込み位置

	cffdat *pcffdat;		//CFF 時

	int is_cff_file,		//CFF ファイルか
		tblrec_num,			//テーブルレコードの数
		loca_offset_size,	//loca テーブルのオフセットサイズ (2 or 4)
		readbuf_err,		//readbuf 読み込みエラー (0=成功)
		readbuf_offset_num;	//現在の記録オフセット数 (0 で空)
	uint16_t maxp_gnum;		//(maxp) グリフ数
	uint32_t ttcf_num,		//コレクションのフォント数 (0 でなし)
		sfnt_offset,		//対象の SFNT テーブルのオフセット (0 以外で TTCF あり)
		readbuf_size,		//readbuf のサイズ
		readbuf_remain,		//readbuf 残りサイズ
		readbuf_offset[FONT_READBUF_OFFSET_NUM];	//オフセット記録
};

//配列データ
typedef struct
{
	void *buf;
	int num;
}ArrayBuf;


int font_make_gidunimap(Font *p);

/*--- read ---*/

int font_alloc_arraybuf(ArrayBuf *p,int num,int size);

int font_goto_table(Font *p,uint32_t tag,FontTbl *dst);
void font_free_readbuf(Font *p);
int font_read_to_readbuf(Font *p,uint32_t offset,uint32_t size);
int font_readfull_to_readbuf(Font *p);
int font_read_table(Font *p,uint32_t tag);
int font_read_table_val(Font *p,uint32_t tag,int offset,int format,void *dst);
int font_read_loca_offset_size(Font *p,int *dst);

int font_readbuf_err(Font *p);
uint32_t font_readbuf_getpos(Font *p);
int font_readbuf_getposptr(Font *p,uint32_t pos,int size,uint8_t **dst);
int font_readbuf_setpos(Font *p,uint32_t pos);

void font_readbuf_push_pos(Font *p);
int font_readbuf_push_pos_goto(Font *p,uint32_t pos);
void font_readbuf_pop_pos(Font *p);
int font_readbuf_skip(Font *p,int size,uint8_t **dst);
int font_readbuf8(Font *p,void *dst);
int font_readbuf16(Font *p,void *dst);
int font_readbuf24(Font *p,void *dst);
int font_readbuf32(Font *p,void *dst);
int font_readbuf64(Font *p,void *dst);
int font_readbuf_nsize(Font *p,int size,uint32_t *dst);
int font_readbuf_format(Font *p,const char *fmt,...);
int font_readbuf_arraybuf16(Font *p,int num,ArrayBuf *dst);

void font_getbuf16(const uint8_t *src,void *dst);
void font_getbuf32(const uint8_t *src,void *dst);
void font_getbuf16_ar(const uint8_t *src,int cnt,void *dst);
void font_getbuf_format(const void *buf,const char *fmt,...);
int font_getbuf_nsize(const uint8_t *buf,int size,uint32_t *dst);

int font_seek(Font *p,int size);
int font_seek_top(Font *p,uint32_t size);
int font_read16(Font *p,void *ptr);
int font_read32(Font *p,void *ptr);
int font_read16_array(Font *p,void *ptr,int num);
int font_read32_array(Font *p,void *ptr,int num);
int font_read16ar_alloc(Font *p,void **pptr,int num);
int font_read32ar_alloc(Font *p,void **pptr,int num);
int font_read_alloc(Font *p,uint32_t offset,int size,void **dst);
int font_reads(Font *p,const char *fmt,...);

/*--- write ---*/

int font_openput(Font *p,const char *filename,const char *header);
int font_openput_fno(Font *p,const char *prefix,const char *suffix,int no);
void font_closeput(Font *p);
int font_closeput_ret(Font *p);

void font_put_header(Font *p,const char *name);
void font_put_tagname(Font *p,uint32_t v);
void font_put_fixed(Font *p,int32_t v);
void font_put_bits_on(Font *p,uint32_t f,int cnt);
void font_put_bits_name(Font *p,uint32_t f,int cnt,const char *names);
void font_put_gid_unicode(Font *p,int gid);

int font_putbuf8(Font *p,const char *name,void *dst);
int font_putbuf16(Font *p,const char *name,void *dst);
int font_putbuf24(Font *p,const char *name,void *dst);
int font_putbuf32(Font *p,const char *name,void *dst);
int font_putbuf8_ar(Font *p,const char *name,int cnt);
int font_putbuf16_ar(Font *p,int cnt,int format);
int font_putbuf32_ar(Font *p,int cnt,int format);
int font_putbuf_date(Font *p,const char *name);

void font_put_uni_utf8(Font *p,uint32_t c);
void font_putbuf_utf16be(Font *p,const void *src,int len);
void font_putbuf_string(Font *p,int platid,int str_offset,int offset,int len);

/*--- put_layout ---*/

int font_put_ScriptList(Font *p,uint32_t offset_top);
int font_put_FeatureList(Font *p,uint32_t offset);
int font_put_LookupList(Font *p,uint32_t offset,FontFunc_LookupSubTable func,int feature_offset);
int font_put_Coverage(Font *p,uint32_t offset,const char *mes);
int font_get_Coverage_gid(Font *p,uint32_t offset,ArrayBuf *dst);
int font_put_ClassDef(Font *p,uint32_t offset);
int font_put_ClassDef_gid(Font *p,uint32_t offset);

/*--- GPOS ---*/

void font_put_ValueRecord(Font *p,uint16_t flags,int fenter);
int font_put_Anchor(Font *p,uint32_t offset);
int font_put_Anchor_for0(Font *p,uint32_t offset,uint16_t offrel);

