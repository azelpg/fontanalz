/*$
fontanalz
Copyright (c) 2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/**********************************
 * フォント: 読み込み
 **********************************/

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

#include "def.h"
#include "font.h"
#include "font_pv.h"



/** ArrayBuf の確保 */

int font_alloc_arraybuf(ArrayBuf *p,int num,int size)
{
	p->buf = app_malloc(size);
	if(!p->buf) return 1;

	p->num = num;

	return 0;
}

/** 指定タグのテーブル位置へ
 *
 * dst: NULL 以外で、情報が入る
 * return: 見つからなかった場合、FONTERR_UNFOUND */

int font_goto_table(Font *p,uint32_t tag,FontTbl *dst)
{
	uint32_t *ps = p->tblrec_buf;
	int i;

	for(i = p->tblrec_num; i; i--, ps += 4)
	{
		if(*ps == tag)
		{
			if(dst)
			{
				dst->offset = ps[2];
				dst->size = ps[3];
			}
			
			if(fseek(p->fp, ps[2], SEEK_SET) == 0)
				return 0;
			else
				return FONTERR_READ;
		}
	}

	return FONTERR_UNFOUND;
}

/** readbuf 解放 */

void font_free_readbuf(Font *p)
{
	app_free(p->readbuf);

	p->readbuf = NULL;
	p->readbuf_cur = NULL;
}

/** readbuf に読み込み
 *
 * offset: ファイル先頭からの位置。0 で現在位置*/

int font_read_to_readbuf(Font *p,uint32_t offset,uint32_t size)
{
	uint8_t *buf;

	if(offset && fseek(p->fp, offset, SEEK_SET))
		return FONTERR_READ;

	font_free_readbuf(p);

	buf = (uint8_t *)app_malloc(size);
	if(!buf) return FONTERR_ALLOC;

	if(fread(buf, 1, size, p->fp) != size)
	{
		app_free(buf);
		return FONTERR_READ;
	}

	p->readbuf = p->readbuf_cur = buf;
	p->readbuf_size = size;
	p->readbuf_remain = size;
	p->readbuf_err = 0;
	p->readbuf_offset_num = 0;

	return 0;
}

/** ファイル全体を readbuf に読み込み */

int font_readfull_to_readbuf(Font *p)
{
	long size;

	fseek(p->fp, 0, SEEK_END);
	size = ftell(p->fp);
	fseek(p->fp, 0, SEEK_SET);

	return font_read_to_readbuf(p, 0, size);
}

/** 指定テーブルをメモリに読み込み (readbuf) */

int font_read_table(Font *p,uint32_t tag)
{
	FontTbl dat;
	int ret;

	ret = font_goto_table(p, tag, &dat);
	if(ret) return ret;

	return font_read_to_readbuf(p, dat.offset, dat.size);
}

/** 指定テーブルの位置から値読み込み
 *
 * format: 0=16bit */

int font_read_table_val(Font *p,uint32_t tag,int offset,int format,void *dst)
{
	FontTbl dat;
	int ret;

	ret = font_goto_table(p, tag, &dat);
	if(ret) return ret;

	if(offset >= dat.size
		|| font_seek_top(p, dat.offset + offset))
		return FONTERR_READ;

	if(format == 0)
		ret = font_read16(p, dst);
	else
		ret = 0;

	return ret;
}

/** head から loca のオフセットサイズ読み込み */

int font_read_loca_offset_size(Font *p,int *dst)
{
	int ret;
	uint16_t v16;

	ret = font_read_table_val(p, MAKE_TAG('h','e','a','d'), 50, 0, &v16);
	if(ret) return ret;

	*dst = (v16)? 4: 2;

	return 0;
}


//======================================
// メモリ (readbuf) から読み込む
//======================================


/** readbuf エラー時 */

int font_readbuf_err(Font *p)
{
	p->readbuf_err = 1;
	p->readbuf_cur = p->readbuf + p->readbuf_size;
	p->readbuf_remain = 0;

	return 1;
}

/** 現在位置を取得 */

uint32_t font_readbuf_getpos(Font *p)
{
	return p->readbuf_cur - p->readbuf;
}

/** 指定位置のポインタを取得
 *
 * size: 指定位置以降に読み込みサイズ (残りチェック) */

int font_readbuf_getposptr(Font *p,uint32_t pos,int size,uint8_t **dst)
{
	if(pos + size > p->readbuf_size)
	{
		p->readbuf_err = 1;
		return 1;
	}

	*dst = p->readbuf + pos;

	return 0;
}

/** 現在位置をセット
 *
 * 終端は成功とする */

int font_readbuf_setpos(Font *p,uint32_t pos)
{
	if(pos > p->readbuf_size)
		return font_readbuf_err(p);

	p->readbuf_cur = p->readbuf + pos;
	p->readbuf_remain = p->readbuf_size - pos;

	return 0;
}

/** 位置を記録 */

void font_readbuf_push_pos(Font *p)
{
	if(p->readbuf_offset_num >= FONT_READBUF_OFFSET_NUM)
	{
		fprintf(stderr, "!! push error\n");
		return;
	}

	p->readbuf_offset[p->readbuf_offset_num++] = p->readbuf_cur - p->readbuf;
}

/** 位置を記録して移動 */

int font_readbuf_push_pos_goto(Font *p,uint32_t pos)
{
	font_readbuf_push_pos(p);
	
	if(font_readbuf_setpos(p, pos))
	{
		font_readbuf_pop_pos(p);
		return 1;
	}

	return 0;
}

/** 位置を戻す */

void font_readbuf_pop_pos(Font *p)
{
	if(!p->readbuf_offset_num) return;

	font_readbuf_setpos(p, p->readbuf_offset[--p->readbuf_offset_num]);
}


/** 指定バイトスキップして、スキップ前の先頭ポインタを返す
 *
 * 現在位置から指定サイズの範囲を、バッファから直接読み込む時。 */

int font_readbuf_skip(Font *p,int size,uint8_t **dst)
{
	if(p->readbuf_remain < size)
		return font_readbuf_err(p);

	if(dst) *dst = p->readbuf_cur;

	p->readbuf_cur += size;
	p->readbuf_remain -= size;

	return 0;
}

/** 8bit 読み込み */

int font_readbuf8(Font *p,void *dst)
{
	if(p->readbuf_remain < 1)
	{
		*((uint8_t *)dst) = 0;
		return font_readbuf_err(p);
	}

	*((uint8_t *)dst) = *(p->readbuf_cur);

	p->readbuf_cur++;
	p->readbuf_remain--;

	return 0;
}

/** 16bit 読み込み */

int font_readbuf16(Font *p,void *dst)
{
	uint8_t *ps = p->readbuf_cur;

	if(p->readbuf_remain < 2)
	{
		*((uint16_t *)dst) = 0;
		return font_readbuf_err(p);
	}

	*((uint16_t *)dst) = (ps[0] << 8) | ps[1];

	p->readbuf_cur += 2;
	p->readbuf_remain -= 2;

	return 0;
}

/** 24bit 読み込み (int32) */

int font_readbuf24(Font *p,void *dst)
{
	uint8_t *ps = p->readbuf_cur;

	if(p->readbuf_remain < 3)
	{
		*((int32_t *)dst) = 0;
		return font_readbuf_err(p);
	}

	*((int32_t *)dst) = (ps[0] << 16) | (ps[1] << 8) | ps[2];

	p->readbuf_cur += 3;
	p->readbuf_remain -= 3;

	return 0;
}

/** 32bit 読み込み */

int font_readbuf32(Font *p,void *dst)
{
	uint8_t *ps = p->readbuf_cur;

	if(p->readbuf_remain < 4)
	{
		*((uint32_t *)dst) = 0;
		return font_readbuf_err(p);
	}

	*((uint32_t *)dst) = ((uint32_t)ps[0] << 24) | (ps[1] << 16) | (ps[2] << 8) | ps[3];

	p->readbuf_cur += 4;
	p->readbuf_remain -= 4;

	return 0;
}

/** 64bit 読み込み */

int font_readbuf64(Font *p,void *dst)
{
	uint8_t *ps = p->readbuf_cur;
	uint32_t v1,v2;

	if(p->readbuf_remain < 8)
	{
		*((uint64_t *)dst) = 0;
		return font_readbuf_err(p);
	}

	v1 = ((uint32_t)ps[0] << 24) | (ps[1] << 16) | (ps[2] << 8) | ps[3];
	v2 = ((uint32_t)ps[4] << 24) | (ps[5] << 16) | (ps[6] << 8) | ps[7];

	*((uint64_t *)dst) = ((uint64_t)v1 << 32) | v2;

	p->readbuf_cur += 8;
	p->readbuf_remain -= 8;

	return 0;
}

/** 指定サイズ分の値を uint32 として読み込み (CFF オフセット)
 *
 * size: 1,2,3,4 */

int font_readbuf_nsize(Font *p,int size,uint32_t *dst)
{
	uint8_t u8;
	uint16_t u16;

	switch(size)
	{
		case 1:
			if(font_readbuf8(p, &u8)) return 1;
			*dst = u8;
			return 0;
		case 2:
			if(font_readbuf16(p, &u16)) return 1;
			*dst = u16;
			return 0;
		case 3:
			return font_readbuf24(p, dst);
		case 4:
			return font_readbuf32(p, dst);
	}

	return 1;
}

/** readbuf から指定フォーマットで取得
 *
 * b=8bit, h=16bit, i=32bit, s<N>=byteスキップ */

int font_readbuf_format(Font *p,const char *fmt,...)
{
	va_list ap;
	void *ptr;
	int c,c2,n,ret = 1;

	va_start(ap, fmt);

	while(*fmt)
	{
		c = *(fmt++);
		c2 = (c)? *fmt: 0;

		if(c2 >= '1' && c2 <= '9')
			n = strtol(fmt, (char **)&fmt, 10);
		else
			n = 1;
		
		switch(c)
		{
			//8bit
			case 'b':
				ptr = va_arg(ap, void *);

				if(font_readbuf8(p, ptr)) goto ERR;
				break;
			//16bit
			case 'h':
				ptr = va_arg(ap, void *);

				if(font_readbuf16(p, ptr)) goto ERR;
				break;
			//32bit
			case 'i':
				ptr = va_arg(ap, void *);

				if(font_readbuf32(p, ptr)) goto ERR;
				break;
			//skip
			case 's':
				if(font_readbuf_skip(p, n, 0)) goto ERR;
				break;
		}
	}

	ret = 0;

ERR:
	va_end(ap);

	return ret;
}

/** 16bit 配列をエンディアン変換して ArrayBuf に取得 */

int font_readbuf_arraybuf16(Font *p,int num,ArrayBuf *dst)
{
	uint8_t *buf,*ps;
	uint16_t *pd;
	int i;

	if(font_readbuf_skip(p, num * 2, &ps)) return 1;

	buf = (uint8_t *)app_malloc(num * 2);
	if(!buf) return 1;

	pd = (uint16_t *)buf;

	for(i = num; i; i--, ps += 2)
		*(pd++) = (ps[0] << 8) | ps[1];

	dst->buf = buf;
	dst->num = num;

	return 0;
}


//================================
// バッファから直接取得
//================================


/** バッファから 16bit 取得 */

void font_getbuf16(const uint8_t *src,void *dst)
{
	*((uint16_t *)dst) = (src[0] << 8) | src[1];
}

/** バッファから 32bit 取得 */

void font_getbuf32(const uint8_t *src,void *dst)
{
	*((uint32_t *)dst) = ((uint32_t)src[0] << 24) | (src[1] << 16) | (src[2] << 8) | src[3];
}

/** バッファから 16bit 配列取得 */

void font_getbuf16_ar(const uint8_t *src,int cnt,void *dst)
{
	uint16_t *pd = (uint16_t *)dst;

	for(; cnt; cnt--, src += 2)
		*(pd++) = (src[0] << 8) | src[1];
}

/** バッファから指定フォーマットで取得
 *
 * b=8bit, h=16bit, k=24bit(int32), i=32bit */

void font_getbuf_format(const void *buf,const char *fmt,...)
{
	va_list ap;
	void *ptr;
	const uint8_t *ps = (const uint8_t *)buf;
	int c;

	va_start(ap, fmt);

	while(*fmt)
	{
		c = *(fmt++);
		
		switch(c)
		{
			//8bit
			case 'b':
				ptr = va_arg(ap, void *);

				*((uint8_t *)ptr) = *(ps++);
				break;
			//16bit
			case 'h':
				ptr = va_arg(ap, void *);

				*((uint16_t *)ptr) = (ps[0] << 8) | ps[1];
				ps += 2;
				break;
			//24bit
			case 'k':
				ptr = va_arg(ap, void *);

				*((int32_t *)ptr) = (ps[0] << 16) | (ps[1] << 8) | ps[2];
				ps += 3;
				break;
			//32bit
			case 'i':
				ptr = va_arg(ap, void *);

				*((uint32_t *)ptr) = ((uint32_t)ps[0] << 24) | (ps[1] << 16) | (ps[2] << 8) | ps[3];
				ps += 4;
				break;
		}
	}

	va_end(ap);
}

/** バッファから、指定サイズ分の値を uint32 として読み込み (CFF オフセット)
 *
 * size: 1,2,3,4 */

int font_getbuf_nsize(const uint8_t *buf,int size,uint32_t *dst)
{
	switch(size)
	{
		case 1:
			*dst = *buf;
			return 0;
		case 2:
			*dst = (buf[0] << 8) | buf[1];
			return 0;
		case 3:
			*dst = (buf[0] << 16) | (buf[1] << 8) | buf[2];
			return 0;
		case 4:
			font_getbuf32(buf, dst);
			return 0;
	}

	return 1;
}


//======================
// ファイル操作
//======================


/** 先頭からシーク */

int font_seek_top(Font *p,uint32_t size)
{
	return fseek(p->fp, size, SEEK_SET);
}

/** 現在位置からシーク */

int font_seek(Font *p,int size)
{
	return fseek(p->fp, size, SEEK_CUR);
}

/** 2byte 読み込み */

int font_read16(Font *p,void *ptr)
{
	uint8_t b[2];

	if(fread(b, 1, 2, p->fp) != 2)
		return 1;

	*((uint16_t *)ptr) = (b[0] << 8) | b[1];

	return 0;
}

/** 4byte 読み込み */

int font_read32(Font *p,void *ptr)
{
	uint8_t b[4];

	if(fread(b, 1, 4, p->fp) != 4)
		return 1;

	*((uint32_t *)ptr) = ((uint32_t)b[0] << 24) | (b[1] << 16) | (b[2] << 8) | b[3];

	return 0;
}

/** 16bit 配列読み込み */

int font_read16_array(Font *p,void *ptr,int num)
{
	uint16_t *pd = (uint16_t *)ptr;
	uint8_t *ps = (uint8_t *)ptr;

	if(fread(ptr, 1, 2 * num, p->fp) != 2 * num)
		return 1;

	for(; num; num--, ps += 2)
		*(pd++) = (ps[0] << 8) | ps[1];

	return 0;
}

/** 32bit 配列読み込み */

int font_read32_array(Font *p,void *ptr,int num)
{
	uint32_t *pd = (uint32_t *)ptr;
	uint8_t *ps = (uint8_t *)ptr;

	if(fread(ptr, 1, 4 * num, p->fp) != 4 * num)
		return 1;

	for(; num; num--, ps += 4)
		*(pd++) = ((uint32_t)ps[0] << 24) | (ps[1] << 16) | (ps[2] << 8) | ps[3];

	return 0;
}

/** 16bit 配列を確保して読み込み */

int font_read16ar_alloc(Font *p,void **pptr,int num)
{
	void *ptr;

	if(num <= 0)
	{
		*pptr = NULL;
		return 0;
	}

	ptr = app_malloc(2 * num);
	if(!ptr) return 1;

	if(font_read16_array(p, ptr, num))
	{
		app_free(ptr);
		return 1;
	}

	*pptr = ptr;

	return 0;
}

/** 32bit 配列を確保して読み込み */

int font_read32ar_alloc(Font *p,void **pptr,int num)
{
	void *ptr;

	if(num <= 0)
	{
		*pptr = NULL;
		return 0;
	}

	ptr = app_malloc(4 * num);
	if(!ptr) return 1;

	if(font_read32_array(p, ptr, num))
	{
		app_free(ptr);
		return 1;
	}

	*pptr = ptr;

	return 0;
}

/** 指定オフセット位置から、指定サイズのメモリを確保して読み込み
 *
 * offset: 0 で現在位置 */

int font_read_alloc(Font *p,uint32_t offset,int size,void **dst)
{
	void *buf;

	if(size < 0) return 1;

	if(offset && font_seek_top(p, offset))
		return 1;

	buf = app_malloc(size);
	if(!buf) return 1;

	if(fread(buf, 1, size, p->fp) != size)
	{
		app_free(buf);
		return 1;
	}

	*dst = buf;

	return 0;
}

/** 複数読み込み
 *
 * b<N>=8bit, h<N>=16bit, i<N>=32bit, s<N>=指定バイトスキップ
 * I=32bit配列確保(次の引数で個数)
 *
 * 後ろに数値があると、配列扱い。
 * 配列は void**。メモリ確保してポインタを返す。 */

int font_reads(Font *p,const char *fmt,...)
{
	va_list ap;
	void *ptr,**pptr;
	int c,c2,ret = 1,n;

	va_start(ap, fmt);

	while(*fmt)
	{
		c = *(fmt++);
		c2 = (*fmt)? *fmt: 0;

		if(c2 >= '0' && c2 <= '9')
			n = strtoul(fmt, (char **)&fmt, 10);
		else
			n = 0;

		switch(c)
		{
			//8bit
			case 'b':
				ptr = va_arg(ap, void *);

				if(n == 0)
					n = 0;
				else
					n = (fread(ptr, 1, n, p->fp) != n);

				if(n) goto END;
				break;
			//16bit
			case 'h':
				ptr = va_arg(ap, void *);

				if(n == 0)
					n = font_read16(p, ptr);
				else
					n = font_read16_array(p, ptr, n);
				if(n) goto END;
				break;
			//32bit
			case 'i':
				ptr = va_arg(ap, void *);

				if(n == 0)
					n = font_read32(p, ptr);
				else
					n = font_read32_array(p, ptr, n);
				if(n) goto END;
				break;
			//スキップ
			case 's':
				if(fseek(p->fp, n, SEEK_CUR)) goto END;
				break;
			//32bit 配列(確保)
			case 'I':
				pptr = va_arg(ap, void **);
				n = va_arg(ap, int);

				if(font_read32ar_alloc(p, pptr, n)) goto END;
				break;
		}
	}

	ret = 0;

END:
	va_end(ap);

	return ret;
}
