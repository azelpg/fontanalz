/*$
fontanalz
Copyright (c) 2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * フォント: GID -> Unicode 変換テーブル
 *****************************************/

#include <stdint.h>
#include <stdio.h>

#include "def.h"
#include "font.h"
#include "font_pv.h"


/* 読み込むサブテーブルのオフセットを取得 */

static int _get_subtbl_offset(uint8_t *pbuf,int num,uint32_t *poff)
{
	const uint16_t id[] = {
		(3<<8)|10, (3<<8)|1, 4, 3, 0
	};
	const uint16_t *pid;
	uint8_t *ps;
	uint16_t plat,enc,plat_cmp,enc_cmp;
	uint32_t off;
	int i;

	//plat=3, enc=10,1 > plat=0, enc=4,3 の順で優先

	for(pid = id; *pid; pid++)
	{
		plat_cmp = *pid >> 8;
		enc_cmp = *pid & 255;
	
		for(ps = pbuf, i = num; i; i--, ps += 8)
		{
			font_getbuf_format(ps, "hhi", &plat, &enc, &off);

			if(plat == plat_cmp && enc == enc_cmp)
			{
				*poff = off;
				return 0;
			}
		}
	}

	return 1;
}

/* format 4: Unicode BMP */

static int _subtbl_format4(Font *p)
{
	uint16_t segcnt2,start,end,range,v16;
	int16_t delta;
	int cnt,gid,n,gnum;
	uint8_t *pend,*pstart,*pdelta,*prange,*pid;
	uint32_t *dstbuf;

	if(font_readbuf_format(p, "s4hs6", &segcnt2)
		|| font_readbuf_skip(p, segcnt2 + 2, &pend)
		|| font_readbuf_skip(p, segcnt2, &pstart)
		|| font_readbuf_skip(p, segcnt2, &pdelta)
		|| font_readbuf_skip(p, segcnt2, &prange))
		return 1;

	dstbuf = p->gidunimap;
	gnum = p->maxp_gnum;

	for(cnt = segcnt2 / 2 - 1; cnt > 0; cnt--)
	{
		font_getbuf16(pend, &end);
		font_getbuf16(pstart, &start);
		font_getbuf16(pdelta, &delta);
		font_getbuf16(prange, &range);

		if(!range)
		{
			//range = 0
			
			for(gid = start + delta; start <= end; start++, gid++)
			{
				n = gid & 0xffff;

				if(n < gnum && !dstbuf[n])
					dstbuf[n] = start;
			}
		}
		else
		{
			pid = prange + range;

			for(; start <= end; start++, pid += 2)
			{
				font_getbuf16(pid, &v16);

				if(v16 < gnum && !dstbuf[v16])
					dstbuf[v16] = start;
			}
		}

		pend += 2;
		pstart += 2;
		pdelta += 2;
		prange += 2;
	}

	return 0;
}

/* format 12: Unicode full */

static int _subtbl_format12(Font *p)
{
	uint32_t num,start,end,gid,*pd;
	uint8_t *pbuf;
	int gnum;

	if(font_readbuf_format(p, "s10i", &num)
		|| font_readbuf_skip(p, num * 12, &pbuf))
		return 1;

	//SequentialMapGroup

	pd = p->gidunimap;
	gnum = p->maxp_gnum;

	for(; num; num--, pbuf += 12)
	{
		font_getbuf_format(pbuf, "iii", &start, &end, &gid);

		for(; start <= end; start++, gid++)
		{
			if(gid < gnum && !pd[gid])
				pd[gid] = start;
		}
	}

	return 0;
}

/* cmap 処理 */

static int _proc_cmap(Font *p)
{
	uint8_t *pbuf;
	uint32_t off;
	uint16_t ver,num,format;

	if(font_readbuf16(p, &ver) || ver != 0) return 1;
	font_readbuf16(p, &num);

	if(font_readbuf_skip(p, num * 8, &pbuf)) return 1;

	//EncodingRecord

	if(_get_subtbl_offset(pbuf, num, &off)) return 1;

	//subtable

	if(font_readbuf_setpos(p, off)) return 1;

	font_readbuf16(p, &format);

	switch(format)
	{
		case 4:
			return _subtbl_format4(p);
		case 12:
			return _subtbl_format12(p);
		default:
			return 1;
	}

	return 0;
}

/** GID -> Unicode テーブル作成 */

int font_make_gidunimap(Font *p)
{
	int ret;

	//cmap

	ret = font_read_table(p, MAKE_TAG('c','m','a','p'));
	if(ret) return ret;

	//バッファ確保

	p->gidunimap = (uint32_t *)app_malloc0(p->maxp_gnum * 4);
	if(!p->gidunimap) return FONTERR_ALLOC;

	//

	if(_proc_cmap(p))
	{
		app_free(p->gidunimap);
		p->gidunimap = NULL;
	}

	font_free_readbuf(p);

	return 0;
}


