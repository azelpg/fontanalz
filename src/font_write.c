/*$
fontanalz
Copyright (c) 2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/**********************************
 * フォント: 出力
 **********************************/

#include <stdint.h>
#include <stdio.h>
#include <stdarg.h>
#include <time.h>

#include "def.h"
#include "font.h"
#include "font_pv.h"



/** 出力を開く (stdout or ファイル)
 *
 * filename: パスは含まない
 * header: NULL 以外でヘッダを出力 */

int font_openput(Font *p,const char *filename,const char *header)
{
	char *buf;
	FILE *fp;

	app_memset0(&p->put, sizeof(FontPut));

	if(!(g_app.optflags & OPTF_FILE))
	{
		//stdout
		
		fp = stdout;
	}
	else
	{
		//ファイル
		
		buf = app_get_output_path(g_app.opt_output, g_app.in_filename, filename);
		if(!buf) return FONTERR_ALLOC;

		fp = fopen(buf, "wb");

		app_free(buf);

		if(!fp) return FONTERR_OPENFILE;

		p->put.fclose = 1;
	}

	p->put.fp = fp;

	//ヘッダ

	if(header)
	{
		fputs("==================================\n", fp);
		fprintf(fp, " * %s\n", header);
		fputs("==================================\n", fp);
	}

	return FONTERR_OK;
}

/** 出力を開く (ファイル名に番号指定。ヘッダはなし)
 *
 * prefix: 番号より前
 * suffix: 番号より後
 * no: ファイル名の番号 */

int font_openput_fno(Font *p,const char *prefix,const char *suffix,int no)
{
	char m[32];

	snprintf(m, 32, "%s%d%s", prefix, no, suffix);

	return font_openput(p, m, NULL);
}

/** 出力を閉じる */

void font_closeput(Font *p)
{
	if(p->put.fp)
		fputc('\n', p->put.fp);

	if(p->put.fclose && p->put.fp)
		fclose(p->put.fp);

	p->put.fp = NULL;
}

/** 出力を閉じて、readbuf のエラーを返す */

int font_closeput_ret(Font *p)
{
	font_closeput(p);

	return (p->readbuf_err)? FONTERR_READ: 0;
}


//================


/** サブヘッダ出力 */

void font_put_header(Font *p,const char *name)
{
	fprintf(p->put.fp, "\n============================\n  %s\n============================\n\n", name);
}

/** 32bit数値から４文字出力 */

void font_put_tagname(Font *p,uint32_t v)
{
	fprintf(p->put.fp, "'%c%c%c%c'",
		(uint8_t)(v >> 24), (uint8_t)(v >> 16), (uint8_t)(v >> 8), (uint8_t)v);
}

/** int32(16:16) 固定小数点数を出力 (改行あり) */

void font_put_fixed(Font *p,int32_t v)
{
	int f;

	f = (int)((v & 0xffff) / 65536.0 * 10000 + 0.5);

	fprintf(p->put.fp, "%d.%04d (0x%08X)\n", v >> 16, f, v);
}

/** ON になっているビットを出力 (改行なし) */

void font_put_bits_on(Font *p,uint32_t f,int cnt)
{
	FILE *fp = p->put.fp;
	int i,prev = 0;

	for(i = 0; f && i < cnt; i++, f >>= 1)
	{
		if(f & 1)
		{
			if(prev) fputc(',', fp);
			fprintf(fp, "bit%d", i);
			prev = 1;
		}
	}

	if(!prev)
		fputs("none", fp);
}

/** "bitN (f) name" を出力
 *
 * names: ';' で区切る */

void font_put_bits_name(Font *p,uint32_t f,int cnt,const char *names)
{
	FILE *fp = p->put.fp;
	int i,len;
	const char *top;

	for(i = 0; i < cnt; i++, f >>= 1)
	{
		top = names;
		for(; *names && *names != ';'; names++);
		
		len = names - top;
		if(*names == ';') names++;
	
		fprintf(fp, "  bit%d (%d) %.*s\n", i, f & 1, len, top);
	}
}

/** "GID (U+XXXX 'char')" を出力*/

void font_put_gid_unicode(Font *p,int gid)
{
	FILE *fp = p->put.fp;
	uint32_t c;

	fprintf(fp, "GID %d", gid);

	if(gid < p->maxp_gnum
		&& p->gidunimap
		&& p->gidunimap[gid])
	{
		c = p->gidunimap[gid];
	
		fprintf(fp, " (U+%04X", c);

		if(c < 0x20)
			fputc(')', fp);
		else
		{
			fputs(" '", fp);
			font_put_uni_utf8(p, c);
			fputs("')", fp);
		}
	}
}


//======================================
// メモリ (readbuf) から読み込んで出力
//======================================
/*
	(16/32bit) name の先頭1文字で、フォーマット指定。
	  +: 符号なし
	  -: 符号付き
	  $: 16進数
	  #: フラグ (16進数 + 次行に ON のビットリスト)
	  &: (16bit) 符号付き 2:14 固定少数
	  %: (32bit) 16:16 固定少数
	  *: (32bit) タグを４文字で

	dst: NULL 以外で、値を代入
*/


/** 8bit 出力 */

int font_putbuf8(Font *p,const char *name,void *dst)
{
	FILE *fp = p->put.fp;
	uint8_t v;

	if(font_readbuf8(p, &v)) return 1;

	switch(name[0])
	{
		case '+':
			fprintf(fp, "u8  %s : %d\n", name + 1, v);
			break;
		case '-':
			fprintf(fp, "s8  %s : %d\n", name + 1, (int8_t)v);
			break;
		case '$':
			fprintf(fp, "u8  %s : 0x%02X\n", name + 1, v);
			break;
	}

	if(dst) *((uint8_t *)dst) = v;
	
	return 0;
}

/** 16bit 出力 */

int font_putbuf16(Font *p,const char *name,void *dst)
{
	FILE *fp = p->put.fp;
	uint16_t v;

	if(font_readbuf16(p, &v)) return 1;

	if(name[0] != '-')
		fprintf(fp, "u16 %s : ", name + 1);

	switch(name[0])
	{
		case '+':
			fprintf(fp, "%d\n", v);
			break;
		case '-':
			fprintf(fp, "s16 %s : %d\n", name + 1, (int16_t)v);
			break;
		case '$':
			fprintf(fp, "0x%04X\n", v);
			break;
		case '&':
			fprintf(fp, "%g (0x%04X)\n", (double)v / (1<<14), v);
			break;
		case '#':
			fprintf(fp, "0x%04X\n", v);

			if(v)
			{
				fputs("  ON = ", fp);
				font_put_bits_on(p, v, 16);
				fputc('\n', fp);
			}
			break;
	}

	if(dst) *((uint16_t *)dst) = v;
	
	return 0;
}

/** 24bit 出力 */

int font_putbuf24(Font *p,const char *name,void *dst)
{
	FILE *fp = p->put.fp;
	int32_t v;

	if(font_readbuf24(p, &v)) return 1;

	switch(name[0])
	{
		case '+':
			fprintf(fp, "u24 %s : %d\n", name + 1, v);
			break;
		case '$':
			fprintf(fp, "u24 %s : 0x%06X\n", name + 1, v);
			break;
	}

	if(dst) *((int32_t *)dst) = v;
	
	return 0;
}

/** 32bit 出力 */

int font_putbuf32(Font *p,const char *name,void *dst)
{
	FILE *fp = p->put.fp;
	uint32_t v;

	if(font_readbuf32(p, &v)) return 1;

	switch(name[0])
	{
		case '+':
			fprintf(fp, "u32 %s : %u\n", name + 1, v);
			break;
		case '-':
			fprintf(fp, "s32 %s : %d\n", name + 1, (int32_t)v);
			break;
		case '$':
			fprintf(fp, "u32 %s : 0x%08X\n", name + 1, v);
			break;
		case '*':
			fprintf(fp, "u32 %s : ", name + 1);
			font_put_tagname(p, v);
			fprintf(fp, " (0x%08X)\n", v);
			break;
		case '%':
			fprintf(fp, "s32 %s : ", name + 1);
			font_put_fixed(p, (int32_t)v);
			break;
	}

	if(dst) *((uint32_t *)dst) = v;
	
	return 0;
}

/** 8bit 配列出力 */

int font_putbuf8_ar(Font *p,const char *name,int cnt)
{
	uint8_t *ps = p->readbuf_cur;
	FILE *fp = p->put.fp;

	if(p->readbuf_remain < cnt)
		return font_readbuf_err(p);

	p->readbuf_cur += cnt;
	p->readbuf_remain -= cnt;

	if(name)
		fprintf(fp, "u8  %s:\n", name);

	if(cnt)
	{
		for(; cnt; cnt--, ps++)
			fprintf(fp, " %d", *ps);

		fputc('\n', fp);
	}

	return 0;
}

/** 16bit 配列出力
 *
 * format: 0=u16, 1=s16 */

int font_putbuf16_ar(Font *p,int cnt,int format)
{
	uint8_t *ps;
	FILE *fp = p->put.fp;

	if(!cnt) return 0;

	if(font_readbuf_skip(p, cnt * 2, &ps))
		return 1;

	switch(format)
	{
		case 0:
			for(; cnt; cnt--, ps += 2)
				fprintf(fp, " %d", (ps[0] << 8) | ps[1]);
			break;
		case 1:
			for(; cnt; cnt--, ps += 2)
				fprintf(fp, " %d", (int16_t)((ps[0] << 8) | ps[1]));
			break;
	}

	fputc('\n', fp);

	return 0;
}

/** 32bit 配列出力
 *
 * format: 0=hex */

int font_putbuf32_ar(Font *p,int cnt,int format)
{
	FILE *fp = p->put.fp;
	uint8_t *ps;
	uint32_t v;

	if(!cnt) return 0;

	if(font_readbuf_skip(p, cnt * 4, &ps))
		return 1;

	for(; cnt; cnt--, ps += 4)
	{
		v = ((uint32_t)ps[0] << 24) | (ps[1] << 16) | (ps[2] << 8) | ps[3];

		fprintf(fp, " 0x%08X", v);
	}

	fputc('\n', fp);

	return 0;
}

/** 64bit 日時出力 */

int font_putbuf_date(Font *p,const char *name)
{
	struct tm *ptm;
	time_t time;
	int64_t val;
	char m[32];

	if(font_readbuf64(p, &val)) return 1;

	// LONGDATETIME: GMT 1904/01/01 00:00 からの秒数
	// time 関数   : GMT 1970/01/01 00:00 からの秒数
	//time 関数の基準に合わせる場合、1904/01/01 から 1970/01/01 にかけての経過秒数を引く。

	time = val - 2082844800;

	ptm = localtime(&time);

	strftime(m, 32, "%Y/%m/%d %H:%M:%S", ptm);

	fprintf(p->put.fp, "u64 %s : %s\n", name, m);

	return 0;
}


//=========================
// 文字列
//=========================


/* UTF-16 文字列から Unicode 1文字取得
 *
 * return: 0=成功、-1=エラー、1=無効な文字(次へ) */

static int _get_utf16_char(uint32_t *dst,const uint8_t *src,const uint8_t **ppnext)
{
	uint32_t c,c2;

	c = (src[0] << 8) | src[1];
	src += 2;

	//サロゲート (U+D800 .. U+DFFF)

	if((c & 0xf800) == 0xd800)
	{
		//ローサロゲートは先頭に来ない
		if(c & 0x0400) return -1;

		c2 = (src[0] << 8) | src[1];
		src += 2;

		//2番目がローサロゲートでない
		if((c2 & 0xfc00) != 0xdc00) return -1;

		c = 0x10000 + ((c - 0xd800) << 10) + (c2 - 0xdc00);
	}

	*dst = c;
	*ppnext = src;

	return (c == 0xfeff)? 1: 0;
}

/** Unicode 1文字 を UTF-8 で出力 */

void font_put_uni_utf8(Font *p,uint32_t c)
{
	int len,top,i;
	char m[4];

	if(c < 0x80)
	{
		len = 1;
		top = 0;
	}
	else if(c < 0x800)
	{
		len = 2;
		top = 0xc0;
	}
	else if(c < 0x10000)
	{
		len = 3;
		top = 0xe0;
	}
	else if(c <= 0x10ffff)
	{
		len = 4;
		top = 0xf0;
	}
	else
		return;

	//

	for(i = len - 1; i > 0; i--)
	{
		m[i] = (c & 0x3f) | 0x80;
		c >>= 6;
	}

	m[0] = c | top;

	fwrite(m, 1, len, p->put.fp);
}

/** UTF-16BE を UTF-8 に変換して出力
 *
 * len: バイト数 */

void font_putbuf_utf16be(Font *p,const void *src,int len)
{
	const uint8_t *ps = (const uint8_t *)src,*next;
	uint32_t uc;
	int ret;

	while(len)
	{
		//UTF-16 => Unicode

		ret = _get_utf16_char(&uc, ps, &next);
		if(ret == -1) return;

		len -= next - ps;
		ps = next;

		if(ret == 1) continue;

		//出力

		font_put_uni_utf8(p, uc);
	}
}

/** (name テーブル) 文字列を出力
 *
 * platid: 0,3=UTF-16BE, それ以外=128-の文字を含む場合はバイナリ
 * len: バイト数 */

void font_putbuf_string(Font *p,int platid,int str_offset,int offset,int len)
{
	FILE *fp;
	uint8_t *buf,*ps;
	int i;

	if(str_offset + offset + len > p->readbuf_size)
		return;

	buf = p->readbuf + str_offset + offset;
	fp = p->put.fp;

	if(platid == 0 || platid == 3)
		font_putbuf_utf16be(p, buf, len);
	else
	{
		//128 以上の文字を含むか

		for(i = len, ps = buf; i && *ps < 128; i--, ps++);

		if(!i)
			//すべて 128 以下
			fwrite(buf, 1, len, p->put.fp);
		else
		{
			//バイナリ出力

			for(; len; len--, buf++)
				fprintf(fp, "0x%02X ", *buf);
		}
	}

	fputc('\n', fp);
}

