/*$
fontanalz
Copyright (c) 2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/**************************************
 * オプション処理
 **************************************/

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "getoption.h"


//-------------------

typedef struct
{
	const goptdat *opts;
	char **argv;
	
	int argc,
		curpos,		//現在の処理位置
		normpos;	//通常引数の先頭位置 (-1 でなし)
}_data;

//-------------------


/* オプション検索 */

static const goptdat *_search_opts(_data *p,char *str)
{
	const goptdat *po;
	char ch;

	if(str[1] == '-')
	{
		//長いオプション
		
		str += 2;

		for(po = p->opts; po->opt_long || po->opt_ch; po++)
		{
			if(strcmp(str, po->opt_long) == 0)
				return po;
		}
	}
	else
	{
		//短いオプション
		
		if(str[2]) return NULL;
		
		ch = str[1];

		for(po = p->opts; po->opt_long || po->opt_ch; po++)
		{
			if(po->opt_ch == ch)
				return po;
		}
	}

	return NULL;
}

/* 先頭が '-' の場合の処理
 *
 * return: 0 でオプションとして処理した。
 *  1 でエラー終了。 */

static int _proc_option(_data *p,char *str)
{
	const goptdat *popt;

	//オプションを検索

	popt = _search_opts(p, str);

	if(!popt)
	{
		fprintf(stderr, "unknown option: %s\n", str);
		return 1;
	}

	p->curpos++;

	//オプション処理

	if(!popt->have_arg)
	{
		//引数なし
		
		(popt->func)(NULL);
	}
	else
	{
		//引数あり

		if(p->curpos >= p->argc)
		{
			fprintf(stderr, "option requires value: %s\n", str);
			return 1;
		}

		(popt->func)(p->argv[p->curpos++]);
	}

	return 0;
}

/* 処理したオプション引数を、通常引数の前に移動 */

static void _move_option(_data *p,int optpos)
{
	char **argv,*opt1,*opt2;
	int i,top,from,num;

	argv = p->argv;
	num = p->curpos - optpos;

	//オプションのポインタを保存

	opt1 = argv[optpos];
	
	if(num == 2) opt2 = argv[optpos + 1];

	//normpos 〜 optpos - 1 までを、下にオプション分ずらす

	top = p->normpos + num;
	from = optpos - 1;

	for(i = p->curpos - 1; i >= top; i--, from--)
		argv[i] = argv[from];

	//オプションのポインタを挿入

	i = p->normpos;

	argv[i] = opt1;
	if(num == 2) argv[i + 1] = opt2;

	//

	p->normpos = top;
}


/** オプション取得
 *
 * opts: opt_long, opt_ch が共に 0 で終了
 * return: 通常引数の先頭位置。なければ argc と同じ値。
 *  -1 でエラー */

int get_options(int argc,char **argv,const goptdat *opts)
{
	_data d;
	char *pc;
	int toppos,ret;

	d.opts = opts;
	d.argc = argc;
	d.argv = argv;
	d.curpos = 1;
	d.normpos = -1;

	while(d.curpos < argc)
	{
		pc = argv[d.curpos];

		if(*pc != '-')
		{
			//通常引数として扱う: 先頭位置をマーク
			
			if(d.normpos == -1)
				d.normpos = d.curpos;

			d.curpos++;
		}
		else
		{
			//--- オプション処理
			//オプション処理成功時、
			//現在位置より前に通常引数があれば、
			//オプションのポインタを通常引数の前に移動させる。
			
			toppos = d.curpos;

			ret = _proc_option(&d, pc);
			
			//エラー
			if(ret)
			{
				fflush(stderr);
				return -1;
			}

			if(d.normpos != -1)
				_move_option(&d, toppos);
		}
	}

	return (d.normpos == -1)? argc: d.normpos;
}

