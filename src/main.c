/*$
fontanalz
Copyright (c) 2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/**********************************
 * main
 **********************************/

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>

#include "def.h"
#include "font.h"
#include "getoption.h"


//---------------

#define TEXT_VERSION  "fontanalz ver 1.0.1\nCopyright (c) 2022 Azel\n\nThis software is released under the MIT License."

AppData g_app;

static const char *g_tagname[] = {
	"TTCF", "SFNT", "head", "OS/2", "maxp", "name",
	"post", "cmap", "cmap-uni", "hhea", "hmtx", "vhea", "vmtx", "VORG",
	"loca", "glyf", "glyf-comp", "EBLC", "GDEF", "BASE", "GPOS",
	"GSUB", "CFF", 0
};

static FontPutFunc g_putfunc[] = {
	font_put_ttcf, font_put_sfnt, font_put_head, font_put_os2, font_put_maxp,
	font_put_name, font_put_post, font_put_cmap, font_put_cmap_uni, font_put_hhea, font_put_hmtx,
	font_put_vhea, font_put_vmtx, font_put_vorg, font_put_loca, font_put_glyf,
	font_put_glyf_comp, font_put_eblc, font_put_gdef, font_put_base, font_put_gpos,
	font_put_gsub, font_put_cff
};

static int _app_option(int argc,char **argv);

//---------------


/* タグ情報出力 */

static void _put_tag(Font *p,FontPutFunc func,const char *tagname)
{
	int ret,is_file;

	is_file = g_app.optflags & OPTF_FILE;

	if(is_file)
		fprintf(stderr, "<%s>\n", tagname);

	ret = (func)(p);

	if(ret && ret != FONTERR_MES)
	{
		if(!is_file)
			fprintf(stderr, "<%s> ", tagname);
		
		font_puterr(ret);
	}
}

/* 各情報を出力 */

static int _app_put_info(Font *p)
{
	uint32_t f;
	int i;

	if(font_is_cff_file(p))
	{
		//CFF

		_put_tag(p, font_put_cff, "CFF");
	}
	else
	{
		//TrueType/OpenType

		f = g_app.tags;
	
		for(i = 0; f; i++)
		{
			if(!g_tagname[i]) break;

			if(f & 1)
				_put_tag(p, g_putfunc[i], g_tagname[i]);

			f >>= 1;
		}
	}

	return FONTERR_OK;
}

/* フォントファイル処理 */

static int _app_proc_font(const char *filename)
{
	Font *p;
	int ret;

	printf("\n**** %s ****\n\n", filename);

	g_app.in_filename = filename;

	ret = font_openfile(&p, filename, g_app.index);
	if(ret) return ret;

	//

	if(g_app.index < 0)
	{
		//index = 負の場合、コレクションのフォント名を表示

		ret = font_put_fontname(p);
	}
	else if(g_app.optflags & OPTF_TABLES)
	{
		//テーブルをファイルに出力

		ret = font_output_tables(p);
	}
	else
	{
		//各タグを出力

		ret = _app_put_info(p);
	}

	font_close(p);

	return ret;
}

/* 初期化 */

static void _app_init(void)
{
	setlocale(LC_ALL, "");

	app_memset0(&g_app, sizeof(AppData));

	g_app.tags = TAG_DEFAULT;
	g_app.gid_top = -1;
}

/* 終了 */

static void _app_exit(int ret)
{
	exit(ret);
}

/** main */

int main(int argc,char **argv)
{
	int i,ret;

	_app_init();

	i = _app_option(argc, argv);

	for(; i < argc; i++)
	{
		ret = _app_proc_font(argv[i]);

		if(ret)
		{
			font_puterr(ret);
			break;
		}
	}

	_app_exit(0);

	return 0;
}


//==========================
// オプション
//==========================


/* --file */

static void _opt_file(char *arg)
{
	g_app.optflags |= OPTF_FILE;
}

/* --tables */

static void _opt_tables(char *arg)
{
	g_app.optflags |= OPTF_TABLES;
}

/* 出力ディレクトリ */

static void _opt_output(char *arg)
{
	if(app_is_existdir(arg))
	{
		fprintf(stderr, "--output: directory does not exist\n");
		_app_exit(1);
	}

	g_app.opt_output = arg;
	g_app.optflags |= OPTF_FILE;
}

/* --index */

static void _opt_index(char *arg)
{
	if(strcmp(arg, "list") == 0)
		g_app.index = -1;
	else
		g_app.index = strtol(arg, NULL, 10);
}

/* --tags */

static void _opt_tags(char *arg)
{
	const char **pp;
	int no;
	uint32_t flags = 0;

	while(*arg)
	{
		//タグ名 (カンマまで)

		if(app_strcomp_camma(arg, "default") == 0)
			flags |= TAG_DEFAULT;
		else if(app_strcomp_camma(arg, "all") == 0)
			flags |= TAG_ALL;
		else
		{
			for(pp = g_tagname, no = 0; *pp; pp++, no++)
			{
				if(app_strcomp_camma(*pp, arg) == 0)
					break;
			}

			if(*pp)
				flags |= 1 << no;
			else
			{
				fprintf(stderr, "--tags: undefined '%s'\n", arg);
				_app_exit(1);
			}
		}

		//カンマの次へ

		arg = strchr(arg, ',');
		if(!arg) break;

		arg++;
	}

	g_app.tags = flags;
}

/* --verbose */

static void _opt_verbose(char *arg)
{
	g_app.optflags |= OPTF_VERBOSE;
}

/* --raw */

static void _opt_raw(char *arg)
{
	g_app.optflags |= OPTF_RAW;
}

/* --gid */

static void _opt_gid(char *arg)
{
	char *pc;

	pc = strchr(arg, '-');

	if(!pc)
	{
		g_app.gid_top = g_app.gid_end = strtol(arg, NULL, 10);
	}
	else
	{
		g_app.gid_top = strtol(arg, NULL, 10);
		g_app.gid_end = strtol(pc + 1, NULL, 10);

		if(g_app.gid_top > g_app.gid_end)
			g_app.gid_end = g_app.gid_top;
	}
}

/* バージョン情報 */

static void _opt_version(char *arg)
{
	puts(TEXT_VERSION);

	_app_exit(0);
}

/* ヘルプの表示 */

static void _put_help(void)
{
	puts(
"fontanalz [opt] <fontfile...>\n\n"
"fontfile: TrueType/OpenType/CFF\n\n"
"[Options]\n"
"  -o,--output <DIR>:\n"
"      output directory (default = current directory)\n"
"      --file turns on\n"
"  -f,--file:\n"
"      output the result to a file (default = stdout)\n"
"      <filename>-<tag>.txt\n"
"  -i,--index <no or 'list'>: (default = 0)\n"
"      when TTC/OTC, specify the index number of the target font (0-)\n"
"      negative value or 'list': output the name of each font\n"
"  -t,--tags <TAG,...>: (default = 'default')\n"
"      tag names to output (case-sensitivity).\n"
"      multiple designations separated by commas.\n"
"      CFF file will always have only 'CFF'.\n"
"      'all': all tags (excluding 'glyf','glyf-comp')\n"
"      'default': ttcf,sfnt,head,os/2,maxp,name,post,cmap,hhea,vhea\n"
"  -v,--verbose:\n"
"      output detailed information for each tag\n"
"  -r,--raw:\n"
"      output raw data for each tag\n"
"  -g,--gid <N or N1-N2>: (default = -1)\n"
"      target glyph index when outputting glyph outline (glyf, cff[-v])\n"
"      negative value: all glyphs\n"
"  -T,--tables:\n"
"      output binaries of all tables in the font to file\n"
"      <filename>-<tag>.dat\n"
"  -V,--version: display version information\n"
"  -h,--help:    display help\n"
"\n[TAG]\n"
"  ttcf: TTC header (TTC/OTC)\n"
"  sfnt: SFNT data (table list)\n"
"  head: global information\n"
"  OS/2: layout information\n"
"  maxp: maximum value\n"
"  name: strings\n"
"  post: PostScript information\n"
"  hhea: horizontal layout information\n"
"  hmtx: horizontal glyph width and bearing\n"
"  vhea: vertical layout information\n"
"  vmtx: vertical glyph width and bearing\n"
"  VORG: (CFF) vertical glyph origin\n"
"  cmap: character code and GID mapping\n"
"    -v : output subtables (mapping)\n"
"    -r : output subtables (raw)\n"
"  cmap-uni: GID to Unicode mapping list\n"
"  loca: (TrueType) offset position of outline\n"
"  glyf: (TrueType) outline data\n"
"    -g : specify target GID\n"
"  glyf-comp: (TrueType) all composite glyphs only\n"
"  EBLC: (Bitmap) bitmap information\n"
"  GDEF: OpenType layout definition\n"
"  BASE: OpenType baseline definition\n"
"    -r : raw (default is baseline list)\n"
"  GPOS: glyph positioning\n"
"    -r : raw (default is 'GID:position')\n"
"  GSUB: glyph replacement\n"
"    -r : raw (default is GID list)\n"
"  CFF: PostScript outline data\n"
"    -v : all information (Subr, CharStrings)\n"
"    -r : raw (Charsets, FDSelect)");

	_app_exit(0);
}

/* ヘルプ */

static void _opt_help(char *arg)
{
	_put_help();
}


static const goptdat g_appopt[] = {
	{"file",'f',0,_opt_file},
	{"output",'o',1,_opt_output},
	{"index",'i',1,_opt_index},
	{"tags",'t',1,_opt_tags},
	{"verbose",'v',0,_opt_verbose},
	{"raw",'r',0,_opt_raw},
	{"gid",'g',1,_opt_gid},
	{"tables",'T',0,_opt_tables},
	{"version",'V',0,_opt_version},
	{"help",'h',0,_opt_help},
	{0,0,0,0}
};

/* オプション処理 */

int _app_option(int argc,char **argv)
{
	int ret;

	ret = get_options(argc, argv, g_appopt);

	if(ret == -1)
		_app_exit(1);
	else if(ret == argc)
		_put_help();

	return ret;
}

