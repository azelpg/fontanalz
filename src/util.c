/*$
fontanalz
Copyright (c) 2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/**********************************
 * util
 **********************************/

#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>

#ifdef ENABLE_MEMDEBUG
#include "memdebug.h"
#endif

#if defined(_WIN64) || defined(WIN32) || defined(_WIN32)
#define OS_WIN
#endif


/** ゼロクリア */

void app_memset0(void *ptr,int size)
{
	memset(ptr, 0, size);
}

/** メモリ確保 */

void *app_malloc(uint32_t size)
{
	return malloc(size);
}

/** メモリ確保してゼロクリア */

void *app_malloc0(uint32_t size)
{
	return calloc(1, size);
}

/** メモリ解放 */

void app_free(void *ptr)
{
	if(ptr) free(ptr);
}

/** 大文字小文字区別せずに比較 (カンマも終端とみなす)
 *
 * return: 0 で等しい */

int app_strcomp_camma(const char *s1,const char *s2)
{
	char c1,c2;

	while(1)
	{
		c1 = *(s1++);
		c2 = *(s2++);

		if(c1 == ',') c1 = 0;
		if(c2 == ',') c2 = 0;

		if(!c1 && !c2) return 0;

		if(c1 >= 'A' && c1 <= 'Z') c1 += 0x20;
		if(c2 >= 'A' && c2 <= 'Z') c2 += 0x20;

		if(c1 != c2) break;
	}

	return 1;
}

/** ディレクトリが存在するか */

int app_is_existdir(const char *path)
{
#ifdef OS_WIN

	struct _stat st;
	if(_stat(path, &st)) return 1;

	if(!(st.st_mode & _S_IFDIR)) return 1;

#else

	struct stat st;
	if(stat(path, &st)) return 1;

	if(!S_ISDIR(st.st_mode)) return 1;

#endif

	return 0;
}

/** パスとファイル名を結合
 *
 * path: NULL でなし */

char *app_join_path(const char *path,const char *fname)
{
	char *buf;
	int len1,len2;

	len1 = (path)? strlen(path): 0;
	len2 = strlen(fname);

	buf = app_malloc0(len1 + len2 + 2);
	if(!buf) return NULL;

	//出力ディレクトリ

	if(len1)
	{
		memcpy(buf, path, len1);

		//パス区切りを追加

	#ifdef OS_WIN
		if(buf[len1 - 1] != '/' && buf[len1 - 1] != '\\')
	#else
		if(buf[len1 - 1] != '/')
	#endif
		{
			buf[len1++] = '/';
		}
	}

	//ファイル名

	memcpy(buf + len1, fname, len2);

	return buf;
}

/** パスからファイル名のみ取得・拡張子を除去・"-<suffix>" を追加 + 出力パス */

char *app_get_output_path(const char *output,const char *filename,const char *suffix)
{
	const char *pc,*top,*end;
	char *buf,*buf2;
	int len,len1,len2;

	//ファイル名の先頭位置

	for(pc = top = filename; *pc; pc++)
	{
	#ifdef OS_WIN
		if(*pc == '/' || *pc == '\\') top = pc + 1;
	#else
		if(*pc == '/') top = pc + 1;
	#endif
	}

	//拡張子

	end = strrchr(top, '.');
	if(!end) end = strchr(top, 0);

	//確保

	len1 = end - top;
	len2 = strlen(suffix);
	len = len1 + len2 + 2;

	buf = (char *)app_malloc(len);
	if(!buf) return NULL;

	//セット

	memcpy(buf, top, len1);
	buf[len1] = '-';
	
	memcpy(buf + len1 + 1, suffix, len2);
	
	buf[len - 1] = 0;

	//出力パスを追加

	if(!output) return buf;

	buf2 = app_join_path(output, buf);
	if(!buf2) return 0;

	app_free(buf);

	return buf2;
}


